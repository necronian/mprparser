use std::fmt;

#[derive(Debug)]
pub enum Error {
    UnexpectedToken {
        val: String,
        typ: String,
        start: usize,
        end: usize,
        line: usize,
        column: usize,
    },
    UnexpectedTokenEnd,
    UnmatchedTokenType,
    EOFBeforeClosingParen,
    MPRReadNoTerminated,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::UnexpectedToken {
                val,
                typ,
                start,
                end,
                line,
                column,
            } => write!(
                f,
                "An Unexpected Token \"{}\" was Encountered! Of {} @ ({},{}) from ({}:{})",
                val, typ, line, column, start, end
            ),
            Error::UnexpectedTokenEnd => write!(
                f,
                "The end of the token stream ran out of items unexpectedly."
            ),
            Error::UnmatchedTokenType => {
                write!(f, "The token supplied to the nud is not a known type.")
            }
            Error::EOFBeforeClosingParen => write!(
                f,
                "The token stream ended before a closing parentheses was found."
            ),
            Error::MPRReadNoTerminated => write!(
                f,
                "The end of the MPR File was reached but the ending ! is missing."
            ),
        }
    }
}

// pub enum MPRParseError {
//     MPRRead(String),
// }

// impl fmt::Display for MPRParseError {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         match self {
//             MPRParseError::MPRRead(e) => write!(f, "Corrupted MPR File! {}", e),
//         }
//     }
// }
