use crate::error::*;
use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    Equal,
    GreaterThan,
    GreaterThanOrEqual,
    LessThan,
    LessThanOrEqual,
    NotEqual,
    Abs,
    Add,
    And,
    ArcCos,
    ArcSin,
    ArcTan,
    CloseParen,
    Constant,
    Cos,
    Divide,
    Else,
    Exp,
    Exponent,
    If,
    Ln,
    Mod,
    Multiply,
    Not,
    Number,
    OpenParen,
    Or,
    Prec,
    Relative,
    Sin,
    Sqrt,
    Standard,
    Subtract,
    Tan,
    Then,
    Variable,
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

// Track the token type, value and other metadata
#[derive(Debug, Clone)]
pub struct Token {
    pub typ: TokenType,
    pub val: String,
    pub start: usize,
    pub end: usize,
    pub line: usize,
    pub column: usize,
}

impl Token {
    pub fn new(
        typ: TokenType,
        val: String,
        start: usize,
        end: usize,
        line: usize,
        column: usize,
    ) -> Token {
        Token {
            typ,
            val,
            start,
            end,
            line,
            column,
        }
    }

    pub fn error(&self) -> Error {
        Error::UnexpectedToken {
            val: self.val.clone(),
            typ: self.typ.to_string(),
            start: self.start,
            end: self.end,
            line: self.line,
            column: self.column,
        }
    }
}

// Struct to handle tokenizing a passed enumerable char string
#[derive(Debug)]
pub struct Tokenizer<'a> {
    position: usize,
    peeked: Option<Box<Token>>,
    chars: std::iter::Peekable<std::iter::Enumerate<std::str::Chars<'a>>>,
    current_line: usize,
    current_column: usize,
}

impl<'a> Tokenizer<'a> {
    pub fn new(source: &'a str) -> Tokenizer {
        Tokenizer {
            position: 0,
            peeked: None,
            chars: source.chars().enumerate().peekable(),
            current_line: 1,
            current_column: 1,
        }
    }

    fn peek_char(&mut self) -> Option<&char> {
        match self.chars.peek() {
            Some((_, c)) => Some(c),
            None => None,
        }
    }

    fn next_char(&mut self) -> Option<char> {
        match self.chars.next() {
            Some((_, e)) => {
                self.position += 1;
                match e {
                    '\n' => {
                        self.current_line += 1;
                        self.current_column = 1;
                    }
                    _ => {
                        self.current_column += 1;
                    }
                }
                Some(e)
            }
            None => None,
        }
    }

    fn eat_whitespace(&mut self) -> Option<String> {
        let mut ws = self.next_char()?.to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if !c.is_whitespace() {
                break;
            }
            self.next_char();
            ws = format!("{}{}", ws, c);
            peek = self.peek_char().cloned();
        }

        Some(ws)
    }

    fn eat_identifier(&mut self) -> Option<String> {
        let mut identifier = self.next_char()?.to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            match c {
                'a'..='z' => {}
                'A'..='Z' => {}
                '0'..='9' => {}
                '_' => {}
                _ => break,
            }
            self.next_char();
            identifier = format!("{}{}", identifier, c);
            peek = self.peek_char().cloned();
        }

        Some(identifier)
    }

    fn eat_digits(&mut self) -> Option<String> {
        let mut number = self.next_char()?.to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if !c.is_digit(10) && c != '.' && c != 'e' && c != 'E' {
                break;
            }
            self.next_char();
            number = format!("{}{}", number, c);
            peek = self.peek_char().cloned();
        }

        Some(number)
    }

    pub fn peek(&mut self) -> &Option<Box<Token>> {
        if self.peeked.is_some() {
            return &self.peeked;
        }

        if let Some(c) = self.peek_char() {
            if c.is_whitespace() {
                self.eat_whitespace();
            }
        }

        let start = self.position;
        let line = self.current_line;
        let column = self.current_column;
        let peeked_char = self.peek_char().cloned();

        let token = match peeked_char {
            Some(c) => match c {
                n if n.is_digit(10) || n == '.' => {
                    let number = self.eat_digits().unwrap();
                    Some(Token::new(
                        TokenType::Number,
                        number,
                        start,
                        self.position,
                        line,
                        column,
                    ))
                }
                'A'..='Z' | 'a'..='z' | '0'..='9' | '_' => {
                    let identifier = self.eat_identifier().unwrap();
                    match identifier.as_ref() {
                        "_cc" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_cw" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_CC" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_CW" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_mirror" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_nonmirror" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_ymirror" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_nonymirror" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_ok" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_no" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_lf" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "_ri" => Some(Token::new(
                            TokenType::Constant,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "@" => Some(Token::new(
                            TokenType::Relative,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "ABS" => Some(Token::new(
                            TokenType::Abs,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "SIN" => Some(Token::new(
                            TokenType::Sin,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "COS" => Some(Token::new(
                            TokenType::Cos,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "TAN" => Some(Token::new(
                            TokenType::Tan,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "ARCSIN" => Some(Token::new(
                            TokenType::ArcSin,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "ARCCOS" => Some(Token::new(
                            TokenType::ArcCos,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "ARCTAN" => Some(Token::new(
                            TokenType::ArcTan,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "EXP" => Some(Token::new(
                            TokenType::Exp,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "LN" => Some(Token::new(
                            TokenType::Ln,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "SQRT" => Some(Token::new(
                            TokenType::Sqrt,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "MOD" => Some(Token::new(
                            TokenType::Mod,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "PREC" => Some(Token::new(
                            TokenType::Prec,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "AND" => Some(Token::new(
                            TokenType::And,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "OR" => Some(Token::new(
                            TokenType::Or,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "NOT" => Some(Token::new(
                            TokenType::Not,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "IF" => Some(Token::new(
                            TokenType::If,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "THEN" => Some(Token::new(
                            TokenType::Then,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "ELSE" => Some(Token::new(
                            TokenType::Else,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        "STANDARD" => Some(Token::new(
                            TokenType::Standard,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                        _ => Some(Token::new(
                            TokenType::Variable,
                            identifier.to_string(),
                            start,
                            self.position,
                            line,
                            column,
                        )),
                    }
                }
                '=' => Some(Token::new(
                    TokenType::Equal,
                    self.next_char().unwrap().to_string(),
                    start,
                    self.position - 1,
                    line,
                    column,
                )),
                '>' => {
                    let tkn = self.next_char().unwrap().to_string();
                    match self.peek_char() {
                        Some('=') => Some(Token::new(
                            TokenType::GreaterThanOrEqual,
                            format!("{}{}", tkn, self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column,
                        )),
                        _ => Some(Token::new(
                            TokenType::GreaterThan,
                            tkn,
                            start,
                            self.position - 1,
                            line,
                            column,
                        )),
                    }
                }
                '<' => {
                    let tkn = self.next_char().unwrap().to_string();
                    match self.peek_char() {
                        Some('=') => Some(Token::new(
                            TokenType::LessThanOrEqual,
                            format!("{}{}", tkn, self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column,
                        )),
                        Some('>') => Some(Token::new(
                            TokenType::NotEqual,
                            format!("{}{}", tkn, self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column,
                        )),
                        _ => Some(Token::new(
                            TokenType::LessThan,
                            tkn,
                            start,
                            self.position - 1,
                            line,
                            column,
                        )),
                    }
                }
                '*' => Some(Token::new(
                    TokenType::Multiply,
                    self.next_char().unwrap().to_string(),
                    start,
                    self.position - 1,
                    line,
                    column,
                )),
                '/' => Some(Token::new(
                    TokenType::Divide,
                    self.next_char().unwrap().to_string(),
                    start,
                    self.position - 1,
                    line,
                    column,
                )),
                '+' => Some(Token::new(
                    TokenType::Add,
                    self.next_char().unwrap().to_string(),
                    start,
                    self.position - 1,
                    line,
                    column,
                )),
                '-' => Some(Token::new(
                    TokenType::Subtract,
                    self.next_char().unwrap().to_string(),
                    start,
                    self.position - 1,
                    line,
                    column,
                )),
                '^' => Some(Token::new(
                    TokenType::Exponent,
                    self.next_char().unwrap().to_string(),
                    start,
                    self.position - 1,
                    line,
                    column,
                )),
                '(' => Some(Token::new(
                    TokenType::OpenParen,
                    self.next_char().unwrap().to_string(),
                    start,
                    self.position - 1,
                    line,
                    column,
                )),
                ')' => Some(Token::new(
                    TokenType::CloseParen,
                    self.next_char().unwrap().to_string(),
                    start,
                    self.position - 1,
                    line,
                    column,
                )),
                _ => panic!("OH NO"),
            },
            None => None,
        };

        self.peeked = match token {
            Some(t) => Some(Box::new(t)),
            None => None,
        };

        &self.peeked
    }

    pub fn peek_owned(&mut self) -> Option<Token> {
        match self.peek() {
            Some(t) => Some(*t.clone()),
            None => None,
        }
    }
}

impl<'a> Iterator for Tokenizer<'a> {
    type Item = Box<Token>;

    fn next(&mut self) -> Option<Box<Token>> {
        if self.peeked.is_none() {
            self.peek();
        }
        let token = self.peeked.as_ref().cloned();
        self.peeked = None;
        token
    }
}
