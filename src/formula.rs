use std::collections::HashMap;

use crate::error::*;
use crate::mpr::*;
use crate::tokenizer::*;
use crate::process::*;

#[derive(Debug)]
pub enum Node {
    Number(f64),
    Unary(Box<Token>, Box<Node>),
    Binary(Box<Node>, Box<Token>, Box<Node>),
    Application(Box<Token>, Box<Node>),
    Conditional(Box<Token>, Box<Node>, Box<Node>),
}

#[derive(Debug)]
pub struct Parser<'a, 'b> {
    tokens: Tokenizer<'a>,
    env: &'b HashMap<String, f64>,
}

impl<'a, 'b> Parser<'a, 'b> {
    pub fn new(env: &'b HashMap<String, f64>, tokens: Tokenizer<'a>) -> Parser<'a, 'b> {
        Parser { env, tokens }
    }

    pub fn bp(&self, t: &Token) -> usize {
        match t.typ {
            TokenType::CloseParen => 0,
            TokenType::Equal => 10,
            TokenType::GreaterThan => 10,
            TokenType::GreaterThanOrEqual => 10,
            TokenType::LessThan => 10,
            TokenType::LessThanOrEqual => 10,
            TokenType::NotEqual => 10,
            TokenType::And => 20,
            TokenType::Or => 20,
            TokenType::Add => 30,
            TokenType::Subtract => 30,
            TokenType::Multiply => 40,
            TokenType::Divide => 40,
            TokenType::Exponent => 50,
            TokenType::ArcCos => 60,
            TokenType::ArcSin => 60,
            TokenType::ArcTan => 60,
            TokenType::Sin => 60,
            TokenType::Cos => 60,
            TokenType::Tan => 60,
            TokenType::Sqrt => 60,
            TokenType::Abs => 60,
            TokenType::If => 70,
            TokenType::Then => 9,
            TokenType::Else => 5,
            TokenType::OpenParen => 80,
            TokenType::Variable => 100,
            TokenType::Number => 100,
            _ => panic!("Need a bp for the token type: {}", t.typ),
        }
    }

    pub fn nud(&mut self, t: Box<Token>, _bp: usize) -> Result<Node, Error> {
        match t.typ {
            TokenType::Number => Ok(Node::Number(t.val.parse::<f64>().unwrap())),
            TokenType::Add | TokenType::Subtract => {
                let right = self.expr(self.bp(&t))?;
                Ok(Node::Unary(t, Box::new(right)))
            }
            TokenType::Variable => Ok(Node::Number(*self.env.get(&t.val).unwrap())),
            TokenType::OpenParen => {
                let right = self.expr(0)?;
                match self.tokens.next() {
                    Some(ref t) if t.typ == TokenType::CloseParen => Ok(right),
                    _ => Err(Error::EOFBeforeClosingParen),
                }
            }
            TokenType::Sin
            | TokenType::Cos
            | TokenType::Tan
            | TokenType::ArcSin
            | TokenType::ArcCos
            | TokenType::ArcTan
            | TokenType::Exp
            | TokenType::Ln
            | TokenType::Sqrt
            | TokenType::Mod
            | TokenType::Prec
            | TokenType::Abs => {
                let right = self.expr(self.bp(&t))?;
                Ok(Node::Application(t, Box::new(right)))
            }
            TokenType::Constant => match t.val.as_str() {
                "_cc" => Ok(Node::Number(0.0)),
                "_cw" => Ok(Node::Number(1.0)),
                "_CC" => Ok(Node::Number(2.0)),
                "_CW" => Ok(Node::Number(3.0)),
                "_ok" => Ok(Node::Number(1.0)),
                "_no" => Ok(Node::Number(0.0)),
                "_lf" => Ok(Node::Number(1.0)),
                "_ri" => Ok(Node::Number(2.0)),
                _ => Err(Error::UnmatchedTokenType),
            },
            TokenType::If => {
                let right = self.expr(0)?;
                Ok(right)
            }
            _ => Err(Error::UnmatchedTokenType),
        }
    }

    pub fn led(&mut self, left: Node, op: Box<Token>, bp: usize) -> Result<Node, Error> {
        match op.typ {
            TokenType::Add
            | TokenType::Subtract
            | TokenType::Multiply
            | TokenType::Divide
            | TokenType::LessThan
            | TokenType::GreaterThan
            | TokenType::GreaterThanOrEqual
            | TokenType::LessThanOrEqual
            | TokenType::NotEqual => {
                let right = self.expr(bp)?;
                Ok(Node::Binary(Box::new(left), op, Box::new(right)))
            }
            TokenType::Exponent => {
                let right = self.expr(bp - 1)?;
                Ok(Node::Binary(Box::new(left), op, Box::new(right)))
            }
            TokenType::Then | TokenType::Else => {
                let right = self.expr(bp)?;
                Ok(Node::Conditional(op, Box::new(left), Box::new(right)))
            }
            _ => Err(op.error()),
        }
    }

    pub fn expr(&mut self, rbp: usize) -> Result<Node, Error> {
        let first_t = self.tokens.next().ok_or(Error::UnexpectedTokenEnd)?;
        let first_t_bp = self.bp(&first_t);

        let mut left = self.nud(first_t, first_t_bp)?;
        if self.tokens.peek().is_none() {
            return Ok(left);
        }

        let mut peeked = self.tokens.peek_owned();
        while peeked.is_some() && rbp < self.bp(&peeked.unwrap()) {
            let op = self.tokens.next().ok_or(Error::UnexpectedTokenEnd)?;
            let op_bp = self.bp(&op);
            left = self.led(left, op, op_bp)?;

            if self.tokens.peek().is_none() {
                break;
            }

            peeked = self.tokens.peek_owned();
        }

        Ok(left)
    }

    pub fn parse(&mut self) -> Result<Node, Error> {
        let result = self.expr(0);
        match self.tokens.next() {
            Some(t) => Err(t.error()),
            None => result,
        }
    }
}

pub fn eval(node: Node) -> Result<f64, String> {
    match node {
        Node::Number(n) => Ok(n),
        Node::Unary(op, node) => match op.typ {
            TokenType::Subtract => Ok(-eval(*node)?),
            TokenType::Add => Ok(eval(*node)?),
            _ => Err("Not a unary operator".to_owned()),
        },
        Node::Binary(left, op, right) => match op.typ {
            TokenType::Add => Ok(eval(*left)? + eval(*right)?),
            TokenType::Subtract => Ok(eval(*left)? - eval(*right)?),
            TokenType::Multiply => Ok(eval(*left)? * eval(*right)?),
            TokenType::Divide => Ok(eval(*left)? / eval(*right)?),
            TokenType::Exponent => Ok(eval(*left)?.powf(eval(*right)?)),
            TokenType::LessThan => {
                if eval(*left)? < eval(*right)? {
                    Ok(1.0)
                } else {
                    Ok(0.0)
                }
            }
            TokenType::GreaterThan => {
                if eval(*left)? > eval(*right)? {
                    Ok(1.0)
                } else {
                    Ok(0.0)
                }
            }
            TokenType::Equal => {
                
                if (eval(*left)? - eval(*right)?).abs() < f64::EPSILON {
                    Ok(1.0)
                } else {
                    Ok(0.0)
                }
            }
            TokenType::GreaterThanOrEqual => {
                if eval(*left)? >= eval(*right)? {
                    Ok(1.0)
                } else {
                    Ok(0.0)
                }
            }
            TokenType::LessThanOrEqual => {
                if eval(*left)? <= eval(*right)? {
                    Ok(1.0)
                } else {
                    Ok(0.0)
                }
            }
            TokenType::NotEqual => {
                if (eval(*left)? - eval(*right)?).abs() > f64::EPSILON {
                    Ok(1.0)
                } else {
                    Ok(0.0)
                }
            }
            _ => Err("Not a binary operator".to_owned()),
        },
        Node::Application(token, arg) => match token.typ {
            TokenType::Abs => Ok(eval(*arg)?.abs()),
            TokenType::Sin => Ok(eval(*arg)?.sin()),
            TokenType::Cos => Ok(eval(*arg)?.cos()),
            TokenType::Tan => Ok(eval(*arg)?.tan()),
            TokenType::ArcSin => Ok(eval(*arg)?.asin()),
            TokenType::ArcCos => Ok(eval(*arg)?.acos()),
            TokenType::ArcTan => Ok(eval(*arg)?.atan()),
            TokenType::Ln => Ok(eval(*arg)?.ln()),
            TokenType::Sqrt => Ok(eval(*arg)?.sqrt()),
            _ => Err(format!("Unknown function {:?}", token)),
        },
        Node::Conditional(_condition, truthy, falsy) => {
            if let Node::Conditional(_token, condition, truthy) = *truthy {
                let condition = eval(*condition)?;
                if (condition - 1.0).abs() < f64::EPSILON {
                    Ok(eval(*truthy)?)
                } else {
                    Ok(eval(*falsy)?)
                }
            } else {
                Err(format!("Error in IF Condition {:?}", truthy))
            }
        }
    }
}

pub fn eval_globals(mpr: MPR) -> HashMap<String, f64> {
    let mut env: HashMap<String, f64> = HashMap::new();

    for var in mpr.variables {
        let tokens = Tokenizer::new(&var.value);
        let parsed = Parser::new(&env, tokens).parse();
        let evaled = eval(parsed.unwrap()).unwrap();

        env.insert(var.name, evaled);
    }

    env
}

pub fn evaluate_variables(mpr: MPR) -> MPR {

    let mut env = mpr.environment.clone();
        
    if let Some(bsx) = &mpr.header._bsx {
        env.insert("_BSX".to_string(),bsx.parse::<f64>().unwrap());
    } 
    
    if let Some(bsy) = &mpr.header._bsy {
        env.insert("_BSY".to_string(),bsy.parse::<f64>().unwrap());
    }
    
    if let Some(bsz) = &mpr.header._bsz {
        env.insert("_BSZ".to_string(),bsz.parse::<f64>().unwrap());
    }

    for var in &mpr.variables {
        let tokens = Tokenizer::new(&var.value);
        let parsed = Parser::new(&env, tokens).parse();
        let evaled = eval(parsed.unwrap()).unwrap();

        env.insert(var.name.clone(), evaled);
    }

    mpr.new_environment(env)
}

pub fn evaluate(env: &HashMap<String, f64>, formula: Option<String>) -> Option<String> {
    if let Some(formula) = formula {
        let tokens = Tokenizer::new(&formula);
        let parsed = Parser::new(env, tokens).parse();
        Some(eval(parsed.unwrap()).unwrap().to_string())
    } else {
        None
    }
}

pub fn eval_process(mpr: MPR) -> MPR {
    let process = mpr.processes.clone();
    let mut p: Vec<Process> = Vec::new();
    
    for x in process {
        match x {
            Process::WerkStck { id, name, la, br, di, ax, ay, rl, rb, rnx,
                                rny, rnz, fnx, fny } => {
                p.push(Process::WerkStck {
                    id,
                    name,
                    la: evaluate(&mpr.environment,la),
                    br: evaluate(&mpr.environment,br),
                    di: evaluate(&mpr.environment,di),
                    ax: evaluate(&mpr.environment,ax),
                    ay: evaluate(&mpr.environment,ay),
                    rl: evaluate(&mpr.environment,rl),
                    rb: evaluate(&mpr.environment,rb),
                    rnx: evaluate(&mpr.environment,rnx),
                    rny: evaluate(&mpr.environment,rny),
                    rnz: evaluate(&mpr.environment,rnz),
                    fnx: evaluate(&mpr.environment,fnx),
                    fny: evaluate(&mpr.environment,fny),
                })
            },
            Process::Comment { id, name, question, en, hp, sp, yve, mx, my, mz,
                               asg, rsel, rwid, kat, mnm, km, ori, } => {
                p.push(Process::Comment {
                    id,
                    name,
                    question: evaluate(&mpr.environment,question),
                    en,
                    hp,
                    sp,
                    yve,
                    mx,
                    my,
                    mz,
                    asg,
                    rsel,
                    rwid,
                    kat,
                    mnm,
                    km,
                    ori,
                })
            },
            _ => ()
        }
    }

    MPR {
        header: mpr.header,
        variables: mpr.variables,
        coordinates: mpr.coordinates,
        contours: mpr.contours,
        processes: p,
        embeds: mpr.embeds,
        environment: mpr.environment,
    }
}
