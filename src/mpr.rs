use crate::contour::Contour;
use crate::coordinate::Coordinate;
use crate::header::Header;
use crate::process::Process;
use crate::variable::Variable;
use crate::initial_parse::PreParseMPR;

use std::{
    collections::HashMap,
    error::Error,
    path::PathBuf,
};

#[derive(Default, Debug, Clone)]
pub struct MPR {
    pub header: Header,
    pub variables: Vec<Variable>,
    pub coordinates: Vec<Coordinate>,
    pub contours: Vec<Contour>,
    pub processes: Vec<Process>,
    pub embeds: Vec<MPR>,
    pub environment: HashMap<String, f64>,
}

impl MPR {
    pub fn parse(initial_mpr: PreParseMPR)
                 -> Result<MPR,Box<dyn Error + Send + Sync>> {
        let header = Header::new(initial_mpr.header);
        let variables = Variable::new(initial_mpr.variable)?;
        let coordinates = Coordinate::new(initial_mpr.coordinates)?;
        let contours = Contour::new(initial_mpr.contours)?;
        let mut processes: Vec<Process> = Vec::new();
        let mut embeds: Vec<MPR> = Vec::new();

        let p = match Process::new(initial_mpr.processes) {
            Ok(p) => p,
            Err(e) => return Err(Box::from(format!("Error {:?}",e)))
        };
        let mut embn: usize = 0;
        let mut embedded_components: HashMap<String, usize> = HashMap::new();

        if !initial_mpr.embeds.is_empty() {
            for embed in initial_mpr.embeds.clone() {
                embeds.push(MPR::parse(embed)?);
            }
        }

        
        for e in p {
            //TODO: REALLY NEED TO CHECK THE EMBBEDED ARE WORKING
            if !initial_mpr.embeds.is_empty() {
                if let Process::Komponente {
                    id,
                    name,
                    question,
                    en,
                    hp,
                    sp,
                    yve,
                    mx,
                    my,
                    mz,
                    asg,
                    rsel,
                    rwid,
                    kat,
                    mnm,
                    ori,
                    ko,
                    xa,
                    ya,
                    va,
                    em,
                    r#in,
                    za,
                    wi,
                    mpr: _,
                } = e.clone()
                {
                    if em.clone().unwrap() == "1" {
                        let nin: String = r#in.to_owned().unwrap().to_string();
                        let empr = match embedded_components.get(&nin) {
                            Some(ebcs) => embeds[*ebcs].clone(),
                            None => {
                                embn += 1;
                                embedded_components.insert(nin, embn);
                                embeds[embn - 1].clone()
                            }
                        };
                        
                        processes.push(Process::Komponente {
                            id,
                            name,
                            question,
                            en,
                            hp,
                            sp,
                            yve,
                            mx,
                            my,
                            mz,
                            asg,
                            rsel,
                            rwid,
                            kat,
                            mnm,
                            ori,
                            ko,
                            xa,
                            ya,
                            va,
                            em,
                            r#in: if r#in == None {
                                None
                            } else {
                                Some(r#in.unwrap().clone())
                            },
                            za,
                            wi,
                            mpr: Some(Box::new(empr)),
                        });
                    } else {
                        processes.push(e);
                    }
                } else {
                    processes.push(e);
                }
            } else {
                processes.push(e);
            }
        }
        
        Ok(MPR {
            header,
            variables,
            coordinates,
            contours,
            processes,
            embeds,
            environment: HashMap::new(),
        })
        
    }
    
    pub fn new(path: PathBuf) -> Result<MPR,Box<dyn Error + Send + Sync>> {
        let initial_mpr = PreParseMPR::new(path)?;
        let mpr = MPR::parse(initial_mpr)?;
        Ok(mpr)
    }
    
    pub fn new_environment(self, env: HashMap<String, f64>) -> Self {
        MPR {
            header: self.header,
            variables: self.variables,
            coordinates: self.coordinates,
            contours: self.contours,
            processes: self.processes,
            embeds: self.embeds,
            environment: env,
        }
    }
}
