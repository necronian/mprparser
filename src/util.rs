/// Takes a string with the format X="Y" and returns a tuple with the key and
/// the value from inside the quotes
pub fn parse_kv(l: &str) -> Option<(&str, &str)> {
    let idx = match l.find('=') {
        Some(idx) => idx,
        None => return None,
    };

    let (k, v) = l.split_at(idx);
    let key = k.trim();
    // Get the value slice less the = sign from the first split
    let mut value = v.trim().get(1..).unwrap();

    // Extract the value from the "" it is surrounded in
    if value.starts_with('"') && value.ends_with('"') {
        value = value.get(1..value.len() - 1).expect("Error getting KV values from parse_kv");
    }

    Some((key, value))
}
