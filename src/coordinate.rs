use std::{
    collections::HashMap,
    error::Error,
};
use crate::util::parse_kv;

/// The list of coordinate systems will be indicated by descriptor '[K' in the
/// first and second column of a row. After that the coordinate systems starts
/// with descriptor <00.

// There are 20 predefined coordinate systems: (located at the corner points
// of processed part)
// | id | plane   | origin      | rotation | handedness |
// |----+---------+-------------+----------+------------|
// | 00 | XY      | X=0 Y=0 Z=0 |        0 | right      |
// | 01 | XY      | X=L Y=0 Z=0 |        0 | left       |
// | 02 | XY      | X=L Y=B Z=0 |      180 | right      |
// | 03 | XY      | X=0 Y=B Z=0 |      180 | left       |
// | A0 | XZ (Y+) | X=0 Y=0 Z=0 |        0 | right      |
// | A1 | XZ (Y+) | X=L Y=0 Z=0 |        0 | left       |
// | A2 | XZ (Y+) | X=L Y=0 Z=D |      180 | right      |
// | A3 | XZ (Y+) | X=0 Y=0 Z=D |      180 | left       |
// | B0 | YZ (X-) | X=L Y=0 Z=0 |        0 | right      |
// | B1 | YZ (X-) | X=L Y=B Z=0 |        0 | left       |
// | B2 | YZ (X-) | X=L Y=B Z=D |      180 | right      |
// | B3 | YZ (X-) | X=L Y=0 Z=D |      180 | left       |
// | C0 | XZ (Y-) | X=L Y=B Z=0 |        0 | right      |
// | C1 | XZ (Y-) | X=0 Y=B Z=0 |        0 | left       |
// | C2 | XZ (Y-) | X=0 Y=B Z=D |      180 | right      |
// | C3 | XZ (Y-) | X=L Y=B Z=D |      180 | left       |
// | D0 | YZ (X+) | X=0 Y=B Z=0 |        0 | right      |
// | D1 | YZ (X+) | X=0 Y=0 Z=0 |        0 | left       |
// | D2 | YZ (X+) | X=0 Y=0 Z=D |      180 | right      |
// | D3 | YZ (X+) | X=0 Y=B Z=D |      180 | left       |
    
#[derive(Debug, Default, Clone)]
pub struct Coordinate {
    // Number of coordinate system. 4 to 99 are the valid values at
    // present. The coordinate systems 0 to 3 are predefined and won't be
    // saved in the MPR-file.
    nr: String,
    // X-Position refers to coordinate system 0.
    xp: String,
    // Y-Position refers to coordinate system 0.
    yp: String,
    // Z-Position refers to coordinate system 0.
    zp: String,
    // Turn angle 1 around Z-Axis of coordinate system 0.
    d1: String,
    // Tilt angle around X-Axis of new coordinate system (after turning around D1)
    ki: String,
    // Turn angle 2 around Z-Axis of new coordinate system (after turning
    // around D1 and tilting around KI)
    d2: String,
    // Mirror. Mirrored coordinate system or vice versa. = 0 = > unmirrored
    // (right handed, like in coordinate system 0) = 1 = > mirrored (left
    // handed, like in coordinate system 1)
    mi: String,
    xf: String,    
    yf: String,    
    zf: String,
}

impl Coordinate {
    pub fn new(h: Vec<String>)
               -> Result<Vec<Coordinate>,Box<dyn Error + Send + Sync>> {
        let mut coordinates: Vec<Coordinate> = Vec::new();
        let mut lines = h.iter().peekable();

        lines.next();

        let process: Vec<PreProcess> = pre_parse_process(&mut lines);

        for coordinate in process {
            let mut p = Coordinate::default();
            for key in coordinate.values.keys() {
                match key.as_str() {
                    "NR" => p.nr = coordinate.values.get("NR").unwrap().to_string(),
                    "XP" => p.xp = coordinate.values.get("XP").unwrap().to_string(),
                    "XF" => p.xf = coordinate.values.get("XF").unwrap().to_string(),
                    "YP" => p.yp = coordinate.values.get("YP").unwrap().to_string(),
                    "YF" => p.yf = coordinate.values.get("YF").unwrap().to_string(),
                    "ZP" => p.zp = coordinate.values.get("ZP").unwrap().to_string(),
                    "ZF" => p.zf = coordinate.values.get("ZF").unwrap().to_string(),
                    "D1" => p.d1 = coordinate.values.get("D1").unwrap().to_string(),
                    "KI" => p.ki = coordinate.values.get("KI").unwrap().to_string(),
                    "D2" => p.d2 = coordinate.values.get("D2").unwrap().to_string(),
                    "MI" => p.mi = coordinate.values.get("MI").unwrap().to_string(),
                    unmatched => return Err(Box::from(
                        format!("Unknown Coordinate System Value! {}",
                                unmatched))),
                }
            }
            coordinates.push(p);
        }

        Ok(coordinates)
    }
}

fn pre_parse_process(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<PreProcess> {
    let mut items: Vec<PreProcess> = Vec::new();

    while iter.peek() != None {
        let proc = read_process(iter);
        let mut p = proc.iter();
        let f = p.next().unwrap();
        let (i, n) = f.split_at(4);
        let name = n.trim();
        let id = i.get(1..).unwrap();
        let mut hm: HashMap<String, String> = HashMap::new();

        for item in p {
            if let Some((k, v)) = parse_kv(item) {
                hm.insert(k.to_string(), v.to_string());
            }
        }

        items.push(PreProcess {
            id: id.to_string(),
            name: name.to_string(),
            values: hm,
        });
    }

    items
}

#[derive(Debug)]
struct PreProcess {
    id: String,
    name: String,
    values: HashMap<String, String>,
}

fn read_process(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();

    output.push(iter.next().unwrap().to_string());

    while !(iter.peek() == None || iter.peek().unwrap().starts_with('<')) {
        output.push(iter.next().unwrap().to_string());
    }

    output
}
