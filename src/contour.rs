use std::{
    collections::HashMap,
    error::Error,
};

use crate::util::parse_kv;

/// Contours consist of contour elements. These elements will be used for
/// contour macros. The elements in the MPR-File will be numbered and
/// identified

#[derive(Default, Debug, Clone)]
pub struct Element {
    pub id: String,
    pub typ: ElementType,
    pub values: HashMap<String, String>,
}

#[derive(Default, Debug, Clone)]
pub struct Contour {
    pub id: String,
    pub elements: Vec<Element>,
}

#[derive(Debug, Clone)]
pub enum ElementType {
    /// Start point of a contour. Keyword is KP. Coordinates are relative to
    /// the respective coordinate system.
    Point {
        name: String,
        x: Option<String>,
        y: Option<String>,
        z: Option<String>,
        // Comment, possibly containing NEST/NESTSEC keywords
        kp: Option<String>,
        // Id of coordinate system
        ko: Option<String>,
        px: Option<String>,
        py: Option<String>,
        pz: Option<String>,
        pko: Option<String>,
    },
    Line {
        name: String,
        x: Option<String>,
        y: Option<String>,
        z: Option<String>,
        // Length of the line in the XY plane
        l: Option<String>,
        // Angle of the line in relation to the X axis
        wi: Option<String>,
        // The gradient angle of the line in Z and in relation to the XY plane
        wz: Option<String>,
        px: Option<String>,
        py: Option<String>,
        pz: Option<String>,
        pl: Option<String>,
        pwi: Option<String>,
        pwz: Option<String>,
    },
    Arc {
        name: String,
        x: Option<String>,
        y: Option<String>,
        x2: Option<String>,
        y2: Option<String>,
        z: Option<String>,
        // Circle midpoint coordinate in X
        i: Option<String>,
        // Circle midpoint coordinate in Y
        j: Option<String>,
        // Rotary direction =0 = > Arc < = 180 degree clockwise =1 = > Arc < =
        // 180 degree counterclockwise =2 = > Arc > 180 degree clockwise =3 =
        // > Arc > 180 degree counterclockwise
        ds: Option<String>,
        // Radius
        r: Option<String>,
        // Start angle in X/Y-level
        wi: Option<String>,
        // End angle in X/Y-level
        wo: Option<String>,
        // Angle to X/Y-level of connecting line from start to end point.
        wz: Option<String>,
        // Angle to X/Y-level on the arc path.
        waz: Option<String>,
        px: Option<String>,
        py: Option<String>,
        px2: Option<String>,
        py2: Option<String>,
        pz: Option<String>,
        pi: Option<String>,
        pj: Option<String>,
        pds: Option<String>,
        pr: Option<String>,
        pwi: Option<String>,
        pwo: Option<String>,
        pwz: Option<String>,
        pwaz: Option<String>,
    },
    Fillet {
        name: String,
        x: Option<String>,
        y: Option<String>,
        x2: Option<String>,
        y2: Option<String>,
        z: Option<String>,
        i: Option<String>,
        j: Option<String>,
        ds: Option<String>,
        // Radius
        r: Option<String>,
        wi: Option<String>,
        wo: Option<String>,
        wz: Option<String>,
        waz: Option<String>,
        px: Option<String>,
        py: Option<String>,
        px2: Option<String>,
        py2: Option<String>,
        pz: Option<String>,
        pi: Option<String>,
        pj: Option<String>,
        pds: Option<String>,
        pr: Option<String>,
        pwi: Option<String>,
        pwo: Option<String>,
        pwz: Option<String>,
        pwaz: Option<String>,
    },
    Chamfer {
        name: String,
        x: Option<String>,
        y: Option<String>,
        z: Option<String>,
        // Length of chamfer
        l: Option<String>,
        // Start angle
        wi: Option<String>,
        wz: Option<String>,
        // Width of chamfer
        fb: Option<String>,
        // End angle
        wo: Option<String>,
        // Shrinking of predecessor element.
        kx: Option<String>,
        // Shrinking of successor element.
        ky: Option<String>,
        px: Option<String>,
        py: Option<String>,
        pz: Option<String>,
        pl: Option<String>,
        pwi: Option<String>,
        pwz: Option<String>,
        pfb: Option<String>,
        pwo: Option<String>,
        pkx: Option<String>,
        pky: Option<String>,
    },
    SplitLine {
        name: String,
        x: Option<String>,
        y: Option<String>,
        z: Option<String>,
        l: Option<String>,
        wi: Option<String>,
        wz: Option<String>,
        // Positive value for AV/AK: Distance from the predecessor
        // point. Negative value for AV/AK: Distance from the successor
        // point. Relative value declaration relates to element
        // length. E.g. AV=@0.5 means splitting in middle of element.
        // -------------------------------------------------------------
        // Distance from real point
        av: Option<String>,
        // Distance from construction point
        ak: Option<String>,
        px: Option<String>,
        py: Option<String>,
        pz: Option<String>,
        pl: Option<String>,
        pwi: Option<String>,
        pwz: Option<String>,
        pav: Option<String>,
        pak: Option<String>,
    },
    SplitArc {
        name: String,
        x: Option<String>,
        y: Option<String>,
        z: Option<String>,
        i: Option<String>,
        j: Option<String>,
        // Positive value for AV/AK: Distance from the predecessor
        // point. Negative value for AV/AK: Distance from the successor
        // point. Relative value declaration relates to element
        // length. E.g. AV=@0.5 means splitting in middle of element.
        // -------------------------------------------------------------
        // Distance from real point
        av: Option<String>,
        // Distance from construction point
        ak: Option<String>,
        ds: Option<String>,
        waz: Option<String>,
        wo: Option<String>,
        wi: Option<String>,
        r: Option<String>,
        px: Option<String>,
        py: Option<String>,
        pz: Option<String>,
        pi: Option<String>,
        pj: Option<String>,
        pav: Option<String>,
        pak: Option<String>,
        pds: Option<String>,
        pwaz: Option<String>,
        pwo: Option<String>,
        pwi: Option<String>,
        pr: Option<String>,
    },
}

impl Default for ElementType {
    fn default() -> Self {
        ElementType::Point {
            name: "".to_string(),
            x: Some("".to_string()),
            y: Some("".to_string()),
            z: Some("".to_string()),
            kp: Some("".to_string()),
            ko: Some("".to_string()),
            px: Some("".to_string()),
            py: Some("".to_string()),
            pz: Some("".to_string()),
            pko: Some("".to_string()),
        }
    }
}

impl Contour {
    pub fn new(c: Vec<String>) -> Result<Vec<Contour>,Box<dyn Error + Send + Sync>> {
        let mut cts = c.iter().peekable();
        let mut contours: Vec<Contour> = Vec::new();
        
        while cts.peek() != None {
            let mut ct = Contour::default();
            let mut elem: Vec<Element> = Vec::new();
            let cont = read_contour(&mut cts);
            let mut ctis = cont.iter().peekable();
            let ctidx = ctis.next().unwrap().get(1..).unwrap().to_string();
            
            ct.id = ctidx;
            
            while ctis.peek() != None {
                let items = read_contour_items(&mut ctis);
                let element = parse_contour_element(items);
                elem.push(element);
            }
            
            ct.elements = elem;
            contours.push(ct);
        }
        
        Ok(contours)
    }
}

fn parse_contour_element(i: Vec<String>) -> Element {
    let mut elem = Element::default();
    let mut it = i.iter();
    let mut vals: HashMap<String, String> = HashMap::new();

    let id = it.next().unwrap().get(2..).unwrap().to_string();
    let itdat = it.next().unwrap().trim();
    let (tp, td) = if itdat.len() == 2 {
        (itdat, "")
    } else {
        itdat.split_at(3)
    };
    let desc = td.trim();

    elem.id = id;

    for line in it {
        if let Some((k, v)) = parse_kv(line) {
            vals.insert(k.to_string(), v.to_string());
        }
    }

    let typ = match tp.trim() {
        "KP" => ElementType::Point {
            name: desc.to_string(),
            x: vals.remove("X"),
            y: vals.remove("Y"),
            z: vals.remove("Z"),
            kp: vals.remove("KP"),
            ko: vals.remove("KO"),
            px: vals.remove(".X"),
            py: vals.remove(".Y"),
            pz: vals.remove(".Z"),
            pko: vals.remove(".KO"),
        },
        "KL" => ElementType::Line {
            name: desc.to_string(),
            x: vals.remove("X"),
            y: vals.remove("Y"),
            z: vals.remove("Z"),
            l: vals.remove("L"),
            wi: vals.remove("WI"),
            wz: vals.remove("WZ"),
            px: vals.remove(".X"),
            py: vals.remove(".Y"),
            pz: vals.remove(".Z"),
            pl: vals.remove(".L"),
            pwi: vals.remove(".WI"),
            pwz: vals.remove(".WZ"),
        },
        "KA" => ElementType::Arc {
            name: desc.to_string(),
            x: vals.remove("X"),
            y: vals.remove("Y"),
            x2: vals.remove("X2"),
            y2: vals.remove("Y2"),
            z: vals.remove("Z"),
            i: vals.remove("I"),
            j: vals.remove("J"),
            ds: vals.remove("DS"),
            r: vals.remove("R"),
            wi: vals.remove("WI"),
            wo: vals.remove("WO"),
            wz: vals.remove("WZ"),
            waz: vals.remove("WAZ"),
            px: vals.remove(".X"),
            py: vals.remove(".Y"),
            px2: vals.remove(".X2"),
            py2: vals.remove(".Y2"),
            pz: vals.remove(".Z"),
            pi: vals.remove(".I"),
            pj: vals.remove(".J"),
            pds: vals.remove(".DS"),
            pr: vals.remove(".R"),
            pwi: vals.remove(".WI"),
            pwo: vals.remove(".WO"),
            pwz: vals.remove(".WZ"),
            pwaz: vals.remove(".WAZ"),
        },
        "KR" => ElementType::Fillet {
            name: desc.to_string(),
            x: vals.remove("X"),
            y: vals.remove("Y"),
            x2: vals.remove("X2"),
            y2: vals.remove("Y2"),
            z: vals.remove("Z"),
            i: vals.remove("I"),
            j: vals.remove("J"),
            ds: vals.remove("DS"),
            r: vals.remove("R"),
            wi: vals.remove("WI"),
            wo: vals.remove("WO"),
            wz: vals.remove("WZ"),
            waz: vals.remove("WAZ"),
            px: vals.remove(".X"),
            py: vals.remove(".Y"),
            px2: vals.remove(".X2"),
            py2: vals.remove(".Y2"),
            pz: vals.remove(".Z"),
            pi: vals.remove(".I"),
            pj: vals.remove(".J"),
            pds: vals.remove(".DS"),
            pr: vals.remove(".R"),
            pwi: vals.remove(".WI"),
            pwo: vals.remove(".WO"),
            pwz: vals.remove(".WZ"),
            pwaz: vals.remove(".WAZ"),
        },
        "KF" => ElementType::Chamfer {
            name: desc.to_string(),
            x: vals.remove("X"),
            y: vals.remove("Y"),
            z: vals.remove("Z"),
            l: vals.remove("L"),
            wi: vals.remove("WI"),
            wz: vals.remove("WZ"),
            fb: vals.remove("FN"),
            wo: vals.remove("WO"),
            kx: vals.remove("KX"),
            ky: vals.remove("KY"),
            px: vals.remove(".X"),
            py: vals.remove(".Y"),
            pz: vals.remove(".Z"),
            pl: vals.remove(".L"),
            pwi: vals.remove(".WI"),
            pwz: vals.remove(".WZ"),
            pfb: vals.remove(".FN"),
            pwo: vals.remove(".WO"),
            pkx: vals.remove(".KX"),
            pky: vals.remove(".KY"),
        },
        "KSL" => ElementType::SplitLine {
            name: desc.to_string(),
            x: vals.remove("X"),
            y: vals.remove("Y"),
            z: vals.remove("Z"),
            l: vals.remove("L"),
            wi: vals.remove("WI"),
            wz: vals.remove("WZ"),
            av: vals.remove("AV"),
            ak: vals.remove("AK"),
            px: vals.remove(".X"),
            py: vals.remove(".Y"),
            pz: vals.remove(".Z"),
            pl: vals.remove(".L"),
            pwi: vals.remove(".WI"),
            pwz: vals.remove(".WZ"),
            pav: vals.remove(".AV"),
            pak: vals.remove(".AK"),
        },
        "KSA" => ElementType::SplitArc {
            name: desc.to_string(),
            x: vals.remove("X"),
            y: vals.remove("Y"),
            z: vals.remove("Z"),
            i: vals.remove("I"),
            j: vals.remove("J"),
            av: vals.remove("AV"),
            ak: vals.remove("AK"),
            ds: vals.remove("DS"),
            waz: vals.remove("WAZ"),
            wo: vals.remove("WO"),
            wi: vals.remove("WI"),
            r: vals.remove("R"),
            px: vals.remove(".X"),
            py: vals.remove(".Y"),
            pz: vals.remove(".Z"),
            pi: vals.remove(".I"),
            pj: vals.remove(".J"),
            pav: vals.remove(".AV"),
            pak: vals.remove(".AK"),
            pds: vals.remove(".DS"),
            pwaz: vals.remove(".WAZ"),
            pwo: vals.remove(".WO"),
            pwi: vals.remove(".WI"),
            pr: vals.remove(".R"),
        },
        unmatched => panic!("Unknown Element Type! {}", unmatched),
    };

    if !vals.is_empty() {
        panic!("STILL VALS! TYP: {:?}, VALS: {:?}", typ, vals);
    }

    elem.typ = typ;
    elem.values = vals;

    elem
}

fn read_contour_items(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();
    output.push(iter.next().unwrap().to_string());

    while !(iter.peek() == None || iter.peek().unwrap().starts_with('$')) {
        output.push(iter.next().unwrap().to_string());
    }

    output
}

fn read_contour(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();
    output.push(iter.next().unwrap().to_string());

    while !(iter.peek() == None || iter.peek().unwrap().starts_with(']')) {
        output.push(iter.next().unwrap().to_string());
    }

    output
}
