use std::collections::HashMap;
use crate::formula::evaluate;

/// Contains general variables for all macros.
#[derive(Default, Debug)]
pub struct BaseMacro {
    // Condition - can be used to make the transfer to the NC program
    // dependent on a condition. - "1"
    pub question: Option<String>,
    // Level and Local CS - Defines the level and coordinate system to which
    // the macro refers. Only for macros which are not contour based. - "A00",
    // "B00", "002" etc. See List of Coordi- nate system IDs
    pub ko: Option<String>,
    // Enable - This is similar to the Condition parameter - when set to "0"
    // deactivates the macro. - "1"
    pub en: Option<String>,
}

impl BaseMacro {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            question: hm.remove("??"),
            ko: hm.remove("KO"),
            en: hm.remove("EN"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        self.question = evaluate(env, self.question);
        self
    }
}

#[derive(Default, Debug)]
pub struct BaseMachineSettings {
    // Smooth contour - This function is intended to smooth non-tangential
    // transitions when contours must be processed that are approximated by a
    // large number of relatively short geometry elements. - "0"
    // Without smoothing = "0"
    // Smooth with CONTOUR MODE = "1"
    // Smooth with BSPLINE = "2"
    kg: Option<String>,
    // Maximum track deviation - Maximum permitted deviation of the CNC
    // contour from the programmed contour in millimeters. - "0.2"
    mba: Option<String>,
    // Omission of machining paths shorter than - Contour elements that are
    // shorter than the entered machining path in millimeters are not
    // generated as an NC set. - "0.2"
    asz: Option<String>,
    // Maximum angular deviation - Maximum angle in degrees by which the
    // trailing axes (C value and A value) may deviate from the programmed
    // value. - "3.0"
    mwa: Option<String>,
    // Omission of angular deviations less than - Contour elements where the
    // angular deviations of the trailing axes (C value and A value) are
    // smaller than the specified value in degrees are not generated as an NC
    // set. - "3.0"
    awn: Option<String>,
    // Ramp factor in % - Control of the acceleration behavior. The ramp
    // factor can be set in the NC options for the mpr-file or / and in the
    // individual macro. When the ramp factor is set in both places, then the
    // value within the macro is used for the respective macro. - "100" Off by
    // default
    rp: Option<String>,
    // Dynamic level - Control of the acceleration behavior. The dynamic level
    // can be set in the NC options for the mpr-file or / and in the
    // individual macro. When the dynamic level is set in both places, then
    // the value within the macro is used for the respective macro. - "0" Off
    // by default
    dst: Option<String>,
}

impl BaseMachineSettings {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            kg: hm.remove("KG"),
            mba: hm.remove("MBA"),
            asz: hm.remove("ASZ"),
            mwa: hm.remove("MWA"),
            awn: hm.remove("AWN"),
            rp: hm.remove("RP"),
            dst: hm.remove("DST"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        self.kg = evaluate(env, self.kg);
        self.mba = evaluate(env, self.mba);
        self.asz = evaluate(env, self.asz);
        self.mwa = evaluate(env, self.mwa);
        self.awn = evaluate(env, self.awn);
        self.rp = evaluate(env, self.rp);
        self.dst = evaluate(env, self.dst);
        self
    }
}

#[derive(Default, Debug)]
pub struct BaseWoodTimeCategory {
    // woodTime category - These categories are used to calculate the
    // estimated runtime of a CNC program on a BOF/BAZ. - "[Name of the
    // category]" e.g. "Transport"
    kat: Option<String>,
    // woodTime macro name - Names are selected from a list that was
    // previously created in the optional woodTime software "[Name of the
    // macro]"
    mnm: Option<String>,
}

impl BaseWoodTimeCategory {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            kat: hm.remove("KAT"),
            mnm: hm.remove("MNM"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        self
    }
}

#[derive(Default, Debug)]
pub struct BaseHoodSetting {
    // Hood setting - Defines the position of the hood during processing. "0"
    // Automatic dust-suction hood = "0"
    // Dust-suction hood down = "1"
    // Intermediate position 1 = "100"
    // Intermediate position 2 = "2"
    // Dust suction on top = "3"
    hp: Option<String>,
    // Processing unit - Function on multiple-spindle ma- chines with 2
    // trimming spindles. "0"
    // Processing unit Automatic = "0"
    // Processing unit 1 = "1"
    // Processing unit 2 = "2"
    sp: Option<String>,
    // Y-offset of second spindle - This is the Y-offset of second spindle, if
    // processing has to be done with both spindles. "0"
    yve: Option<String>,
    // Default tool ids - The default tool ids suitable for this macro. This
    // is used when a tool is selected in woodWOP to shrink down the list. "0"
    ww: Option<String>,
    // Suction device - The suction device can be activated and deactivated
    // using this function. "0"
    // Suction Off = "0"
    // Suction On = "1"
    // Automatic suction = "2"
    asg: Option<String>,
}

impl BaseHoodSetting {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            hp: hm.remove("HP"),
            sp: hm.remove("SP"),
            yve: hm.remove("YVE"),
            ww: hm.remove("WW"),
            asg: hm.remove("ASG"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        //self. = evaluate(env, self.);
        self
    }
}

#[derive(Default, Debug)]
pub struct BaseSynchronousModeBehavior {
    // Synchronous mode behavior - Using these parameters, it is possible to
    // assign various processing designs to a program on different slots of
    // the machine and to operate in synchronous mode. - "0"
    // Synchronous mode ON = "1"
    sya: Option<String>,
    // Synchronous mode behavior - Using these parameters, it is possible to
    // assign various processing designs to a program on different slots of
    // the ma- chine and to operate in synchronous mode. - "0"
    // Master = "0"
    // Slave 1 = "1"
    // Slave 2 = "2"
    // Slave 3 = "3"
    syv: Option<String>,
}

impl BaseSynchronousModeBehavior {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            sya: hm.remove("SYA"),
            syv: hm.remove("SYV"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        //self. = evaluate(env, self.);
        self
    }
}

// Note: _M ∗ F parameters where introduced when the feature measurement
// reference was implemented. These new parameters have priority over the old
// ones, which still are saved for compatibility reasons. As soon as there is
// a non-STANDARD measurement reference (i.e a measurement that is not
// compatible with the old behaviour), the old parameters will be set to -1
// and become invalid in older woodWOP versions.
#[derive(Default, Debug)]
pub struct BaseMeasurementDependence {
    // Measurement dependency X - Calculation of dimension offset that was
    // determined during a previous measurement run in the X direction. - "1"
    // Off by default
    mx: Option<String>,
    // Measurement dependency Y - Calculation of dimension offset that was
    // determined during a previous measurement run in the Y direction. - "1"
    // Off by default
    my: Option<String>,
    // Measurement dependency Z - Calculation of dimension offset that was
    // determined during a previous measurement run in the Z direction. - "1"
    // Off by default
    mz: Option<String>,
    // Measurement factor in X - The factor of the measurement in the X axis -
    // "1"
    mxf: Option<String>,
    // Measurement factor in X - The factor of the measurement in the X axis -
    // "1"
    _mxf: Option<String>,
    // Measurement factor in Y - The factor of the measurement in the Y axis -
    // "1"
    myf: Option<String>,
    // Measurement factor in Y - The factor of the measurement in the Y axis -
    // "1"
    _myf: Option<String>,
    // Measurement factor in Z - The factor of the measurement in the Z axis -
    // "1"
    mzf: Option<String>,
    // Measurement factor in Z - The factor of the measurement in the Z axis -
    // "1"
    _mzf: Option<String>,
    // Dependent position measurement - macro depends on a position
    // measurement macro. - "1"
    mlm: Option<String>,
    // Position measurement reference - reference of the dependent position
    // measurement - "STANDARD" (i.e. preceeding measurement)
    mlr: Option<String>,
    // Measurement reference X - reference of the dependent measurement macro
    // in X - "STANDARD"
    mxr: Option<String>,
    // Measurement reference Y - reference of the dependent measurement macro
    // in Y - "STANDARD"
    myr: Option<String>,
    // Measurement reference Z - reference of the dependent measurement macro
    // in Z - "STANDARD"
    mzr: Option<String>,
}

impl BaseMeasurementDependence {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            mx: hm.remove("MX"),
            my: hm.remove("MY"),
            mz: hm.remove("PMZ"),
            mxf: hm.remove("MXF"),
            _mxf: hm.remove("_MXF"),
            myf: hm.remove("MYF"),
            _myf: hm.remove("_MYF"),
            mzf: hm.remove("MZF"),
            _mzf: hm.remove("_MZF"),
            mlm: hm.remove("MLM"),
            mlr: hm.remove("MLR"),
            mxr: hm.remove("MXR"),
            myr: hm.remove("MYR"),
            mzr: hm.remove("MZR"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        //self. = evaluate(env, self.);
        self
    }
}

// Further instructions available! See "Automate technology"
// documentation. For these parameters to have any effect, the techauto tool
// has to be run on the file.
#[derive(Default, Debug)]
pub struct BaseTechAuto {
    // Reference enable - Enables the macro for automation. - "0"
    // TechAuto ON = "1"
    rsel: Option<String>,
    // Selected reference ID - When RSEL is active, the reference ID is
    // specified here.
    rwid: Option<String>,
}

impl BaseTechAuto {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            rsel: hm.remove("RSEL"),
            rwid: hm.remove("RWID"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        //self. = evaluate(env, self.);
        self
    }
}

#[derive(Default, Debug)]
pub struct BaseContour {
    // Starting point - Start and end point of a programmed contour
    // definition. - "1:1"
    ea: Option<String>,
    // Endpoint - Start and end point of a programmed contour
    // definition. - "1:5"
    ee: Option<String>,
    // Forwards - This parameter is used to define the processing
    // direction of the tool in relation to the contour definition
    // direction. - "1"
    // backwards = "0"
    // forwards = "1"
    ri: Option<String>,
}

impl BaseContour {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            ea: hm.remove("EA"),
            ee: hm.remove("EE"),
            ri: hm.remove("RI"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        self.ri = evaluate(env, self.ri);
        self
    }
}

/// Macro id is in the form of <112 \Tasche\
#[derive(Default, Debug)]
pub struct MacroId {
    // The id number of the macro
    id: Option<String>,
    // The name of the macro
    name: Option<String>,
}

impl MacroId {
    pub fn new(hm: &mut HashMap<String, String>) -> Self {
        Self {
            id: hm.remove("ID"),
            name: hm.remove("NAME"),
        }
    }

    pub fn evaluate(mut self, env: &HashMap<String, f64>) -> Self {
        self
    }
}

#[derive(Default, Debug)]
pub struct GlueEdgeData {
    // Edge channel - Entry corresponds to the edge channel number from
    // which the edge to be glued is to be taken. "1"
    p59: Option<String>,
    // Test run without edge - For test purposes, the user can execute
    // processing without an edge. "1"
    tl: Option<String>,
    // Test run via workpiece - For test purposes, the user can execute
    // the test run without an edge over the workpiece. "1"
    m15: Option<String>,
}

pub enum Macro {
    /// 136 - This macro can be used to program the application of a T-mold
    /// edge to a contour element with an appropriate machine.
    ApplyTMoldEdge {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_contour: BaseContour,
        glue_edge: GlueEdgeData,
        // Side - This parameter is used to define the processing side of the
        // tool in the area. - "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        rk: Option<String>,
        // Distance - Distance of the pressure roller from the programmed
        // contour. "-0.5"
        ab: Option<String>,
        // Z dimension - For gluing units with a powered Z axis, the Z
        // dimension specification can influ-ence the edge overhangs on the
        // upper and lower edge of the workpiece. "1" On by default
        z_: Option<String>,
        // Additional C angle - This parameter defines an additional glu-ing
        // unit rotation angle relative to the con-tour. "-45"
        p60: Option<String>,
        // Correct additional C-angle - If the checkbox is deactivated, the
        // rota-tion angle of the unit always is relative to the contour
        // element. "0"
        ac: Option<String>,
        // Rotating to the start point - This parameter can switch the
        // automatic determination of the direction of rotation on or off. Off
        // by default
        // Clockwise = "0"
        // Counterclockwise = "1"
        rsp: Option<String>,
        // Separate mode - If the checkbox is activated, a user- defined NC
        // subprogram is called up in- stead of the default approach and
        // return program. "" Off by default
        md: Option<String>,
        // Code - This parameter corresponds to the tool ID for the gluing
        // unit (type of gluing unit). "27."
        vn: Option<String>,
        // Code - This parameter corresponds to the tool ID for the gluing
        // unit (type of gluing unit). "27"
        kn: Option<String>,
        // Tool number - Enter the number of a suitable tool di-rectly or
        // select one from the selection di-alog. "STANDARD" Off by default
        tno: Option<String>,
        // Feed - Enter feed speed in m/min "5"
        f_: Option<String>,
        // Speed - Speed in percent of database value "50"
        s_: Option<String>,
        // Wait time - Wait time for conveying the edge until it reaches the
        // workpiece and processing can commence. "2.5"
        ws: Option<String>,
        // Additional edge length - Adds the value entered in this parameter
        // to the calculated edge length. "30"
        kz: Option<String>,
        // Parameter set - This parameter can be used to save or load
        // different definitions of the edge pa- rameters (file extension: ∗
        // .par). "STANDARD"
        nm: Option<String>,
        // Comment - Text field for entering further information about a
        // macro. "Kante"
        km: Option<String>,
        // With glue application - If the checkbox is activated, glue is ap-
        // plied at the beginning and the end of the T-mold edge. "0"
        ka: Option<String>,
        // Interval until glue application - Distance from the start and end
        // point of the edge processing at which the gluing application is
        // activated and deactivated. "250"
        kse: Option<String>,
        // Feed, glue application - Feed speed in m/min during glue
        // applica-tion. "3.5"
        ksf: Option<String>,
        // Post pressure roller on - Defines the distance (in mm) after the
        // programmed start point from which the post-pressure roller swivels
        // onto the workpiece.
        abn: Option<String>,
        // Edge transport off - During gluing, the edge is transported out of
        // the gluing unit by the value specified here (in mm). "150"
        abv: Option<String>,
        // Post pressure roller off - Defines the distance (in mm) at the end
        // of gluing from which the post-pressure roller is swung away from
        // the material. "90"
        aba: Option<String>,
        // Wait-time for post-pressure roller - Defines the time (in seconds)
        // that the post-pressure roller presses on the edge at the end of
        // gluing. "0"
        va: Option<String>,
        // Slow edge drawing - If the checkbox is activated, the edge of the
        // gluing unit is removed from the pre- snipping station with reduced
        // feed speed. "0"
        la: Option<String>,
        // Unit with interface - Defines the type of unit and controls the
        // edge parameters to be defined for pro- gramming the T-mold edge
        // process. "1"
        ai: Option<String>,
        // Approach loading position - If the checkbox is activated, the unit
        // moves forward for the machine operator to insert the edge
        // material. "0"
        bp: Option<String>,
        // Butt joint gluing - If the checkbox is activated, the parameters
        // for defining a butt joint gluing opera-tion are activated. "0"
        rv: Option<String>,
        // Distance tracing ON - Specification of the distance between the
        // pressure roller and the start point of the butt joint gluing from
        // which the trace op- eration is enabled. "320"
        abt: Option<String>,
        // Tracing feed - Feed speed in m/min for raised tracer "2"
        fk: Option<String>,
        // Tracing path - Specification of the distance with tracer swiveled
        // in. "160"
        tb: Option<String>,
        // Joint correction - Input in mm "60"
        sk: Option<String>,
        // Distance pressure roller off - Defines the distance (in mm) at the
        // end of gluing from which the pressure roller swivels away. "200"
        ara: Option<String>,
        // Distance pressure roller on - Defines the distance (in mm) after
        // the programmed start point from which the pressure roller swivels
        // onto the work- piece. "60"
        are: Option<String>,
        // Distance post pressure - Defines the distance (in mm) from the
        // defined end point from which the applied T- mold edge is
        // post-pressed. "300"
        apr: Option<String>,
        // Split length - When the split length is specified, the contour is
        // split into individual pieces for calculation. "50"
        sl: Option<String>,
        // Correction angle increment - In the event of a collision, the
        // specified value is repeatedly added to or subtracted from the C
        // offset until a collision-free po- sition is achieved. "5"
        cf: Option<String>,
        // Safety margin - This parameter lengthens or shortens the contour
        // being processed in parallel. "2"
        sa: Option<String>,
    },
    /// 121 - This macro is used to combine contour-independent processes into
    /// one block.
    Block {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // X position - Defines the insertion point using X and Y coordinates of
        // the selected coordinate system. The X/Y position defined usually
        // refers to the center position of the element. - "0.0"
        xp: Option<String>,
        // Y position - Defines the insertion point using X and Y coordinates of
        // the selected coordinate system. The X/Y position defined usually
        // refers to the center position of the element. - "0.0"
        yp: Option<String>,
        // Z position - This parameter is used to select the Z value of the
        // element from the defined Z start coordinates of the selected
        // coordinate system. - "0.0"
        zp: Option<String>,
        // Name - This parameter can be used to save or load an existing block
        // (file extension: ∗ .blk). A user interface appears when saving, in
        // which the file name must be specified. - ""
        nm: Option<String>,
        // Number X/Y - The contents of a block can be copied by entering the
        // number in the X and Y direction. "1"
        ax: Option<String>,
        // Number X/Y - The contents of a block can be copied by entering the
        // number in the X and Y direction. "1"
        ay: Option<String>,
        // Matrix X/Y - If the number X/Y > 1, the spacing of the associated
        // block reference points is determined by this. - "0.0"
        rx: Option<String>,
        // Matrix X/Y - If the number X/Y > 1, the spacing of the associated
        // block reference points is determined by this. - "0.0"
        ry: Option<String>,
        // Optimize components - This parameter optimizes components in the
        // block, aiming on minimal tool changes - "0", Optimize components =
        // "1"
        oc: Option<String>,
        // Number of macros - This defines how many of the following macros
        // are to be considered part of this block. - "0"
        dp: Option<String>,
    },
    /// 126 - This macro is used to program a contour blow-off, e.g. to clean
    /// an edge before gluing.
    BlowOff {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_contour: BaseContour,
        // Side - This parameter is used to define the processing side of the
        // tool in the area. - "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        rk: Option<String>,
        // Z dimension - Tool Z dimension from the defined Z start
        // coordinates. - "0"
        za: Option<String>,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "10"
        ab: Option<String>,
        // Additional C angle - This parameter is used to define an additional
        // angle of rotation of the unit in relation to the workpiece. "0"
        p60: Option<String>,
        // Tool number - If the checkbox is activated, the input field is
        // activated so the number of a tool can be entered. - "175"
        tno: Option<String>,
        // Feed - Feed speed in m/min - "20"
        f_: Option<String>,
    },
    /// 170 - This macro is used to program units designed to process a
    /// contour for CF machines. Example: Separating agent application
    CFContour {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        base_contour: BaseContour,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "10"
        ab: Option<String>,
        // Tool number - Tool number of the unit - "101"
        tno: Option<String>,
        // Aggregate number - Aggregate for planning - "0"
        ag: Option<String>,
    },
    /// 157 - This macro is used for flush trimming with a CF-Unit on contour
    /// elements.
    CFFlushTrimming {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        base_contour: BaseContour,
        // Edge reference - This parameter is used for the definition of the
        // edge. - "0"
        // Front Edge: 0
        // Rear Edge: "1"
        bk: Option<String>,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "0"
        ab: Option<String>,
        // Additional C-Angle - This parameter is used to define an additional
        // angle of rotation of the unit in relation to the workpiece. - "10"
        c_: Option<String>,
        // Separate mode - If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program. The value entered has 3 characters and is numeric and/or
        // alphanumeric, depending on the naming of the subprogram by the
        // programmer. - "001"
        md: Option<String>,
        // Tool number -Tool number of the unit - "101"
        tno: Option<String>,
        // Aggregate number - Aggregate for planning - "0"
        ag: Option<String>,
    },
    /// 159 - This macro is used to glue with a CF-Unit on contour elements.
    CFGluingEdges {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        base_contour: BaseContour,
        // Parameter set - This parameter can be used to save or load
        // different definitions of the edge parameters (file extension: ∗
        // .par). - "STANDARD"
        nm: Option<String>,
        // Comment - Text field for entering further information about a
        // macro. - "PVC-edge 2mm REHAU"
        km: Option<String>,
        // Edge reference - This parameter is used for the definition of the
        // edge. - "0"
        // Front edge: "0"
        // Rear edge: "1"
        bk: Option<String>,
        // Heating power - This parameter is used to enter the percentage
        // value for the operating temperature of the hot air nozzle. - "10"
        r#str: Option<String>,
        // Edge transport off - During gluing, the edge is transported out of
        // the edge banding unit during the first entered mm. - "10"
        kta: Option<String>,
        // Thick edge - Mark edge thicker than 2 mm. - "0"
        dk: Option<String>,
        // Edge thickness - Thickness of the edge in mm - "3"
        ks: Option<String>,
        // Edge height - Height of the edge in mm - "25"
        kh: Option<String>,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "0"
        ab: Option<String>,
        // C value - Additional C-angle of the post processing roller. - "0"
        c_: Option<String>,
        // Angle edge b. unit - Angle of the edge banding unit as absolute
        // value - "10"
        aeu: Option<String>,
        // Separate mode - If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program. The value entered has 3 characters and is numeric and/or
        // alphanumeric, depending on the naming of the subprogram by the
        // programmer. - "001"
        md: Option<String>,
        // Tool number - Tool number of the unit - "101"
        tno: Option<String>,
        // Aggregate number - Aggregate for planning - "0"
        ag: Option<String>,
        // Front edge overhang - The edge is transported through the gluing
        // section [parameter] mm before the workpiece. - "0"
        kuv: Option<String>,
        // Rear edge overhang - The edge is transported through the gluing
        // section [parameter] mm after the workpiece. - "0"
        kuh: Option<String>,
    },
    /// 165 - This macro is used to program the pressure zone for a CF-Unit on
    /// contour elements
    CFPressureZone {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        base_contour: BaseContour,
        // Z dimension - The Z dimension determines the machining level of the
        // tool. - "10"
        ho: Option<String>,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "0"
        ab: Option<String>,
        // Tool number - Tool number of the unit - "101"
        tno: Option<String>,
        // Aggregate number - Aggregate for planning - "0"
        ag: Option<String>,
    },
    /// 158 - This macro is used to route with a CF-Unit on contour elements.
    CFRouting {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        base_contour: BaseContour,
        // Edge reference - This parameter is used for the definition of the
        // edge. - "0"
        //Front edge: "0"
        // Rear edge: "1"
        bk: Option<String>,
        // Z dimension - The Z dimension determines the machining level of the
        // tool. - "-3"
        ho: Option<String>,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "0"
        ab: Option<String>,
        // Separate mode - If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program. The value entered has 3 characters and is numeric and/or
        // alphanumeric, depending on the naming of the subprogram by the
        // programmer. - "001"
        md: Option<String>,
        // Tool number - Tool number of the unit - "101"
        tno: Option<String>,
        // Aggregate number - Aggregate for planning - "0"
        ag: Option<String>,
        // Cutting feed - This parameter is used to define the cutting feed -
        // "10"
        fs: Option<String>,
    },
    /// 167 - This macro is used to program sanding operations on contours for
    /// CF machines.
    CFSanding {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        base_contour: BaseContour,
        // Edge reference - This parameter is used for the definition of the
        // edge. - "0"
        //Front edge: "0"
        // Rear edge: "1"
        bk: Option<String>,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "0"
        ab: Option<String>,
        // Separate mode - If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program. The value entered has 3 characters and is numeric and/or
        // alphanumeric, depending on the naming of the subprogram by the
        // programmer. - "001"
        md: Option<String>,
        // Tool number - Tool number of the unit - "101"
        tno: Option<String>,
        // Aggregate number - Aggregate for planning - "0"
        ag: Option<String>,
        // Cutting feed - This parameter is used to define the cutting feed -
        // "10"
        fs: Option<String>,
    },
    /// 166 - This macro is used to program the snipping off of overhanging
    /// edges following edge banding for CF machines.
    CFSnipping {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        base_contour: BaseContour,
        // Front edge - If the checkbox is activated, a snipping cut is
        // performed at the front edge (first saw cut). - "1"
        vk: Option<String>,
        // Rear edge - If the checkbox is activated, a snipping cut is
        // performed at the rear edge ( second saw cut ). - "1"
        hk: Option<String>,
        // Tool number - Tool number of the unit - "140"
        tno: Option<String>,
        // Distance (begin) - Offset of the tool from the programmed saw cut
        // at the front edge. Negative value means more pressure. - "0"
        ab: Option<String>,
        // Distance (end) - Offset of the tool from the programmed saw cut at
        // the rear edge. Negative value means more pressure. - "0"
        c_: Option<String>,
    },
    /// 180 - This macro is used to change processing parameters such as feed,
    /// speed, C-angle or M functions within contour processes. There can be a
    /// maximum of 16 changes per macro. Only the first row is listed here,
    /// for the other rows replace the "1" of the KEY with "2" to "16".
    ChangeTechnologyParameter {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // Starting point - This parameter is used to select the start point
        // of processing. - "1:1"
        ea: Option<String>,
        // Endpoint - This parameter is used to select the end point of
        // processing. - "1:1"
        ee: Option<String>,
        // Technology data set - This parameter can be used to save or load an
        // existing equipment change (file extension: ∗ .nca). - "0"
        r#fn: Option<String>,
        // Comment - Text field for entering further information about a
        // macro. - "0"
        km: Option<String>,
        // Technology changes Type, 1st row - Defines the type of change.
        // "1": New value for feedrate
        // "2": New value for C-axis
        // "3": New value for evolution
        // "4": New value for Z-axis
        // "5": New M-function
        // "6": New G-function
        // "7": New NC line
        // "8": New value for A-axis
        // "9": New heat power (decor side)
        // "10": New heat power (glue side)
        // "11": Edge loop length (relative)
        // "12": Laser Power
        ds_typ1: Option<String>,
        // Technology changes Value, 1st row - The column "Value" contains the
        // new value for the given type at the specified location. - "0"
        ds_wer1: Option<String>,
        // Technology changes Location, 1st row - The column "Location"
        // specifies whether this row of technology change refers to the
        // selected starting point or to the selected endpoint.
        // "2": starting point
        // "3": endpoint
        ds_ort1: Option<String>,
        // Technology changes Path, 1st row - The column "Path" specifies a
        // distance relative to the selected location. Negative values shift
        // the position, where the change becomes effective, in opposite
        // direction of the technology macro. Positive values shift the
        // position in direction of the technology macro. - "-20"
        ds_str1: Option<String>,
        ds_typ2: Option<String>,
        ds_wer2: Option<String>,
        ds_ort2: Option<String>,
        ds_str2: Option<String>,
        ds_typ3: Option<String>,
        ds_wer3: Option<String>,
        ds_ort3: Option<String>,
        ds_str3: Option<String>,
        ds_typ4: Option<String>,
        ds_wer4: Option<String>,
        ds_ort4: Option<String>,
        ds_str4: Option<String>,
        ds_typ5: Option<String>,
        ds_wer5: Option<String>,
        ds_ort5: Option<String>,
        ds_str5: Option<String>,
        ds_typ6: Option<String>,
        ds_wer6: Option<String>,
        ds_ort6: Option<String>,
        ds_str6: Option<String>,
        ds_typ7: Option<String>,
        ds_wer7: Option<String>,
        ds_ort7: Option<String>,
        ds_str7: Option<String>,
        ds_typ8: Option<String>,
        ds_wer8: Option<String>,
        ds_ort8: Option<String>,
        ds_str8: Option<String>,
        ds_typ9: Option<String>,
        ds_wer9: Option<String>,
        ds_ort9: Option<String>,
        ds_str9: Option<String>,
        ds_typ10: Option<String>,
        ds_wer10: Option<String>,
        ds_ort10: Option<String>,
        ds_str10: Option<String>,
        ds_typ11: Option<String>,
        ds_wer11: Option<String>,
        ds_ort11: Option<String>,
        ds_str11: Option<String>,
        ds_typ12: Option<String>,
        ds_wer12: Option<String>,
        ds_ort12: Option<String>,
        ds_str12: Option<String>,
        ds_typ13: Option<String>,
        ds_wer13: Option<String>,
        ds_ort13: Option<String>,
        ds_str13: Option<String>,
        ds_typ14: Option<String>,
        ds_wer14: Option<String>,
        ds_ort14: Option<String>,
        ds_str14: Option<String>,
        ds_typ15: Option<String>,
        ds_wer15: Option<String>,
        ds_ort15: Option<String>,
        ds_str15: Option<String>,
        ds_typ16: Option<String>,
        ds_wer16: Option<String>,
        ds_ort16: Option<String>,
        ds_str16: Option<String>,
    },
    /// 115- This macro is intended for machinetables where the consoles are
    /// oriented in Y-direction of the machine. It is used to enter the
    /// positions of consoles and clamping equipment on the consoles. There
    /// can be a maximum of 8 clamping means per console. Only the first row
    /// is listed here, for the other rows replace the trailing "1" of the KEY
    /// with "2" to "8"
    ClampingConsole {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // Console position - This parameter is used to define the X position
        // of the center of the console. - "0"
        xa: Option<String>,
        // Number - Defines the number of clamping units on the active
        // console. - "0"
        an: Option<String>,
        // Position - Y position of the clamping unit on the console. - "0"
        ya1: Option<String>,
        // ID - Type of clamping unit. - "0"
        ty1: Option<String>,
        // Angle, support - Defines the rotation angle of the clamping unit. -
        // "0"
        wi1: Option<String>,
        // Angle, body 1 - Rotational angle of compound relative to its
        // seating - "0"
        w11: Option<String>,
        // Clamping - Relative transformation distance of compound 1 based on
        // its initial distance - "_B ←- SZ"
        tr1: Option<String>,
        // Vacuum - Vacuum active - "1"
        va1: Option<String>,
        ya2: Option<String>,
        ty2: Option<String>,
        wi2: Option<String>,
        w12: Option<String>,
        tr2: Option<String>,
        va2: Option<String>,
        ya3: Option<String>,
        ty3: Option<String>,
        wi3: Option<String>,
        w13: Option<String>,
        tr3: Option<String>,
        va3: Option<String>,
        ya4: Option<String>,
        ty4: Option<String>,
        wi4: Option<String>,
        w14: Option<String>,
        tr4: Option<String>,
        va4: Option<String>,
        ya5: Option<String>,
        ty5: Option<String>,
        wi5: Option<String>,
        w15: Option<String>,
        tr5: Option<String>,
        va5: Option<String>,
        ya6: Option<String>,
        ty6: Option<String>,
        wi6: Option<String>,
        w16: Option<String>,
        tr6: Option<String>,
        va6: Option<String>,
        ya7: Option<String>,
        ty7: Option<String>,
        wi7: Option<String>,
        w17: Option<String>,
        tr7: Option<String>,
        va7: Option<String>,
        ya8: Option<String>,
        ty8: Option<String>,
        wi8: Option<String>,
        w18: Option<String>,
        tr8: Option<String>,
        va8: Option<String>,
    },
    /// 130 - This macro is intended for machinetables where the consoles are
    /// oriented in X-direction of the machine. It is used to enter the
    /// positions of consoles and clamping equipment on the consoles. There
    /// can be a maximum of 25 clamping means per console. Only the first row
    /// is listed here, for the other rows replace the trailing "1" of the KEY
    /// with "2" to "25".
    ClampingLongitudinalConsole {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // Console position - This parameter is used to define the Y position
        // of the center of the console. - "0"
        ya: Option<String>,
        // Number - Defines the number of clamping units on the active
        // console. - "0"
        an: Option<String>,
        // Position - X position of the clamping unit on the console. - "0"
        xa1: Option<String>,
        // ID - Type of clamping unit. - "0"
        tx1: Option<String>,
        // Angle, support - Defines the rotation angle of the clamping unit. -
        // "0"
        wi1: Option<String>,
        // Angle, body 1 - Rotational angle of compound relative to its
        // seating - "0"
        w11: Option<String>,
        // Clamping - Relative transformation distance of compound 1 based on
        // its initial distance - "_B ←- SZ"
        tr1: Option<String>,
        // Vacuum - Vacuum active - "1"
        va1: Option<String>,
        xa2: Option<String>,
        tx2: Option<String>,
        wi2: Option<String>,
        w12: Option<String>,
        tr2: Option<String>,
        va2: Option<String>,
        xa3: Option<String>,
        tx3: Option<String>,
        wi3: Option<String>,
        w13: Option<String>,
        tr3: Option<String>,
        va3: Option<String>,
        xa4: Option<String>,
        tx4: Option<String>,
        wi4: Option<String>,
        w14: Option<String>,
        tr4: Option<String>,
        va4: Option<String>,
        xa5: Option<String>,
        tx5: Option<String>,
        wi5: Option<String>,
        w15: Option<String>,
        tr5: Option<String>,
        va5: Option<String>,
        xa6: Option<String>,
        tx6: Option<String>,
        wi6: Option<String>,
        w16: Option<String>,
        tr6: Option<String>,
        va6: Option<String>,
        xa7: Option<String>,
        tx7: Option<String>,
        wi7: Option<String>,
        w17: Option<String>,
        tr7: Option<String>,
        va7: Option<String>,
        xa8: Option<String>,
        tx8: Option<String>,
        wi8: Option<String>,
        w18: Option<String>,
        tr8: Option<String>,
        va8: Option<String>,
        xa9: Option<String>,
        tx9: Option<String>,
        wi9: Option<String>,
        w19: Option<String>,
        tr9: Option<String>,
        va9: Option<String>,
        xa10: Option<String>,
        tx10: Option<String>,
        wi10: Option<String>,
        w110: Option<String>,
        tr10: Option<String>,
        va10: Option<String>,
        xa11: Option<String>,
        tx11: Option<String>,
        wi11: Option<String>,
        w111: Option<String>,
        tr11: Option<String>,
        va11: Option<String>,
        xa12: Option<String>,
        tx12: Option<String>,
        wi12: Option<String>,
        w112: Option<String>,
        tr12: Option<String>,
        va12: Option<String>,
        xa13: Option<String>,
        tx13: Option<String>,
        wi13: Option<String>,
        w113: Option<String>,
        tr13: Option<String>,
        va13: Option<String>,
        xa14: Option<String>,
        tx14: Option<String>,
        wi14: Option<String>,
        w114: Option<String>,
        tr14: Option<String>,
        va14: Option<String>,
        xa15: Option<String>,
        tx15: Option<String>,
        wi15: Option<String>,
        w115: Option<String>,
        tr15: Option<String>,
        va15: Option<String>,
        xa16: Option<String>,
        tx16: Option<String>,
        wi16: Option<String>,
        w116: Option<String>,
        tr16: Option<String>,
        va16: Option<String>,
        xa17: Option<String>,
        tx17: Option<String>,
        wi17: Option<String>,
        w117: Option<String>,
        tr17: Option<String>,
        va17: Option<String>,
        xa18: Option<String>,
        tx18: Option<String>,
        wi18: Option<String>,
        w118: Option<String>,
        tr18: Option<String>,
        va18: Option<String>,
        xa19: Option<String>,
        tx19: Option<String>,
        wi19: Option<String>,
        w119: Option<String>,
        tr19: Option<String>,
        va19: Option<String>,
        xa20: Option<String>,
        tx20: Option<String>,
        wi20: Option<String>,
        w120: Option<String>,
        tr20: Option<String>,
        va20: Option<String>,
        xa21: Option<String>,
        tx21: Option<String>,
        wi21: Option<String>,
        w121: Option<String>,
        tr21: Option<String>,
        va21: Option<String>,
        xa22: Option<String>,
        tx22: Option<String>,
        wi22: Option<String>,
        w122: Option<String>,
        tr22: Option<String>,
        va22: Option<String>,
        xa23: Option<String>,
        tx23: Option<String>,
        wi23: Option<String>,
        w123: Option<String>,
        tr23: Option<String>,
        va23: Option<String>,
        xa24: Option<String>,
        tx24: Option<String>,
        wi24: Option<String>,
        w124: Option<String>,
        tr24: Option<String>,
        va24: Option<String>,
        xa25: Option<String>,
        tx25: Option<String>,
        wi25: Option<String>,
        w125: Option<String>,
        tr25: Option<String>,
        va25: Option<String>,
    },
    /// 114 - This macro can be used to enter the position of clamping
    /// equipment, such as a vacuum cup.
    ClampingSingle {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // X position - X position of the clamping unit. - "0"
        xa: Option<String>,
        // Y position - Y position of the clamping unit. - "0"
        ya: Option<String>,
        // ID - Type of clamping unit. - "0"
        ty: Option<String>,
        // Angle, support - Defines the rotation angle of the clamping unit. -
        // "0"
        wi: Option<String>,
        // Angle, Body 1 - Rotational angle of compound 1 relative to its
        // seating - "0"
        w1: Option<String>,
        // Clamping - Relative transformation distance of compound 1 based on
        // its initial distance - "_B ←- SZ"
        tr: Option<String>,
    },
    /// 101 - This macro can be used to display a comment.
    Comment {
        id: MacroId,
        base: BaseMacro,
        // Comment - Defines a line of the comment - ""
        km: Option<String>,
    },
    /// 139 -This macro is used to create woodWOP programs in modular fashion
    /// from a main program and inserted modules (components).
    Component {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // X movement - Defines the insertion point using X and Y coordinates
        // of the selected coordinate system. - "0.0"
        xa: Option<String>,
        // Y movement Defines the insertion point using X and Y coordinates of
        // the selected coordinate system. "0.0"
        ya: Option<String>,
        // Z movement - This parameter is used to select the Z value of the
        // element from the defined Z start coordinates of the selected
        // coordinate system. "0.0"
        za: Option<String>,
        // Angle - This angle is used to rotate processing in the XY plane
        // around the Z axis. - "0"
        wi: Option<String>,
        // Name - This parameter can be used to load an existing component for
        // insertion or open it in a new window for purposes of
        // modification. - ""
        r#in: Option<String>,
        // Embed - These parameters can be used to set the values for each
        // component individually. - "0"
        em: Option<String>,
        // Component values - If the checkbox is activated, the external file
        // is permanently saved in the woodWOP file. - ""
        va: Option<String>,
    },
    /// 190 - This rare macro is used to copy mill with special machines.
    CopyMilling {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        synchronous_mode: BaseSynchronousModeBehavior,
        base_contour: BaseContour,
        // Contour position - This parameter defines the position of the
        // contour. - "0"
        // Top contour = "0"
        // Bottom contour = "1"
        kl: Option<String>,
        // NC output - This parameter defines the edge of the output of the
        // nc. - "0"
        // Front edge bottom = "0"
        // Front edge top = "1"
        // Rear edge top = "2"
        // Rear edge bottom = "3"
        na: Option<String>,
        // Feed - Feed speed in m/min - "25" Off by default
        vul: Option<String>,
        // Return feed - Return feed speed in m/min - "25" Off by default
        aul: Option<String>,
        // Extended contour processing - Prolonged contour processing at lower
        // left side in addition to programmed contour. - "0"
        kul: Option<String>,
        // Withdrawal angle - Departure angle at lower left side to
        // application point of device. Will only be used if contour starts
        // with an arc element. - "10"
        wul: Option<String>,
    },
    /// 192 - This macro is used to program horizontal bore holes and serial
    /// holes, including the process for gluing and pressing in dowels.
    DowelingHorizontal {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // Position code - "When MI is = "0" then XA and YA define the start
        // point of a hole series. When MI is ="1" then XA and YA define the
        // center point of a hole series." - "0"
        mi: Option<String>,
        // Start point - "Defines the start point for processing on the X
        // axis. (When MI = "0")" - "0"
        // Center point - "Specifies the center point of a hole series. (When
        // MI = "1")" - "0"
        xa: Option<String>,
        // Start point - "Defines the start point for processing on the Y
        // axis. (When MI = "0")" - "0"
        // Center point - "Specifies the center point of a hole series. (When
        // MI = "1")" - "0"
        ya: Option<String>,
        // Z position - "This parameter is used to select the Z value of the
        // element from the defined Z start coordinates of the selected
        // coordinate system." - "0"
        za: Option<String>,
        // Drilling direction, standard - "Defines the work direction of the
        // tool in relation to the alignment of the coordinate system
        // selected." - "XP"
        // X+ direction = "XP"
        // X- direction = "XM"
        // Y+ direction = "YP"
        // Y- direction = "YM"
        // free angle = "C"
        bm: Option<String>,
        // Drill direction, free C-angle - "Angle of drilling in the
        // X/Y-level.The angle unit is degrees (e.g.45.0).Parameter ex-
        // clusively necessary when BM="C"." - "0"
        wi: Option<String>,
        // Number - Defines the number of drill holes in a macro. - "1"
        an: Option<String>,
        // Length - Defines the length of a hole series. - "0" Off by default"
        la: Option<String>,
        // Grid - "Is equivalenttothe distance betweentwo drill holes." - "32"
        ab: Option<String>,
        // Step depth - "If the step depth is less than the drilling depth,
        // the drilling is processed in several cycles." - "0"
        zt: Option<String>,
        // Retraction dimension - "Dimension of the retraction movement after
        // the individual adjustment." - "0"
        rm: Option<String>,
        // Holding time - "Holding time at drilling depth after each in-
        // termediate step." - "0"
        vw: Option<String>,
        // Standard drill mode - Defines the execution of the process. - "STD"
        // Standard = "STD"
        // Drilling with return = "BMR"
        bm2: Option<String>,
        // Separate mode - "If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program." - "0" Off by default"
        md: Option<String>,
        // Diameter - "Tool diameter.Only one diameter is possi- ble in each
        // individual macro." - "8"
        du: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the processing direction." - "0"
        ti: Option<String>,
        // Feed - Feed speed in m/min - "0" Off by default"
        f_: Option<String>,
        // Approach clearance - "Defines a safety distance in the XY plane at
        // which the tool moves downwards along the X axis." - "20"
        ana: Option<String>,
        // Drilling active - If drilling should take place - "1"n
        bo: Option<String>,
        // Gluing active - If glue should be applied - "1"
        ln: Option<String>,
        // Dowel driving - if a dowel should be inserted - "1"
        db: Option<String>,
        // Gluing time - "This parameter is used to define the duration of
        // glue injection." - "100"
        lz: Option<String>,
        // Dowel depth - "Press-in depth of the dowel from the defined start
        // point in the processing direction." - "16"
        dt: Option<String>,
        // Dowel length - "This parameter is used to define the length of the
        // dowel." - "32"
        dl: Option<String>,
        // Vibrating chamber - "This parameter is used to select the vibrat-
        // ing chamber." - "1"
        rt: Option<String>,
        // Press-in time - "This parameter is used to define the dura- tion of
        // the press-in operation." - "100"
        ez: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - SM must be set to "1" to activate the use of s_ or S_A -
        // "0"
        sm: Option<String>,
    },
    /// 131 - This macro is used to program drill holes in the underside of
    /// the workpiece.
    DrillingFromBelow {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // Starting point - Defines the start point for processing on the X
        // and Y axes. - "0"
        ea: Option<String>,
        // Endpoint - Defines the start point for processing on the X and Y
        // axes. - "0"
        ee: Option<String>,
        // Separate mode - "If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program." - "1" Off by default"
        md: Option<String>,
        // Diameter - "Tool diameter.Only one diameter is possible in each
        // individual macro." - "8"
        du: Option<String>,
        // Tool number - Tool number of the unit. - "61"
        tno: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the pro- cessing direction." - "0"
        ti: Option<String>,
        // Feed - Feed speed in m/min - "0" Off by default"
        f_: Option<String>,
        // Approach clearance - "Defines a safety distance in the xy plane at
        // which the tool moves downwards along the x axis." - "20"
        ab: Option<String>,
        // Arm angle - "Orientation of the underfloor unit's arm around the
        // Z-axis of the referenced coordinate system. X-direction = 0. Unit =
        // degrees." - "0"
        wi: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed -SM must be set to "1" to activate the use of S_ or S_A - "0"
        sm: Option<String>,
    },
    /// 103 - This macro is used to program horizontal drill holes and hole
    /// series.
    DrillingHorizontal {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // Position code - "When MI is = "0" then XA and YA define the start
        // point of a hole series. When MI is ="1" then XA and YA define the
        // center point of a hole series." - "0"
        mi: Option<String>,
        // Start point - "Defines the start point for processing on the X
        // axis. (When MI = "0")" - "0"
        // Center point - "Specifies the center point of a hole series. (When
        // MI = "1")" - "0"
        xa: Option<String>,
        // Start point - "Defines the start point for processing on the Y
        // axis. (When MI = "0")" - "0"
        // Center point - "Specifies the center point of a hole series. (When
        // MI = "1")" - "0"
        ya: Option<String>,
        // Z position - "This parameter is used to select the Z value of the
        // element from the defined Z start coordinates of the selected
        // coordinate system." - "0"
        za: Option<String>,
        // Drilling direction, standard - "Defines the work direction of the
        // tool in rela- tion to the alignment of the coordinate system
        // selected." - "XP"
        // X+ direction = "XP"
        // X- direction = "XM"
        // Y+ direction = "YP"
        // Y- direction = "YM"
        // free angle = "C"
        bm: Option<String>,
        // Drill direction, free C-angle - "Angle of drilling in the
        // X/Y-level.The angle unit is degrees (e.g.45.0). Parameter
        // exclusively necessary when BM="C"." - "0"
        wi: Option<String>,
        // Swiveling angle - "This parameter can be used to define the swivel
        // angle. While that angle has no effect on the resulting hole, it can
        // be used in some cases to avoid collision problems with asymmetric
        // aggregates." - "0" Off by default"
        a_: Option<String>,
        // Number - Defines the number of drill holes in a macro. - "1"
        an: Option<String>,
        // Length - Defines the length of a hole series. - "0" Off by default"
        la: Option<String>,
        // Grid - "Is equivalent to the distance between two drill holes." -
        // "32"
        ab: Option<String>,
        // Step depth - "If the step depth is less than the drilling depth,
        // the drilling is processed in several cycles." - "0"
        zt: Option<String>,
        // Retraction dimension - Dimension of the retraction movement after
        // the individual adjustment. - "0"
        rm: Option<String>,
        // Holding time - "Holding time at drilling depth after each in-
        // termediate step." - "0"
        vw: Option<String>,
        // Standard drill mode - Defines the execution of the process. - "STD"
        // Standard = "STD"
        // Drilling with return = "BMR"
        bm2: Option<String>,
        // Separate mode - "If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program." - "0" Off by default"
        md: Option<String>,
        // Diameter - "Tool diameter.Only one diameter is possible in each
        // individual macro." - "8"
        du: Option<String>,
        // Tool number - Tool number of the unit. - "61"
        t_: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the processing direction." - "0"
        ti: Option<String>,
        // Feed - Feed speed in m/min - "0" Off by default"
        f_: Option<String>,
        // Approach clearance - "Defines a safety distance in the XY plane at
        // which the tool moves downwards along the X axis." - "20"
        ana: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - SM must be set to "1" to activate the use of S_ or S_A - "0"
        sm: Option<String>,
    },
    /// 102 - This macro is used to program vertical drill holes and hole
    /// series.
    DrillingVertical {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        // Position code - "When MI is = "0" then XA and YA de- fine the start
        // point of a hole series. When MI is ="1" then XA and YA de- fine the
        // center point of a hole series." - "0"
        mi: Option<String>,
        // Start point - "Defines the start point for processing on the X
        // axis. (When MI = "0")" - "0"
        // Center point - "Specifies the center point of a hole series. (When
        // MI = "1")" - "0"
        xa: Option<String>,
        // Start point - "Defines the start point for processing on the Y
        // axis. (When MI = "0")" - "0"
        // Center point - "Specifies the center point of a hole series. (When
        // MI = "1")" - "0"
        ya: Option<String>,
        // Z position - "This parameter is used to select the Z value of the
        // element from the defined Z start coordinates of the selected
        // coordinate system." - "0"
        za: Option<String>,
        // Number - "Defines the number of drill holes in a macro." - "1"
        an: Option<String>,
        // Length - Defines the length of a hole series. - "0" Off by default"
        la: Option<String>,
        // Grid - "Is equivalent to the distance between two drill holes." -
        // "32"
        ab: Option<String>,
        // Angle Drill line - "This angle is used to rotate the hole series in
        // the XY plane around the Z axis." - "0"
        wi: Option<String>,
        // Step depth - "If the step depth is less than the drilling depth,
        // the drilling is processed in several cycles." - "0"
        zt: Option<String>,
        // Retraction dimension - "Dimension of the retraction movement after
        // the individual adjustment." - "0"
        rm: Option<String>,
        // Holding time - "Holding time at drilling depth after each
        // intermediate step." - "0"
        vw: Option<String>,
        // Drill mode, standard - "Defines the execution of the process. There
        // are several predefined modes available."
        // "Slow-fast to depth "LS"
        // Fast-fast to depth "SS"
        // Slow-fast-slow through "LSL"
        // Fast-fast-fast through "SSS"
        // Drilling with return "BMR"
        // Modes with restricted  availability:
        // Slow-fast from below to depth "LSU"
        // Fast-fast from below to depth "SSU"
        // Slow-fast-slowfrombelowthrough "LSLU"
        // Fast-fast-fast from below through "SSSU"
        // Drilling with return from below "BMRU"
        // Drill mode, user defined - "The use of a value different from the
        // predefined values and consisting of three numerical or
        // alphanumerical digits calls a user defined mode. This means that a
        // corresponding NC-subprogram(which must exist) is called instead of
        // the standard approach and return program." - "000"
        bm: Option<String>,
        // Diameter - "Tool diameter.Only one diameter is possible in each
        // individual macro." - "8"
        du: Option<String>,
        // Tool number - Tool number of the unit. - "61"
        tno: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the processing direction." - "12"
        ti: Option<String>,
        // Feed - Feed speed in m/min - "0" Off by default"
        f_: Option<String>,
        // Speed - "The speed can be set in each drilling macro by selecting a
        // fixed speed." - "1"
        // Slow = "0"
        // Medium = "1"
        // Fast = "2"
        // User-defined = "3"
        s_: Option<String>,
        // Speed - Speed in percent of database value - "100"
        s_p: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
    },
    /// 104 - This macro is used to program drill holes that are executed
    /// under an A-angle.
    DrillingWithAAngle {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // Start point - "Defines the start point for processing on the X
        // axis." - "0"
        xa: Option<String>,
        // Start point - "Defines the start point for processing on the Y
        // axis." - "0"
        ya: Option<String>,
        // Center point - "Specifies the center point of a hole series.(Start
        // point and Center point are mutually exclusive)" - "0"
        xm: Option<String>,
        // Center point - "Specifies the center point of a hole series." - "0"
        ym: Option<String>,
        // Z position - "This parameter is used to select the Z value of the
        // element from the de- fined Z start coordinates of the se- lected
        // coordinate system." - "0"
        za: Option<String>,
        // Tool perpendicular to level - "These parameter defines the tool's
        // axis of rotation perpendicular to the X/Y plane or in a freely
        // defined orientation to the X/Y plane." - "Active when parameters CA
        // and WI are not specified."
        // -------------------------------------------------------------
        // Drill direction, free C-angle - "Angle of drilling in the
        // X/Y-level. The angle unit is degrees(e.←↩ g.45.0). Parameter
        // exclusively necessary when the tool is not perpendicular to the
        // XY-level." - "0"
        ca: Option<String>,
        // Swivelling angle - "This parameter can be used to define the swivel
        // angle. Parameter ex- clusively necessary when the tool is not
        // perpendicular to the XY-level." - "0"
        wi: Option<String>,
        // Number - "Defines the number of drill holes in a macro." - "1"
        an: Option<String>,
        // Length - Defines the length of a hole series. - "0" Off by default"
        la: Option<String>,
        // Grid - "Is equivalent to the distance between two drill holes." -
        // "32"
        ab: Option<String>,
        // Step depth - "If the step depth is less than the drilling depth,the
        // drilling is processed in several cycles." - "0"
        zt: Option<String>,
        // Retraction dimension - "Dimension of the reaction movement after
        // the individual adjustment." - "0"
        rm: Option<String>,
        // Holding time - "Holding time at drilling depth after each
        // intermediate step." - "0"
        vw: Option<String>,
        // Drill mode, standard - "Defines the execution of the process." -
        // "STD"
        // Standard = "STD"
        // Drilling with return = "BMR"
        // Drill mode, user-defined - "The use of a value different from the
        // predefined values and consisting of three numerical or
        // alphanumerical digits calls a user defined mode. This means that a
        // corresponding NC-subprogram (which must exist)is called instead of
        // the standard approach and withdrawal program." -
        bm: Option<String>,
        // Diameter - "Tool diameter. Only one diameter is possible in each
        // individual macro." - "8"
        du: Option<String>,
        // Tool number - Tool number of the unit. - "61"
        tno: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the processing direction." - "0"
        ti: Option<String>,
        // Feed - Feed speed in m/min - "0" Off by default"
        f_: Option<String>,
        // Operation from below - "When WIU, ANU or ABU are specified,
        // operation from below is active" -
        // Arm angle - "For operation from below. Orientation of the drilling
        // unit's arm around the Z-axis of the referenced coordinate
        // system. X-direction = 0.Unit =degrees. When A_ and C_ are
        // specified, WIU is computed automatically." - "0"
        wiu: Option<String>,
        // Approach clearance - "For operation from below. Defines a safety
        // distance in the XY plane from the starting point at which the tool
        // moves downwards in Z- direction." - "0"
        anu: Option<String>,
        // Withdrawal clearance - "For operation from below. Defines a safety
        // distance in the XY plane from the endpoint at which the tool
        // moves upwards in Z-direction." - "10"
        abu: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Speed - SM must be set to "1" to activate the use of S_ or S_A - "0"
        sm: Option<String>,
    },
    /// 107 - This macro can be used to program the flush trimming of the top
    /// and bottom overhang of the edge material and the scraper after the
    /// edges have been glued.
    FlushTrimmingEdges {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_contour: BaseContour,
        // Side - "This parameter defines on which side the tool moves
        // relative to the processing direction." - "LI"
        // "LI" = left
        // "RE" = right"
        md: Option<String>,
        // Z dimension - "The Z-dimension determines the processing height of
        // the tools." - "0"
        z_: Option<String>,
        // Distance - "Offset of the tool relative to the programmed contour."
        // - "-0.5"
        ab: Option<String>,
        // Additional C angle - "This parameter is used to define an
        // additional angle of rotation of the unit in relation to the
        // workpiece." - "0"
        p60: Option<String>,
        // Correct additional C-angle - "If the checkbox is activated, the
        // rotation angle of the unit is relative to the contour element. The
        // absolute value of the rotation is calculated automatically for each
        // contour element.The additional C angle remains constant by the unit
        // being tracked in its angle position." - "0"
        ac: Option<String>,
        // Separate mode - "If the checkbox is activated, a user defined NC
        // subprogram is called up instead of the default approach and return
        // program. The value entered has 3 characters and is numeric and/or
        // alphanumeric, depending on the naming of the subprogram by the
        // programmer." - "000"
        r#mod: Option<String>,
        // Tool number - "Enter the number of a suitable tool directly or
        // select one from the selection dialog." - "110"
        ag: Option<String>,
        // Feed - Feed speed in m/min - "15"
        f_: Option<String>,
        // Speed - "If the checkbox is activated, the tool speed can be
        // specified as a percentage of the entry in the tool database." - "0"
        s_: Option<String>,
        // Overlap at joint - "When a closed contour is processed (starting
        // point = endpoint), a part of the edge remains unprocessed. This
        // parameter extends the processing at the start and end point by the
        // specified distance." - "0"
        lap: Option<String>,
        // Tracing order - Depending on the edge material, it may be better
        // first to move the horizontal sensor to the workpiece and then to
        // move the vertical sensor to the workpiece or vice versa. - "0"
        // Horizontal first = "0"
        // Vertical first = "1"
        trf: Option<String>,
        // Smooth movement - "This parameter makes movements more even when
        // processing with units." - "0"
        ku: Option<String>,
        // Split length - "When the split length is specified,the contour is
        // split into individual pieces for the calculation of the correction
        // angle." - "50"
        sl: Option<String>,
        // Correction angle increment - "In the event of a collision, the
        // specified value is repeatedly added to or subtracted from the C
        // offset until a collision-free position is achieved." - "5"
        cf: Option<String>,
        // Safety margin - This parameter offsets the contour, which is
        // processed, parallel to itself. This offset-contour is then checked
        // for collision with the outline of the unit on the level of the
        // edge. - "2"
        sa: Option<String>,
    },
    /// 188 - This macro can be used to program the gluing of an edge to a
    /// contour element on an appropriate machine with the easyEdge gluing
    /// unit.
    GluingEasyEdge {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_contour: BaseContour,
        glue_edge: GlueEdgeData,
        // Approach mode - "Selection of one out of several predefined
        // approach modes. Within the approach motion, the tool radius
        // compensation is gradually added." - "TAN"
        // Tangential: "TAN"
        // Lateral: "SEI"
        // Lateral inner contour: "INN"
        mda: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN_AB"
        // Tangential: "TAN_AB"
        // Lateral: "SEI_AB"
        // Lateral inner contour: "INN_AB"
        mde: Option<String>,
        // Distance - "Distance of the pressure roller from the programmed
        // contour." - "-2"
        ab: Option<String>,
        // Z dimension - "If the checkbox is activated, the 'Z dimension'
        // parameter is activated." - "0"
        z_: Option<String>,
        // Additional C angle - "This parameter defines an additional gluing
        // unit rotation angle relative to the contour." - "-55"
        p60: Option<String>,
        // Correct additional C-angle - "If the checkbox is activated, the
        // rotation angle of the unit is initially relative to the contour
        // element." - "0"
        ac: Option<String>,
        // Rotating to the start point - "If the checkbox is activated, the
        // direction of rotation of the unit can be entered in the active
        // selection window." - "0"
        // Clockwise: "0"
        // Counterclockwise: "1"
        rsp: Option<String>,
        // NC mode - "A user-defined NC subprogram is called up instead of the
        // default approach and return program." - ""
        md: Option<String>,
        // ID code - "This parameter corresponds to the tool ID for the gluing
        // unit (type of gluing unit)." - "262"
        kn: Option<String>,
        // Tool number - "If the checkbox is activated, the input field is
        // activated so the number of a tool can be entered." - "0"
        tno: Option<String>,
        // Feed edge approach - "Defines the feed from the gluing unit's
        // approach to the workpiece up to the time of activating the
        // "increased glue application" (M455)." - "3"
        fka: Option<String>,
        // Wait-time edge fixing at start - "Defines the wait time after
        // setting up the pressure roller on the workpiece, which fixes the
        // edge at the edge approach." - "2"
        wks: Option<String>,
        // Additional edge length - "Adds the value entered in this parameter
        // to the calculated edge length. This ensures an overhang at the rear
        // edge." - "25"
        kz: Option<String>,
        // Parameter set - "This parameter can be used to save or load
        // different definitions of the edge parameters (file extension:
        // *.par)." - "STANDARD"
        nm: Option<String>,
        // Comment - "Text field for entering further information about a
        // macro." - "PVC-edge 2mm REHAU"
        km: Option<String>,
        // Pressure roller ON - "Defines the distance (in mm) after the
        // programmed start point from which the pressure roller swivels onto
        // the workpiece." - "55"
        aae: Option<String>,
        // Pressure roller movement - "For units with glue nozzles, this
        // parameter defines the line between swiveling the pressure roller
        // onto the workpiece and the position wait time for edge fixing." -
        // "10"
        oa: Option<String>,
        // Pressure roller OFF - "Defines the distance (in mm) at the end of
        // gluing from which the pressure roller swivels away." - "55"
        aaa: Option<String>,
        // Wait-time pressure roller - "Defines the time (in seconds) that the
        // pressure roller remains stationary on the edge after gluing." - "1"
        wke: Option<String>,
        // Edge ready time - "This parameter is not evaluated when edging with
        // easyEdge." - "1"
        kb: Option<String>,
        // "Gluing unit distance/pre-snip station" - "This parameter defines
        // the distance between the edge banding unit and the piggyback edge
        // changer." - "0"
        av: Option<String>,
        // Thick edge - "Activates a special mode at the
        // frontedge (easyEdge1with nozzle)" - "0"
        dk: Option<String>,
        // Butt joint gluing - "If the checkbox is activated, the parameters
        // for defining a butt joint gluing operation are activated." - "0"
        rv: Option<String>,
        // Distance glue application - "Defines the distance before the
        // programmed starting point." - "50"
        ala: Option<String>,
        // Correction glue application - "This parameter enables the glue
        // application to be extended or shortened at the actual gluing end."
        // - "0"
        kla: Option<String>,
        // Split length - "When the split length is specified, the contour is
        // split into individual pieces for the calculation of the correction
        // angle." - "50"
        sl: Option<String>,
        // Correction angle increment - "In the event of a collision,the
        // specified value is repeatedly added to or subtracted from the C
        // offset until a collision-free position is achieved." - "5"
        cf: Option<String>,
        // Safety margin - This parameter offsets the contour, which is
        // processed, parallel to itself. This offset-contour is then checked
        // for collision with the outline of the unit on the level of the
        // edge. - "2"
        sa: Option<String>,
    },
    /// 106 - This macro can be used to program the gluing of an edge to a
    /// contour element with an appropriate machine.
    GluingEdges {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_contour: BaseContour,
        glue_edge: GlueEdgeData,
        // Alphanumeric edge code active - This function allows an edge code
        // to be entered in place of the edge channel. - "0" Off by default
        kca: Option<String>,
        // Alphanumeric edge code - "This function allows an edge code to be
        // entered in place of the edge channel." - "0" Off by default"
        kc: Option<String>,
        // Approach mode - "Selection of one out of several predefined
        // approach modes. Within the approach motion, the tool radius
        // compensation is gradually added." - "TAN"
        // Tangential = "TAN"
        // Lateral = "SEI"
        // Lateral inner contour = "INN"
        // Inside corner 90° =  "INN90"
        mda: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN"
        // Tangential = "TAN_AB"
        // Lateral = "SEI_AB"
        // Lateral inner contour = "INN_AB"
        // Inside corner 90° = "INN90_AB"
        mde: Option<String>,
        // Distance - "Distance of the pressure roller from the programmed
        // contour." - "-0.5"
        ab: Option<String>,
        // Z dimension - "For gluing units with a powered Z axis,the Z
        // dimension specification can influence the edge overhangs on the
        // upper and lower edge of the workpiece." - "0" Off by default"
        z_: Option<String>,
        // Edge height - "If the checkbox is activated, the automatic edge
        // holding-down device adjusts to the value specified in the
        // 'Edgeheight' parameter field." - "25" Off by default"
        kh: Option<String>,
        // Additional C angle - This parameter defines an additional gluing
        // unit rotation angle relative to the contour. - "-45"
        p60: Option<String>,
        // Correct additional C-angle - "If the checkbox is activated, the
        // rotation angle of the unit is initially relative to the contour
        // element." - "0"
        ac: Option<String>,
        // Rotating to the start point - "This parameter can switch the
        // automatic determination of the direction of rotation on or off." -
        // "0" Off by default
        // Clockwise = "0"
        // Counterclockwise = "1"
        rsp: Option<String>,
        // Separate mode - "If the checkbox is activated,a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program." - "" Off by default"
        md: Option<String>,
        // Code - "This parameter correspondsto the tool ID for the gluing
        // unit (type of gluing unit)." - "34"
        kn: Option<String>,
        // Tool number - "Enter the number of a suitable tool directly or
        // select onefrom the selection dialog." - "STANDARD" Off by default"
        tno: Option<String>,
        // Feed - Enter feed speed in m/min - "8"
        f_: Option<String>,
        // Speed absolute / relative - Speed, use relative value - "0"
        // DZ="1": Speed relative
        // DZ="0": Speed absolute"
        dz: Option<String>,
        // Speed relative - "Speed in percent of the database
        // value. Evaluation when DZ="0" - "80"
        s_p: Option<String>,
        // Speed absolute - "Speed in rpm, absolute value. Evaluation when
        // DZ="1" - "0"
        s_: Option<String>,
        // Wait time - "Wait time for conveying the edge until it reaches the
        // workpiece and processing can commence." - "0.3"
        ws: Option<String>,
        // Additional edge length - "Adds the value entered in this parameter
        // to the calculated edge length." - "30"
        kz: Option<String>,
        // Parameter set - "This parameter can be used to save or load
        // different definitions ofthe edge parameters (file extension:
        // *.par)." - "STANDARD"
        nm: Option<String>,
        // Comment - "Text field for entering further information about a
        // macro." - "PVC-Kante 2mm REHAU"
        km: Option<String>,
        // Glue section radiator - "If the checkbox is activated, the radiator
        // on the glue section is activated." - "0"
        se: Option<String>,
        // Pre-snip stat.radiator - "If the checkbox is activated, the
        // radiator on the pre-snipping station is activated." - "0"
        sv: Option<String>,
        // Laser power - "If the checkbox is activated, the input laser power
        // in J/cm2 is applied at the process start point." - "20" Off by
        // default"
        ll: Option<String>,
        // TODO: Key is - in documentation is this part of ll or hl?
        // Specify the heating power - If the checkbox is activated, the
        // regulation of the radiator and the hot air nozzle on the gluing
        // unit is activated.
        // -------------------------------------------------------------
        // Heating power decor side - "This parameter isused to enter the
        // percentage value for the heating power of the radiator on the decor
        // side." - "100"
        hl: Option<String>,
        // Heating power glue side - "This parameter is used to enter the
        // percentage value for the operating temperature of the hot air
        // nozzle." - "100"
        hl2: Option<String>,
        // Post pressure roller on - "Defines the distance (in mm) after the
        // programmed start point from which the post-pressure roller swivels
        // on to the workpiece." - "40"
        abn: Option<String>,
        // Edge transport off - "During gluing, the edge is transported out of
        // the gluing unit by the value specified here(in mm)." - "100"
        abv: Option<String>,
        // Post pressure roller off - "Defines the distance (in mm) at the end
        // of gluing from which the post-pressure roller is swung away from
        // the material." - "55"
        aba: Option<String>,
        // "Wait-timeforpost-pressure roller" - "Defines the time (in seconds)
        // that the post-pressure roller presses on the edge at the end of
        // gluing." - "1"
        va: Option<String>,
        // Edge ready time - "This parameter defines at which processing step
        // the edge is loaded in the gluing unit." - "1"
        kb: Option<String>,
        // Snipping offset from end - "This function must be activated if the
        // entire edge material travels with the gluing unit for several
        // gluing operations." - "175" Off by default"
        ka: Option<String>,
        // Edge loop length - "This parameter can be used to set the length of
        // the edge loop between the edgebanding unit and the piggyback edge
        // changer." - "400"
        kso: Option<String>,
        // "Gluing unit distance/pre-snip station" - "This parameter defines
        // the distance between the edge banding unit and the piggyback edge
        // changer." - "0"
        av: Option<String>,
        // Thick edge - "Set this flag only when processing edges with a
        // thickness > 2 mm.Not used for units with a gluing roller .Only
        // required for special units." - "1"
        dk: Option<String>,
        // Lower pre-snipping station - "If the checkbox is activated, the
        // pre-snipping station is lowered during edge drawing." - "0"
        vk: Option<String>,
        // Slow edge drawing - If the checkbox is activated, the edge of the
        // gluing unit is removed from the pre-snipping station with reduced
        // feed speed. - "1"
        la: Option<String>,
        // Edge station with radiator - "If the checkbox is activated,a
        // radiator is available at the pre-snipping station." - "0"
        svk: Option<String>,
        // Butt joint gluing - "If the checkbox is activated, the parameters
        // for defining a butt joint gluing operation are activated." - "0"
        rv: Option<String>,
        // Distance tracing ON - "Specification of the distance between the
        // pressure roller and the start point of the butt joint gluing from
        // which the trace operation is enabled." - "280"
        abt: Option<String>,
        // Tracing feed - "Feed speed in m/min for raised tracer" - "5"
        fk: Option<String>,
        // Tracing path - "Specification of the distance with tracer swiveled
        // in." - "100"
        tb: Option<String>,
        // Joint correction - Input in mm - "0"
        sk: Option<String>,
        // Saw swivelling - "If the checkbox is activated, the cutting angle
        // ofthe saw in the unit positions itself at a right angle to the
        // edge." - "0"
        ks: Option<String>,
        // Deactivate measure - "If the checkbox is activated, gluing is
        // performed without the use of the measuring tracer." - "0"
        dm: Option<String>,
        // Front edge snipping - "If the checkbox is activated,a snipping cut
        // is performed at the front edge." - "0"
        vkk: Option<String>,
        // "Inside corner approach- Change pressure roller" - "This parameter
        // specifies the distance from the inside corner at which the pressure
        // roller is changed from triangular to round." - "50"
        ndwa: Option<String>,
        // "Inside corner approach-Feed after roller change" - "This parameter
        // specifies the feed with which further gluing is to be performed
        // after a roller change." - "8"
        ndda: Option<String>,
        // "Insidecornerreturn-Change pressure roller" - "This parameter
        // specifies the distance from the inside corner at which the pressure
        // roller is changed from round to triangu- lar." - "50"
        ndwe: Option<String>,
        // "Inside corner return - Feed after roller change" - "This parameter
        // specifies the feed with which further gluing is to be performed
        // after a roller change." - "8"
        ndde: Option<String>,
        // "Double edge - offset dimension, front reinforcing shoulder joint"
        // - "This parameter specifies the offset dimension of the support
        // edge to the gluing contour." - "0"
        skv: Option<String>,
        // "Double edge - offset dimension, rear reinforcing shoulder joint" -
        // "This parameter specifies the offset dimension of the support edge
        // to the gluing contour." - "0"
        skh: Option<String>,
        // Swivel angle - This parameter can be used to define the swivel
        // angle for the swiveling edge banding unit. - "0" On by default
        sw: Option<String>,
        // Z edge position - "The Z-value of the contour is specified using
        // the Z-dimension." - "0"
        kp: Option<String>,
        // Split length - "When the split length is specified, the contour is
        // split into individual pieces for the calculation of the correction
        // angle." - "50"
        sl: Option<String>,
        // Correction angle increment - "In the event of a collision,the
        // specified value is repeatedly added to or subtracted from the C
        // offset until a collision-free position is achieved." - "5"
        cf: Option<String>,
        // Safety margin - This parameter offsets the contour, which is
        // processed, parallel to itself. This offset-contour is then checked
        // for collision with the outline of the unit on the level of the
        // edge. - "2"
        sa: Option<String>,
    },
    /// This macro can be used to program the gluing of an edge to a contour
    /// element with an appropriate machine.
    GluingPowerEdge {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_contour: BaseContour,
        glue_edge: GlueEdgeData,
        // Alphanumeric edge code active - This function allows an edge code
        // to be entered in place of the edge channel. - "0" Off by default
        kca: Option<String>,
        // Alphanumeric edge code - "This function allows an edge code to be
        // entered in place of the edge channel." - "0" Off by default"
        kc: Option<String>,
        // Approach mode - "Selection of one out of several predefined
        // approach modes. Within the approach motion, the tool radius
        // compensation is gradually added." - "TAN"
        // Tangential = "TAN"
        // Lateral = "SEI"
        // Lateral inner contour = "INN"
        // Inside corner 90° =  "INN90"
        mda: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN"
        // Tangential = "TAN_AB"
        // Lateral = "SEI_AB"
        // Lateral inner contour = "INN_AB"
        // Inside corner 90° = "INN90_AB"
        mde: Option<String>,
        // Distance - "Distance of the pressure roller from the programmed
        // contour." - "-0.5"
        ab: Option<String>,
        // Z dimension - "For gluing units with a powered Z axis,the Z
        // dimension specification can influence the edge overhangs on the
        // upper and lower edge of the workpiece." - "0" Off by default"
        z_: Option<String>,
        // Edge height - "If the checkbox is activated, the automatic edge
        // holding-down device adjusts to the value specified in the
        // 'Edgeheight' parameter field." - "25" Off by default"
        kh: Option<String>,
        // Additional C angle - This parameter defines an additional gluing
        // unit rotation angle relative to the contour. - "-45"
        p60: Option<String>,
        // Correct additional C-angle - "If the checkbox is activated, the
        // rotation angle of the unit is initially relative to the contour
        // element." - "0"
        ac: Option<String>,
        // Rotating to the start point - "This parameter can switch the
        // automatic determination of the direction of rotation on or off." -
        // "0" Off by default
        // Clockwise = "0"
        // Counterclockwise = "1"
        rsp: Option<String>,
        // Separate mode - "If the checkbox is activated,a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program." - "" Off by default"
        md: Option<String>,
        // Code - "This parameter correspondsto the tool ID for the gluing
        // unit (type of gluing unit)." - "34"
        kn: Option<String>,
        // Tool number - "Enter the number of a suitable tool directly or
        // select onefrom the selection dialog." - "STANDARD" Off by default"
        tno: Option<String>,
        // Feed - Enter feed speed in m/min - "8"
        f_: Option<String>,
        // Speed absolute / relative - Speed, use relative value - "0"
        // DZ="1": Speed relative
        // DZ="0": Speed absolute"
        dz: Option<String>,
        // Speed relative - "Speed in percent of the database
        // value. Evaluation when DZ="0" - "80"
        s_p: Option<String>,
        // Speed absolute - "Speed in rpm, absolute value. Evaluation when
        // DZ="1" - "0"
        s_: Option<String>,
        // Additional edge length - "Adds the value entered in this parameter
        // to the calculated edge length." - "30"
        kz: Option<String>,
        // Parameter set - "This parameter can be used to save or load
        // different definitions ofthe edge parameters (file extension:
        // *.par)." - "STANDARD"
        nm: Option<String>,
        // Comment - "Text field for entering further information about a
        // macro." - "PVC-Kante 2mm REHAU"
        km: Option<String>,
        // Laser power - "If the checkbox is activated, the input laser power
        // in J/cm2 is applied at the process start point." - "20" Off by
        // default"
        ll: Option<String>,
        // TODO: Key is - in documentation is this part of ll or hl?
        // Specify the heating power - If the checkbox is activated, the
        // regulation of the radiator and the hot air nozzle on the gluing
        // unit is activated.
        // -------------------------------------------------------------
        // Heating power decor side - "This parameter isused to enter the
        // percentage value for the heating power of the radiator on the decor
        // side." - "100"
        hl: Option<String>,
        // Heating power glue side - "This parameter is used to enter the
        // percentage value for the operating temperature of the hot air
        // nozzle." - "100"
        hl2: Option<String>,
        // Post pressure roller on - "Defines the distance (in mm) after the
        // programmed start point from which the post-pressure roller swivels
        // on to the workpiece." - "40"
        abn: Option<String>,
        // Edge transport off - "During gluing, the edge is transported out of
        // the gluing unit by the value specified here(in mm)." - "100"
        abv: Option<String>,
        // Post pressure roller off - "Defines the distance (in mm) at the end
        // of gluing from which the post-pressure roller is swung away from
        // the material." - "55"
        aba: Option<String>,
        // "Wait-timeforpost-pressure roller" - "Defines the time (in seconds)
        // that the post-pressure roller presses on the edge at the end of
        // gluing." - "1"
        va: Option<String>,
        // Edge ready time - "This parameter defines at which processing step
        // the edge is loaded in the gluing unit." - "1"
        kb: Option<String>,
        // Snipping offset from end - "This function must be activated if the
        // entire edge material travels with the gluing unit for several
        // gluing operations." - "175" Off by default"
        ka: Option<String>,
        // Edge loop length - "This parameter can be used to set the length of
        // the edge loop between the edgebanding unit and the piggyback edge
        // changer." - "400"
        kso: Option<String>,
        // "Gluing unit distance/pre-snip station" - "This parameter defines
        // the distance between the edge banding unit and the piggyback edge
        // changer." - "0"
        av: Option<String>,
        // Lower pre-snipping station - "If the checkbox is activated, the
        // pre-snipping station is lowered during edge drawing." - "0"
        vk: Option<String>,
        // Slow edge drawing - If the checkbox is activated, the edge of the
        // gluing unit is removed from the pre-snipping station with reduced
        // feed speed. - "1"
        la: Option<String>,
        // Edge station with radiator - "If the checkbox is activated,a
        // radiator is available at the pre-snipping station." - "0"
        svk: Option<String>,
        // Butt joint gluing - "If the checkbox is activated, the parameters
        // for defining a butt joint gluing operation are activated." - "0"
        rv: Option<String>,
        // Joint correction - Input in mm - "0"
        sk: Option<String>,
        // Deactivate measure - "If the checkbox is activated, gluing is
        // performed without the use of the measuring tracer." - "0"
        dm: Option<String>,
        // Front edge snipping - "If the checkbox is activated,a snipping cut
        // is performed at the front edge." - "0"
        vkk: Option<String>,
        // "Double edge - offset dimension, front reinforcing shoulder joint"
        // - "This parameter specifies the offset dimension of the support
        // edge to the gluing contour." - "0"
        skv: Option<String>,
        // "Double edge - offset dimension, rear reinforcing shoulder joint" -
        // "This parameter specifies the offset dimension of the support edge
        // to the gluing contour." - "0"
        skh: Option<String>,
        // Swivel angle - This parameter can be used to define the swivel
        // angle for the swiveling edge banding unit. - "0" On by default
        sw: Option<String>,
        // Z edge position - "The Z-value of the contour is specified using
        // the Z-dimension." - "0"
        kp: Option<String>,
        // Split length - "When the split length is specified, the contour is
        // split into individual pieces for the calculation of the correction
        // angle." - "50"
        sl: Option<String>,
        // Correction angle increment - "In the event of a collision,the
        // specified value is repeatedly added to or subtracted from the C
        // offset until a collision-free position is achieved." - "5"
        cf: Option<String>,
        // Safety margin - This parameter offsets the contour, which is
        // processed, parallel to itself. This offset-contour is then checked
        // for collision with the outline of the unit on the level of the
        // edge. - "2"
        sa: Option<String>,
        // Overhang front - Overhang of the edge material at the front -"3"
        uv: Option<String>,
        // Pressure - Pressure in percent - "100"
        ak: Option<String>,
        // Edge thickness - Thickness of the edge material - "2"
        kd: Option<String>,
        // Processing temperature - The temperature at which the edge material
        // is applied (The operation mode At Airtec must not be used) - "160"
        vt: Option<String>,
        // Processing temperature AirTec - The temperature for AirTec at which
        // the edge material is applied (The operation mode AT Airtec must be
        // used) - "160"
        vta: Option<String>,
        // Operation mode Airtec - The operation mode Airtec is used - "0"
        at: Option<String>,
        // Usage of Heating power - If the checkbox is checked, the heating
        // power for decor and glue side can be specified - "0"
        hla: Option<String>,
    },
    /// 152 - This macro can be used to display polygon features in the
    /// woodWOP graphics without processing them.
    GraphicalComment {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // X/Y position - "Defines the insertion point using X and Y
        // coordinates of the selected coordinate system." - "0"
        xa: Option<String>,
        // X/Y position - "Defines the insertion point using X and Y
        // coordinates of the selected coordinate system." - "0"
        ya: Option<String>,
        // Name - "Name of the polygon definition file (file extension: *.ply)
        // to be added." - ""
        nm: Option<String>,
        // Length - "This parameter is used to change the length of the
        // polygon definition in the X direction." - "0"
        br: Option<String>,
        // Width - "This parameter is used to change the width of the polygon
        // definition in the Y direction." - "32"
        he: Option<String>,
        // Angle - "This angle is used to rotate polygon in the XY plane
        // around the Z axis." - "0"
        wi: Option<String>,
        // Can be mirrored - "If this parameter is selected,the graphical
        // commentis mirrored when the workpiece is mirrored." - "0"
        md: Option<String>,
        // Embed - "If the checkbox is activated,the external file is
        // permanently saved in the woodWOP file." - "1"
        local: Option<String>,
        // Color - "The color display of the graphical comment can be set as
        // required by means of the red, green, and blue components." - "0"
        rot: Option<String>,
        // Color - "The color display of the graphical comment can be set as
        // required by means of the red, green, and blue components." - "0"
        gruen: Option<String>,
        // Color - "The color display of the graphical comment can be set as
        // required by means of the red, green, and blue components." - "0"
        blau: Option<String>,
        // Line Style - Four modes can be selected for the line type - "0"
        // Continous = "0" Dash = "1" Point = "2" Dash point = "3"
        style: Option<String>,
        // Line width - The line width for the display can be freely
        // entered. Minimum value = 1. - "1"
        width: Option<String>,
    },
    /// 194 - This macro is used to program saw grooves on contours. The
    /// contour must be a straight line.
    GroovingContour {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Prolong start - "This parameter moves the actual start point of the
        // operation by the value entered in opposition to the processing
        // direction before the selected start point." - "0" Off by default"
        vls: Option<String>,
        // Prolong end - "This parameter moves the actual end point of the
        // operation by the value entered in the processing direction after
        // the selected end point." - "0" Off by default"
        vle: Option<String>,
        // Insert mode - "Decision if the groove has thefulldepthinits start
        // and end point (then the groove must be extended beyond start and
        // end point)or whether the groove is exactly between start and
        // endpoint." - "MOD0"
        // depth = "MOD0"
        // length = "MOD1"
        em: Option<String>,
        // Side - "Specifies the side of the saw blade's offset with view in
        // the actual contour direction." - "WRKL"
        // left: "WRKL"
        // right:  "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Prescoring depth - "Depth of the prescoring cut. Refers to Z-Start
        // and runs in opposite direction of the Z-axis." - "0"
        tv: Option<String>,
        // Prescoring offset - "Lateral distance of the saw blade relative to
        // the programmed contour.Positive offset moves the prescoring cut in
        // the direction of the programmed side." - "0.0"
        vd: Option<String>,
        // Pre-scoring mode - "Defines the processing direction of the saw
        // blade through the material." - "GL"
        // climb miling = "GL"
        // conventional milling = "GGL"
        mv: Option<String>,
        // Depth - Depth of the finished cut. Refers to Z-Start and runs in
        // opposite direction of the Z-axis. - "0"
        ti: Option<String>,
        // Distance - "Lateral distance of the saw blade relative to the
        // programmed contour.Positive offset moves the prescoring cut in the
        // direction of the programmed side." - "0"
        ab: Option<String>,
        // Z start - "Z value of processing from the defined Z start co-
        // ordinates." - "0"
        zm: Option<String>,
        // Grooving mode - "Defines the processing direction of the saw blade
        // through the material." - "GL"
        // Climb miling = "GL"
        // Conventional milling = "GGL"
        mn: Option<String>,
        // Separate mode - "If the checkbox is activated,a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program.The value entered has 3 characters and is numeric and/or
        // alphanu- meric,depending on the naming of the subpro- gram by the
        // programmer." - "000"
        fm: Option<String>,
        // Groove width - "If the selection box is activated, the groove width
        // value can be entered." - "3.2"
        nb: Option<String>,
        // Adjustment in % - "If thegroovewidthisprogrammedtobelarger than the
        // saw blade width, the adjustment defines
        // thestepwisesawoffsetinpercentof thesaw blade width." - "100"
        xy2: Option<String>,
        // Tool number - "If the checkbox is activated, the input field is
        // activated so the number of a tool can be entered." - "145"
        t_: Option<String>,
        // Feed - Feed speed in m/min - "0"
        f_: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the process motion
        // into the workpiece." - "0"
        zu: Option<String>,
        // Speed - Speed in percent of database value - "100"
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Mechanical Tracing - "This parameter is used to activate the
        // tracing sys- tem" - "0"
        mt: Option<String>,
        // Tracing stroke - "Movementpathinthescanningdirectioninor- der to
        // bring the sensing device into the sensing range." - "0"
        hu: Option<String>,
        // Optimization - "Optimization with regard of speed or of quality
        // when groove width > saw blade width.Speed optimization tries to
        // avoid repositioning moves to the start point of the groove without
        // processing by corresponding rotation of the spindle
        // orientation. Quality optimization will avoid rotation motions of
        // the spindle.In this case "empty" repositionining moves to the
        // startpoint of the groove are caused." - "0
        // 0="Optimized for speed"
        // 1="Quality optimized"
        op: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing. - "0"
        bl: Option<String>,
    },
    /// 196 - This macro can be used for labels.
    Label {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // X/Y position - Defines the insertion point using X and Y
        // coordinates of the selected coordinate system. - "0"
        xa: Option<String>,
        // X/Y position - Defines the insertion point using X and Y
        // coordinates of the selected coordinate system. - "0"
        ya: Option<String>,
        // Tool number - If the checkbox is activated, the input field is
        // activated so the number of a tool can be entered. - "1450"
        tno: Option<String>,
        // Dataset (UPID) - UPID (Unique part ID) of the label to be printed /
        // to be sticked. - ""
        dat: Option<String>,
        // Angle - This angle is used to rotate polygon in the XY plane around
        // the Z axis. - "0"
        wi: Option<String>,
        // NC mode - A user-defined NC subprogram is called up instead of the
        // default approach and return program. - ""
        md: Option<String>,
    },
    /// 129 - This macro is used to program measuring runs. Measuring runs are
    /// only ever possible along one axis. During the measuring run, an offset
    /// is obtained from the programmed dimension of the measuring point to
    /// the actual dimension. This offset is calculated for processing
    /// operations that are dependent on the measuring run.
    Measure {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        // X start - "This parameter is used to specify the start point of the
        // measuring run on the X axe." - "0"
        xa: Option<String>,
        // Y start - "This parameter is used to specify the start point of the
        // measuring run on the Y axe." - "0"
        ya: Option<String>,
        // Z start - "This parameter is used to determine the start point of
        // measuring run on the Z axis." - "0"
        za: Option<String>,
        // Direction - "This parameter is used to define the direction of the
        // measurement run." - "X"
        // Mode X = "X"
        // Mode Y = "Y"
        // Mode Z = "Z"
        md: Option<String>,
        // Measurement - "This parameter is used to determine the measuring
        // point coordinate in the specified measuring run direction." - "0"
        mp: Option<String>,
        // Approach clearance - "Defines a safety distance in the XY plane at
        // which the tool moves downwards along the Z axis." - "10"
        ab: Option<String>,
        // Separate mode - "If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program." - "000" Off by default"
        r#mod: Option<String>,
        // Tool number - Tool number of the unit. - "0" Off by default"
        t_: Option<String>,
        // Measuring with - This parameter is used to select the type of
        // measuring tool. - "0"
        // symbol = "0"
        // plunger = "1"
        po: Option<String>,
        // Permit optimization - "If the checkbox is activated, a measurement
        // macro can also be defined directly before the process to which it
        // relates." - "0"
        oz: Option<String>,
        // Measurement number - "This parameter provides measuring runs with a
        // code that can be used as a reference for measurement dependency in
        // a process." - "---"
        mmn: Option<String>,
    },
    /// 132 - This macro is used to measure an unprocessed part on the machine
    /// table and subsequently move or rotate the reference coordinate systems
    MeasureWorkpiecePosition {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        // Point 1 X/Y - "This parameter is used to specify the first point of
        // the measuring run on the X and Y axes." - "0"
        x1: Option<String>,
        // Point 1 X/Y - "This parameter is used to specify the first point of
        // the measuring run on the X and Y axes." - "0"
        y1: Option<String>,
        // Point 1 Z - "This parameter is used to specify the Z coordinate of
        // the first measuring run." - "0"
        z1: Option<String>,
        // Point 2 X/Y - "This parameter is used to specify the second point
        // of the measuring run on the X and Y axes." - "0"
        x2: Option<String>,
        // Point 2 X/Y - "This parameter is used to specify the second point
        // of the measuring run on the X and Y axes." - "0"
        y2: Option<String>,
        // Point 2 Z - "This parameter is used to specify the Z coordinate of
        // the second measuring run." - "0"
        z2: Option<String>,
        // Point 3 X/Y - "This parameter is used to specify the third point of
        // the measuring run on the X and Y axes." - "0"
        x3: Option<String>,
        // Point 3 X/Y - "This parameter is used to specify the third point of
        // the measuring run on the X and Y axes." - "0"
        y3: Option<String>,
        // Point 3 Z - "This parameter is used to specify the Z coordinate of
        // the third measuring run." - "0"
        z3: Option<String>,
        // Mode - "This parameter determines the corner point to be measured."
        // - "HL"
        // Behind left = "HL"
        // Behind right = "HR"
        // In front left = "VL"
        // In front right = "VR"
        mm: Option<String>,
        // Angle - "Included angle of the corner being calibrated. For
        // right-angled parts: Angle = 90°." - "90"
        wi: Option<String>,
        // -------------------------------------------------------------
        // * Three measuring runs - "If the checkbox is activated,the position
        // calibration of the workpiece is defined by three measuring points."
        // - "1"
        // * Single measuring run in Y direction - "If the checkbox is
        // activated, one single measuring run in the Y direction and two
        // further measuring runs in the X direction are executed,in
        // accordance with the 'Mode' parameter selection." - "0"
        // * Same Z level - "If the checkbox is activated, all measuring
        // points are on the same Z level, meaning that the Z position only
        // has to be specified once." - "1"
        // * Axis-parallel measuring run - "If the checkbox is activated, one
        // axis parallel measuring run is implemented." - "1"
        // -------------------------------------------------------------
        // Measure on unprocessed part edges - "If this parameter is set, the
        // measuring points specified in X/Y are used for the measuring runs,
        // or the measuring points X/Y are determined automatically using the
        // defined unprocessed part dimensions and offsets." - "1"
        vr: Option<String>,
        // TODO: mm is defined twice in the documentation, figure out what is
        //       correct
        // Separate mode - "If the checkbox is deactivated, processing is
        // performed with the standard subprograms." - "0" Off by default"
        // mm: Option<String>,
        // Tool number - Tool number of the unit - "39"
        tno: Option<String>,
        // Measuring with - "This parameter is used to select the type of
        // measuring tool." - "1"
        // symbol = "0"
        // plunger = "1"
        po: Option<String>,
        // Permit optimization - "If the checkbox is activated, a measurement
        // macro can also be defined directly before the process to which it
        // relates." - "0"
        oz: Option<String>,
        // Measurement number - This parameter provides measuring runs with a
        // code that can be used as a reference for measurement dependency in
        // a process - "---"
        mmn: Option<String>,
    },
    /// 117 - This macro is used to program a temporary machine stop.
    NCStop {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // X/Y-position - "For entering the support park position on the X and
        // Y axes in relation to the machine zero point while the stop is
        // active." - "0"
        xa: Option<String>,
        // X/Y-position - "For entering the support park position on the X and
        // Y axes in relation to the machine zero point while the stop is
        // active." - "0"
        ya: Option<String>,
        // Specify the X-Position - "If the checkbox is deactivated, the
        // 'Xposition' parameter is deactivated. A process motion in X does
        // not take place." - "1"
        _x: Option<String>,
        // Specify Y-position - "If the checkbox is activated, the 'Y
        // position' parameter is activated to allow the support park
        // position to be specified on the Y axis." - "0"
        _y: Option<String>,
        // Loosen vacuum - "If the checkbox is activated, the vacuum is
        // loosened on stop. The 'New offset dimension' parameter for
        // specifying the new position of the workpiece is activated." - "0"
        vl: Option<String>,
        // New offset dimension - "This parameter is used to specify the
        // workpiece offset dimensions, in relation to the finished part
        // dimensions, after the stop." - "0"
        xv: Option<String>,
        // New offset dimension - "This parameter is used to specify the
        // workpiece offset dimensions, in relation to the finished part
        // dimensions, after the stop." - "0"
        yv: Option<String>,
        // New offset dimension - "This parameter is used to specify the
        // workpiece offset dimensions, in relation to the finished part
        // dimensions, after the stop." - "0"
        zv: Option<String>,
        // Output message at the appropriate time - "If the checkbox is
        // activated, the message is out- put exactly after the previous
        // NC-command has been processed bythe machine.Otherwiseit is possible
        // the the message is output at another time." - "0"
        zr: Option<String>,
        // Wait time - "This parameter is only available in connection with
        // the woodTime function. Its purpose is to forward an estimated time
        // period for the NC-stop to woodTime. No effect on the real
        // processing of the workpiece." - "0.0"
        wt: Option<String>,
        // Comment - "Text field for entering further information about a
        // macro." - ""
        km: Option<String>,
        // Separate mode - "If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program.The value entered has 3 characters and is numeric and/or
        // alphanumeric, depending on the naming of the subprogram by the
        // programmer." - "000"
        md: Option<String>,
        // Feed - Feed speed in m/min during the support movement to the park
        // position. - "30"
        f_: Option<String>,
        // Park position mode - This new parameter define how the x/y position
        // of the stop macro should behave. - "0"
        // In the workpiece coordinate system: "0"
        // In the machine coordinate system: "1"
        // As free-motion mode position: "2"
        ppm: Option<String>,
    },
    /// 116 - This macro is used to integrate NC sub-programs (in accordance
    /// with DIN) with a defined offset for a CF-unit.
    NCSubProgram {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        // X-position - "NC subprogram offset in X and Y. A zero point offset
        // is implemented." - "0"
        xa: Option<String>,
        // Y-position - "NC subprogram offset in X and Y. A zero point offset
        // is implemented." - "0"
        ya: Option<String>,
        // Z position - "This parameter is used to select the Z value of the
        // element from the defined Z start coordinates of the selected
        // coordinate system." - "0"
        za: Option<String>,
        // Include NC sub-program - "If no sub-programs need to be integrated,
        // only the NC code is written to the NC pro- gram." - "0"
        upc: Option<String>,
        // Sub-routine name - "This parameter is used to integrate an existing
        // sub-program." - ""
        nm: Option<String>,
        // NC code - "Free NC code,whichis entered intothe NC program before
        // NC subprogram call." - "(∗∗∗∗∗ free NC-Code ∗∗∗∗∗)"
        pa: Option<String>,
        // Calculate values - "If formulas and variables are used in the NC
        // code, they are calculated when writing to the NC program." - "1"
        pr: Option<String>,
        // Range the tool - If this parameter is activated, the previously
        // used tool will be returned to the tool changer before the start of
        // a subprogram.
        ta: Option<String>,
    },
    /// 153 - This macro is used to integrate special NC sub-programs with
    /// tool preselection and a defined offset. Remark: A separate
    /// documentation for the use of this macro as CAD/CAM-interface is
    /// available.
    NCSubProgramWithTool {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // X/Y position - "Defines The insertion point using X and Y
        // coordinates of the selected coordinate system." - "0"
        xa: Option<String>,
        // X/Y position - "Defines The insertion point using X and Y
        // coordinates of the selected coordinate system." - "0"
        ya: Option<String>,
        // Z position - "This parameter is used to select the Z value of the
        // element from the defined Z start coordinates of the selected
        // coordinate system." - "0"
        za: Option<String>,
        // Angle - "This angle is used to rotate processing in the XY plane
        // around the Z axis." - "0"
        wi: Option<String>,
        // Name - "A file can be selected from the list of available NC
        // subprograms." - ""
        nm: Option<String>,
        // Embed - "The contents of the subprogram can optionally be saved
        // locally = as part of the mpr-file." - "1"
        // LOCAL="0": Call of an external sub program.
        // LOCAL="1":Subprogrambe- comes part of the mpr-file."
        local: Option<String>,
        // P1 to P4 - "The parameters P1...P4 can be freely defined and are
        // used as variables for the NC subprogram." - "0"
        p1: Option<String>,
        // P1 to P4 - "The parameters P1...P4 can be freely defined and are
        // used as variables for the NC subprogram." - "0"
        p2: Option<String>,
        // P1 to P4 - "The parameters P1...P4 can be freely defined and are
        // used as variables for the NC subprogram." - "0"
        p3: Option<String>,
        // P1 to P4 - "The parameters P1...P4 can be freely defined and are
        // used as variables for the NC subprogram." - "0"
        p4: Option<String>,
        // "Evaluate woodWOP parameters" - "The'Evaluate woodWOP parameters'
        // parameter activates the use of the hood control, suction device and
        // coordinate system parameters." - "0"
        wwp: Option<String>,
        // Tool number - "Enter the number of a suitable tool directly or
        // select one from the selection dialog." - "0"
        wz: Option<String>,
        // Feed - Feed speed in m/min - "0" Off by default"
        f_: Option<String>,
        // Speed - "Speed value. Depending on SM interpretation as relative
        // value (percentage of the database value) or as absolute value." -
        // "0" Off by default"
        s_: Option<String>,
        // Speed input - "Speed in rpm, absolute speed or relative percentage
        // to database entry" - "0"
        // "0" = relative
        // "1" = absolute"
        sm: Option<String>,
        // Enter processing range - "If the checkbox is activated, a
        // rectangular processing range can be defined by two diagonally
        // opposite corner points of the rectangle. Intended for machines with
        // several spindles on one machine axis or with several tables where
        // the risk of collisions between several spindles is given." - "0"
        bba: Option<String>,
        // X1/Y1 - "Defines the X-coordinate of the first corner point of the
        // processing range. Specification in combination with parameter
        // BBA." - "0"
        px1: Option<String>,
        // X1/Y1 - "Defines the Y-coordinate of the first corner point of the
        // processing range. Specification in combination with parameter BBA."
        // - "0"
        py1: Option<String>,
        // X2/Y2 - "Defines the X-coordinate of the second corner point of the
        // processing range. Specification in combination with parameter BBA."
        // - "0"
        px2: Option<String>,
        // X2/Y2 - "Defines the Y-coordinate of the second corner point of the
        // processing range. Specification in combination with parameter BBA."
        // - "0"
        py2: Option<String>,
        // Spindle not vertical - "If the checkbox is activated, the
        // processing range is increased, assuming a horizontal machining
        // spindle. Specification in combination with parameter BBA." - "0"
        kri: Option<String>,
        // Calculate values - If formulas and variables are used in the NC
        // code, they are calculated when writing to the NC program.
        pr: Option<String>,
        // Suitable for 3-axis spindles - "If the checkbox is activated, the
        // NC subprogram can be executed with the corresponding tools
        // available in 3-axis spindles." - "0"
        i3a: Option<String>,
        // Suitable for 4-axis spindles - "If the checkbox is activated, the
        // NC subprogram can be executed with the corresponding tools
        // available in 4-axis spindles." - "0"
        i4a: Option<String>,
        // "Suitable for 5-axis fork type milling heads" - "If the checkbox is
        // activated, the NC subprogram can be executed with the corresponding
        // tools available in 5-axis fork type milling heads." - "0"
        i5t: Option<String>,
        // "Suitable for 5-axis gimbal type milling heads" - "If the check box
        // is deactivated, the NC subprogram can be executed with the
        // corresponding tools available in 5-axis gimbal type milling heads."
        // - "0"
        i5d: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing. - "0"
        bl: Option<String>,
    },
    /// 127 - This macro is used to notch sharp-edged inner or outer corners.
    NotchCorner {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // Contour point - "This parameter is used to select the inside corner
        // to be processed." - "1:1"
        ea: Option<String>,
        // Mode - "Defines how the processing is to be executed. Seen in
        // contour direction:
        // Mode1 = Only one notching operation before the contour point
        // Mode 2 = Only one notching operation behind the contour point
        // Mode 1 & 2 = Notching before and behind the contour
        // point" - "MOD0"
        // Mode 1 = "MOD1"
        // Mode 2 = "MOD2"
        // Mode 1 & 2 = "MOD0"
        md: Option<String>,
        // Additional C angle - "This parameter defines an additional tool
        // rotation angle in relation to the contour." - "0"
        p60: Option<String>,
        // Workpiece distance - Defines the tool clearance parallel to the
        // contour. - "0"
        abv: Option<String>,
        // Corner distance - Defines the tool clearance from the corner. - "0"
        aev: Option<String>,
        // Side - This parameter is used to define the processing side of the
        // tool in the area. - "LI"
        // left: "LI"
        // right: "RE"
        rk: Option<String>,
        // Define values for cut 2 - "When values P61, ABH, AEH for the 2nd
        // cut are not defined, the checkbox will be empty and values from the
        // 1st cut will be used for a 2nd cut, if there is any." - "0"
        // -------------------------------------------------------------
        // Additional C angle - "This parameter defines an additional tool
        // rotation angle in relation to the contour for the 2nd cut." - "0"
        p61: Option<String>,
        // Workpiece distance - "Defines the additional tool clearance
        // parallel to the contour for the 2nd cut." - "0"
        abh: Option<String>,
        // Corner distance - "Defines the additional tool clearance from the
        // corner for the 2nd cut." - "0"
        aeh: Option<String>,
        // Separate mode - "If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and
        // return program." - "" Off by default"
        r#mod: Option<String>,
        // Tool number - Tool number of the unit - "140" Off by default
        tno: Option<String>,
    },
    /// 181 - This macro is used to clean out a closed contour with the pocket
    /// cycle.
    PocketTrimmingFreeform {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_machine_settings: BaseMachineSettings,
        // Contour - "Assignment of the contour to be processed. The contour
        // must be closed. Specification by the order number of the start
        // point." - "1:0"
        ea: Option<String>,
        // Distance - "Offset of the tool relative to the programmed contour."
        // - "0"
        ad: Option<String>,
        // Z start - "Reference value in Z for start of processing. If this
        // value is not specified, processing begins on the workpiece
        // surface." - "0" Off by default"
        za: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the processing direction." - "0"
        ti: Option<String>,
        // Step depth - "If the step depth is less than the trimming depth,
        // then the pocket is processed in several cycles." - "0"
        zt: Option<String>,
        // Adjustment in % - "Specification of the lateral adjustment as
        // percentage of the tool diameter. Value 0 causes only the outer edge
        // to be milled." - "5"
        xy: Option<String>,
        // Trimming direction - "Defines whether the trimmer mills the pocket
        // in a clockwise or counter-clockwise direction." - "1"
        // Clockwise = "0"
        // Counterclockwise = "1"
        ds: Option<String>,
        // A value - "The A axis angle setting is specified here." - "0" Off
        // by default"
        a_: Option<String>,
        // Separate mode - If the checkbox is deactivated, processing is
        // performed with the standard subprograms. - "000" Off by default
        md: Option<String>,
        // Tool number - Tool number of the unit - "101"
        t_: Option<String>,
        // Feed - Feed speed in m/min. - "5"
        f_: Option<String>,
        // Feed oscillation - "Activation of the oscillating milling mode." -
        // "0" :Standard milling without oscillation
        // "1": Milling with oscillation"
        oszi: Option<String>,
        // Feed oscillation - "Feed rate in Z direction for the oscillation
        // motion [m/min]." - "0"
        oszvs: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the process motion
        // into the workpiece." - "0.5" Off by default"
        zu: Option<String>,
        // Speed - Speed in percent of database value - "100"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Approach clearance - "Defines a safety distance in the XY plane at
        // which the workpiece is offset to the entered processing start
        // point." - "0"
        az: Option<String>,
        // Mechanical tracing - "This parameter is used to define the tracing
        // system." - "0"
        mt: Option<String>,
        // Tracing stroke - "Movement path in the scanning direction in order
        // to bring the sensing device into the sensing range." - "0"
        hu: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing. - "0"
        bl: Option<String>,
    },
    /// 151 - This macro is used to program pockets in the underside of the
    /// workpiece.
    PocketTrimmingFromBelow {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_machine_settings: BaseMachineSettings,
        // X/Y center - "Defines the center point of the pocket based on the
        // selected coordinate system.The coordinate entered is the
        // penetration point of the tool." - "0"
        xa: Option<String>,
        // X/Y center - "Defines the center point of the pocket based on the
        // selected coordinate system.The coordinate entered is the
        // penetration point of the tool." - "0"
        ya: Option<String>,
        // Z start - "Reference value in Z for start of processing. If this
        // value is not entered, processing begins on the workpiece surface."
        // - "0" Off by default"
        za: Option<String>,
        // Length - Length of the pocket in X direction. - "0"
        la: Option<String>,
        // Width - Width of the pocket in Y direction. - "0"
        br: Option<String>,
        // Corner radius - "Radius of the pocket-corners. Application to all
        // four corners of the pocket.Possible values are 0 or a value larger
        // than or equal to the tool radius." - "0"
        rd: Option<String>,
        // Angle - Orientation angle of the pocket's length-axis in relation
        // to the X-axis.
        wi: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the processing direction." - "0"
        ti: Option<String>,
        // Step depth - "If the step depth is less than the trimming depth,
        // then the pocket is processed in several cycles." - "0"
        zt: Option<String>,
        // Adjustment in % - "Specification of the lateral adjustment as
        // percentage of the tool diameter. Value 0 causes only the outer
        // edge to be milled." - "80"
        xy: Option<String>,
        // Trimming direction - "Defines whether the trimmer mills the pocket
        // in a clockwise or counter-clockwise direction." - "1"
        // Clockwise = "0"
        // Counterclockwise = "1"
        ds: Option<String>,
        // Arm angle - "Orientation of the under floor unit's arm around the
        // Z-axis of the referenced coordinate system. X-direction = 0. Unit =
        // degrees." - "0"
        awi: Option<String>,
        // Separate mode - "Activation of a checkbox and an edit line for the
        // specification of a separate mode when parameter MD exists in the
        // mpr code.The value must consist of three numerical or
        // alphanumerical digits and calls a corresponding NC-subprogram
        // (which must exist) instead of the standard approach and withdrawal
        // program." - "000" Off by default"
        md: Option<String>,
        // Tool number - Tool number of the unit - "125"
        t_: Option<String>,
        // Feed - Feed speed in m/min - "5" On by default"
        f_: Option<String>,
        // Approach clearance - "Defines a safety distance in the XY plane at
        // which the tool moves downwards along the Z axis." - "30"
        ab: Option<String>,
        // Run under the workpiece - "If the checkbox is activated, the tool
        // approaches from below the workpiece with a safety distance and the
        // tool radius correction is set up below the workpiece contact
        // surface." - "1"
        am: Option<String>,
    },
    /// 123 - This macro is used to program horizontal pockets.
    PocketTrimmingHorizontal {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_machine_settings: BaseMachineSettings,
        // X/Y center - "Defines The center point of the pocket based on the
        // selected coordinate system.The coordinate entered is the
        // penetration point of the tool." - "0"
        xa: Option<String>,
        // X/Y center - "Defines The center point of the pocket based on the
        // selected coordinate system.The coordinate entered is the
        // penetration point of the tool." - "0"
        ya: Option<String>,
        // Z center - "Defines the Z position of the center of the pocket. The
        // tool penetrates the center of the pocket at this Z position." - "0"
        za: Option<String>,
        // Length - Length of the pocket in X direction. - "0"
        la: Option<String>,
        // Height - Defines the height of the pocket in the Z direction. - "0"
        br: Option<String>,
        // Corner radius - "Radius of the pocket-corners. Application to all
        // four corners of the pocket. Possible values are 0 or a value larger
        // than or equal to the tool radius." - "0"
        rd: Option<String>,
        // Angle - The angle describes the work direction of the tool. - "90"
        wi: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the processing direction." - "0"
        ti: Option<String>,
        // Step depth - "If the step depth is less than the trimming depth,
        // then the pocket is processed in several cycles." - "0"
        zt: Option<String>,
        // Adjustment in % - "Specification of the lateral adjustment as
        // percentage of the tool diameter. Value 0 causes only the outer edge
        // to be milled." - "80"
        xy: Option<String>,
        // Trimming direction - "Defines whether the trimmer mills the pocket
        // in a clockwise or counter-clockwise direction." - "1"
        // Clockwise = "0"
        // Counterclockwise = "1"
        ds: Option<String>,
        // A value - The A axis angle setting is specified here. - "0" Off by
        // default"
        a_: Option<String>,
        // Separate mode - "Acitvation of a checkbox and an editline for the
        // specification of a separate mode when parameter MD exists in
        // the mpr code.The value must consist of three numerical or
        // alphanumerical digits and calls a corresponding NC-subprogram
        // (which must exist)instead of the standard approach and withdrawal
        // program." - "000" Off by default"
        md: Option<String>,
        // Tool number - Tool number of the unit - "167"
        tno: Option<String>,
        // Feed - Feed speed in m/min - "5" On by default"
        f_: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Approach clearance - "Defines a safety distance in the XY plane at
        // which the tool moves downwards along the Z axis." - "10"
        ab: Option<String>,
    },
    /// 112 - This macro is used to trim vertical pockets to depth or as a
    /// cut-out.
    PocketTrimmingVertical {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_machine_settings: BaseMachineSettings,
        // X/Y center - "Defines the center point of the pocket based on the
        // selected coordinate system. The coordinate entered is the penetration
        // point of the tool." - "0"
        xa: Option<String>,
        // X/Y center - "Defines the center point of the pocket based on the
        // selected coordinate system. The coordinate entered is the
        // penetration point of the tool." - "0"
        ya: Option<String>,
        // Z start - "Reference value in Z for start of processing. If this
        // value is not entered, processing begins on the workpiece surface."
        // - "0" Off by default"
        za: Option<String>,
        // Length - Length of the pocket in X direction. - "0"
        la: Option<String>,
        // Width - "Defines the width of the pocket in the Y direction." - "0"
        br: Option<String>,
        // Corner radius - "Radius of the pocket-corners. Application to all
        // four corners of the pocket. Possible values are 0 or a value larger
        // than or equal to the tool radius." - "0"
        rd: Option<String>,
        // Angle - "Orientation angle of the pocket's length-axis in relation
        // to the X-axis." - "0"
        wi: Option<String>,
        // Depth - "Depth of the processing from the defined start point in the
        // processing direction." - "0"
        ti: Option<String>,
        // Step depth - "If the step depth is less than the trimming depth,
        // then the pocket is processed in several cycles." - "0"
        zt: Option<String>,
        // Adjustment in % - "Specification of the lateral adjustment as
        // percentage of the tool diameter. Value 0 causes only the outer edge
        // to be milled." - "80"
        xy: Option<String>,
        // Trimming direction - "Defines whether the trimmer mills the
        // pocket in a clockwise or counter-clockwise direction." - "1"
        // Clockwise = "0"
        // Counterclockwise = "1"
        ds: Option<String>,
        // Separate mode - "Acitvation of a checkbox and an editline for the
        // specification of a separate mode when parameter MD exists in the
        // mpr code. The value must consist of three numerical or
        // alphanumerical digits and calls a corresponding NC-subprogram
        // (which must exist) instead of the standard approach and withdrawal
        // program." - "000" Off by default"
        md: Option<String>,
        // Tool number - Tool number of the unit - "101"
        tno: Option<String>,
        // Feed - Feed speed in m/min - "5" On by default"
        f_: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the process motion
        // into the workpiece." - "0"
        zu: Option<String>,
        // Feed oscillation - "Activation of the oscillating milling mode." -
        // "0": Standard milling without oscillation
        // "1": Milling with oscillation"
        oszi: Option<String>,
        // Feed Oscillation - "Feed rate in Z direction for the oscillation
        // motion [m/min]. Available when XY ="0"." - "0" Off by default"
        oszvs: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing.
        bl: Option<String>,
    },
    /// 141 - This macro is used to program pockets that are executed under an
    /// A-angle.
    PocketTrimmingWithAAngle {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_machine_settings: BaseMachineSettings,
        // X/Y center - "Defines the center point of the pocket based on the
        // selected coordinate system.The coordinate entered is the
        // penetration point of the tool." - "0"
        xa: Option<String>,
        // X/Y center - "Defines the center point of the pocket based on the
        // selected coordinate system.The coordinate entered is the
        // penetration point of the tool." - "0"
        ya: Option<String>,
        // Z start - "Reference value in Z for start of processing.If this
        // value is not entered,processing begins on the workpiece surface." -
        // "0" Off by default"
        z_: Option<String>,
        // Length - Length of the pocket in X direction. - "0"
        la: Option<String>,
        // Width - Defines the width of the pocket in the Y direction. - "0"
        br: Option<String>,
        // Corner radius - "Radius of the pocket-corners. Application to all
        // four corners of the pocket. Possible values are 0 or a value larger
        // than or equal to the tool radius." - "0"
        rd: Option<String>,
        // Angle - "Orientation angle of the pocket's length-axis in relation
        // to the X-axis." - "0"
        wi: Option<String>,
        // Depth - "Depth of the processing from the defined start point in
        // the processing direction." - "0"
        ti: Option<String>,
        // Step depth - "If the step depth is less than the trimming depth,
        // then the pocket is processed in several cycles." - "0"
        zt: Option<String>,
        // Adjustment in % - "Specification of the lateral adjustment as
        // percentage of the tool diameter. Value 0 causes only the outer edge
        // to be milled." - "80"
        xy: Option<String>,
        // Trimming direction - "Defines whether the trimmer mills the pocket
        // in a clockwise or counter-clockwise direction." - "1" Clockwise =
        // "0" Counterclockwise = "1"
        ds: Option<String>,
        // A value - The A axis angle setting is specified here. - "0"
        a_: Option<String>,
        // C value - "Specification of the C axis angle. Without specification
        // of A_ and C_ the tool orientation is derived by the perpendicular
        // direction of the assigned level." - "0"
        c_: Option<String>,
        // Separate mode - Activation of a checkbox and an editline for the
        // specification of a separate mode when parameter MD exists in the
        // mpr code. The value must consist of three numerical or
        // alphanumerical dig- its and calls a corresponding NC-subprogram
        // (which must exist) instead of the standard approach and withdrawal
        // program. - "000" Off by default
        md: Option<String>,
        // Tool number - Tool number of the unit - "101"
        t_: Option<String>,
        // Feed - Feed speed in m/min - "5" On by default"
        f_: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the process motion
        // into the workpiece." - "0"
        zu: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Operation from below - "When WIU, ANU or AMU are specified,
        // operation from below is active"
        // ------------------------------------------------------------- Arm
        // angle - "For operation from below. Orientation of the milling
        // unit's arm around the Z-axis of the referenced coordinate
        // system. X-direction = 0. Unit = degrees. Without specification of
        // A_ and C_ (= tool perpendicular to XY-plane), the specification of
        // WIU, and ANU is recommended when an operation from below has to be
        // programmed. When A_ and C_ are specified, WIU is computed
        // automatically. The specification of ANU is sufficient." - "90"
        wiu: Option<String>,
        // Approach clearance - "For operation from below. Defines a safety
        // distance in the XY plane from the starting point at which the tool
        // moves downwards in Z-direction." - "0"
        anu: Option<String>,
        // Run under the workpiece - "For operation from below. When active,
        // the unit / tool is moved downwards in Z-direction to a level below
        // the bottom surface of the workpiece keeping the approach clearance
        // from the start point. The approach to the startpoint and the
        // activation of the tool radius correction is made below the
        // workpiece. When inactive,the unit/tool is just moved downwards to
        // the Z-level of the milling operation.The approach to the startpoint
        // and the activation of the tool radius correction is made on this
        // level." - "0"
        amu: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing. - "0"
    },
    /// 154 - This macro is used to project contours with a laser.
    ProjectContour {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        synchronous_mode: BaseSynchronousModeBehavior,
        base_contour: BaseContour,
        // Side - "Specifies the side of the laser offset with view in the
        // actual contour direction." - "NoWRK"
        // left: "WRKL"
        // right: "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Z dimension - "Use for the correction of the laser projection to
        // the Z-level where the beam actually impinges on the workpiece (for
        // projection lasers, where the laser beam propagates in conical
        // manner)." - "0" Off by default"
        _z: Option<String>,
        // Distance - "Lateral offset of the laser projection with respect to
        // the contour. Only effective in combination with side = "WRKL" or
        // "WRKR". Positive values shift the laser beam in direction of the
        // selected side, negative values in opposite direction." - "0"
        ab: Option<String>,
        // Feed - Enter the feed speed in m/min - "STANDARD" Off by default"
        f_: Option<String>,
        // Cycles - "This parameters defines how often the contour must be
        // displayed by the cross laser." - "1"
        zy: Option<String>,
        // Number - Defines the number displayed by the projection laser. -
        // "1" Off by default
        nr: Option<String>,
    },
    /// 134 - This macro can be used to program the pressure zone for rebate
    /// gluing.
    RabbetPressureZone {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_contour: BaseContour,
        // Side - "This parameter is used to define the processing side of the
        // tool in the area." - "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        rk: Option<String>,
        // Distance - "Distance of the pressure roller from the programmed
        // contour." - "0"
        ab: Option<String>,
        // Z dimension - "Specification of the Z coordinate.Enables unit
        // positioning to the rabbet pressure zone." - "0"
        ho: Option<String>,
        // NC mode - "The value entered has 3 characters and is numeric and/or
        // alphanumeric, depending on the naming of the subprogram by the
        // programmer." - ""
        r#mod: Option<String>,
        // Tool number - "If the checkbox is activated, the input field is
        // activated so the number of a tool can be entered." - "170"
        tno: Option<String>,
        // Feed - Enter feed speed in m/min. - "5"
        f_: Option<String>,
        // Approach clearance - "Defines a safety distance in addition to the
        // defined pro- cess start point at which the tool is lowered along
        // the Z axis." - "30"
        an: Option<String>,
        // Withdrawl clearance - Defines a safety distance in addition to the
        // defined process end point at which the tool is raised along the Z
        // axis. - 30
        // ab: Option<String>,
        // Pressure pad heat output - "If the checkbox is activated, the
        // percentage value entered in the input field is applied to the heat
        // output at the pressure pad." - "100"
        hlds: Option<String>,
        // Heating time - Specification of heating time in seconds. - "10"
        ti: Option<String>,
        // Heating power - This parameter is used to enter the percentage
        // value for the operating temperature of the hot air nozzle. - "10"
        hl: Option<String>,
    },
    /// 182 - This macro helps to achieve better results with the suction unit
    /// recommendation function by marking the workpiece and blocked areas.
    ReduceOffcuts {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // Contour - "This parameter is used to select which contour
        // definition is processed with the macro." - "1:0"
        ea: Option<String>,
        // Area type - "This parameter defines the function of the previously
        // selected contour. For an un-ambiguous definition, only one of the
        // parameters WE,RT,SF, SO may have value "1" at the same time.(with
        // the exception of SF+SO)" - "1" Workpiece: "1"
        we: Option<String>,
        // Area type - "This parameter defines the function of the previously
        // selected contour. For an un-ambiguous definition, only one of the
        // parameters WE,RT,SF, SO may have value "1" at the same time.(with
        // the exception of SF+SO)" - "0" Offcut: "1"
        rt: Option<String>,
        // Residual mode - "Residual mode works exclusively in combination with
        // area type "Workpiece".Only one of RW, NR may have value "1" at the
        // same time." - "0" Reduce offcuts: "1"
        rw: Option<String>,
        // Residual mode - "Residual mode works exclusively in combination with
        // area type "Workpiece".Only one of RW, NR may have value "1" at the
        // same time." - "0" Reduce nesting offcuts: "1"
        nr: Option<String>,
        // Blocking surface top - "Blocking surface works exclusively in
        // combination with area type "Blocked"." - "Top: SO="1"
        so: Option<String>,
        // Blocking surface bottom - "Blocking surface works exclusively in
        // combination with area type "Blocked -
        // "Bottom: SF="1"
        // Top+Bottom=SO="1" and SF="1"
        sf: Option<String>,
        // Generate tracks - "For area types "Workpiece" or "Offcut",this
        // parameter allows you to select which tracks are" - "0"
        // All = "0"
        // Only at edge = "1"
        // Only between the parts = 2
        mzk: Option<String>,
        // Milling in steps - "If active, the contour is processed according
        // to the entry for the Number of adjustments parameter in several
        // increments."  - "0"
        stufen: Option<String>,
        // Number of adjustments - "If the milling in steps parameter is
        // activated, the contouris milled in the number of steps specified by
        // this parameter." - "0"
        anzzst: Option<String>,
        // Z Start - "If the milling in steps parameter is activated,a
        // reference value for Z is defined for the first adjustment of the
        // tool using Z start." - "0"
        zstart: Option<String>,
        // Engage on-the-fly - "Must be set to "1" to activate." - "0"
        em: Option<String>,
        // Engage on-the-fly - "This parameter defines the length of the
        // immersion." - "15" Off by default"
        et: Option<String>,
        // Z dimension - "The Z dimension determines the processing height of
        // the tools." - "-3"
        za: Option<String>,
        // Distance - "Offset of the tool relative to the programmed contour."
        // - "0"
        r#as: Option<String>,
        // Separate mode - "If not specified,processing is performed with the
        // standard subprograms." - "0" Off by default"
        md: Option<String>,
        // Tool number - "Enter the number of a suitable tool directly or
        // select one from the selection dialog." - "101"
        t_: Option<String>,
        // Feed - Feed speed in min/m - "0" Off by default"
        f_: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the process motion
        // into workpiece." - "0.5"
        zu: Option<String>,
        // Feed oscillation - "Activation of the oscillating milling mode." -
        // "0": Standard milling without oscillation
        // "1": Milling with // oscillation"
        oszi: Option<String>,
        // Feed oscillation - "Feed rate in Z direction for the oscillation
        // motion [m/min]." - "0"
        oszvs: Option<String>,
        // Approach clearance - "Defines a safety distance perpendicular to
        // the XY plane. Distance between the Z-start point of the pocket
        // processing step and the reference point of the tool." - "20"
        az: Option<String>,
        // "Calculate parameters automatically" - "If active, the parameters
        // for the horizontal and vertical cuts are determined automatically."
        // - "0"
        pt: Option<String>,
        // Grid size - "Defines The distance between horizontal cuts ( 'Grid
        // size in X direction') and vertical cuts( 'Grid size in Y direction'
        // )." - "200"
        rx: Option<String>,
        // Grid size - "Defines The distance between horizontal cuts ( 'Grid
        // size in X direction') and vertical cuts( 'Grid size in Y direction'
        // )." - "200"
        ry: Option<String>,
        // Minimum track length - "Minimum length for an offcut reduction
        // track. Shorter tracks are rejected." - "50"
        mb: Option<String>,
        // Maximum offcut length - "This parameter defines whether horizontal
        // and vertical cuts are carried out on an offcut area." - "200"
        sw: Option<String>,
        // Maximum strip length - "This parameter defines the maximum strip
        // length for longi- tudinal processing." - "10000"
        ms: Option<String>,
        // "Separate offcuts from workpiece" - "Trimming takes place along the
        // programmed workpiece contour after the offcuts have been reduced."
        // - "0"
        // Reduce offcuts only = "0"
        // Reduce offcuts and separate from workpiece = "1"
        // Separate offcuts from workpiece only = "2"
        tr: Option<String>,
        // Horizontal cuts - "If active, trimming is executed to separate
        // residual parts in X direction." - "0"
        hs: Option<String>,
        // Cutting direction - "This parameter defines the processing
        // direction of the horizontal cuts." - "0"
        // X+ = "1"
        // X- = "0"
        // X zigzag = "0"
        xp: Option<String>,
        // Cutting direction - "This parameter defines the processing
        // direction of the horizontal cuts." - "0"
        // X+ = "0"
        // X- = "1"
        // X zigzag = "0"
        xm: Option<String>,
        // Cutting direction - "This parameter defines the processing
        // direction of the horizontal cuts." - "0"
        // X+ = "0"
        // X- = "0"
        // X zigzag = "0"
        zh: Option<String>,
        // Cut sequence Y+ - "Defines The processing sequence of the
        // horizontal cuts ( X direction )." - "0"
        // Y-="0"
        // Y+="1"
        sy: Option<String>,
        // Vertical cuts - "If active, trimming is executed to separate
        // residual parts in Y direction." - "0"
        vs: Option<String>,
        // Cutting direction - "This parameter defines the processing
        // direction of the vertical cuts." - "0"
        // Y+ = "1"
        // Y- = "0"
        // Y zigzag = "0"
        yp: Option<String>,
        // Cutting direction - "This parameter defines the processing
        // direction of the vertical cuts." - "0"
        // Y+ = "0"
        // Y- = "1"
        // Y zigzag = "0"
        ym: Option<String>,
        // Cutting direction - "This parameter defines the processing
        // direction of the vertical cuts." - "0"
        // Y+ = "0"
        // Y- = "0"
        // Y zigzag = "0"
        zv: Option<String>,
        // Cut sequence X+ - If active, the processing for reducing offcut
        // areas in X direction ( 'Horizontal cuts' ) is executed before the
        // processing in Y direction ( 'Vertical cuts' ). - "0"
        sx: Option<String>,
        // "Horizontal cuts before vertical" - "If active, the processing for
        // reducing offcut areas in X direction ( 'Horizontal cuts' ) is
        // executed before the processing in Y direction ( 'Vertical cuts' )."
        // - "0"
        hv: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing.
        bl: Option<String>,
    },
    /// 171 - This macro is used to round off an edged 90 degree corner with
    /// the corner rounding unit.
    RoundOffCorner {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        // Contour point - This parameter is used to select the outer corner
        // to be processed. - "1:1"
        ea: Option<String>,
        // Side - "This parameter defines on which side the tool moves
        // relative to the processing direction." - "LI"
        // left: "LI"
        // right: "RE"
        rk: Option<String>,
        // Distance before - "Defines the distance of the unitto the contour
        // element before the selected point in the programmed contour
        // definition direction." - "0"
        abv: Option<String>,
        // Distance after - "Defines the distance of the unit to the contour
        // element after the selected point in the programmed contour
        // definition direction." - "0"
        abn: Option<String>,
        // Separate mode - "If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program." - "" Off by default"
        r#mod: Option<String>,
        // Tool number - Tool number of the unit - "140" Off by default"
        tno: Option<String>,
    },
    /// 125 - This macro can be used to program sanding operations on
    /// contours.
    SandingVertical {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Approach mode - "Selection of one out of severalpre- defined
        // approach modes.Withinthe approach motion, the tool radius com-
        // pensation is gradually added." - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        mda: Option<String>,
        // Side - "Theside is used tospecify the tool offset in the trimming
        // direction, from the programmed start point to the end point." -
        // "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN_AB"
        // tangential: "TAN_AB"
        // lateral: "SEI_AB"
        // vertical: "SEN_AB"
        mde: Option<String>,
        // Z dimension - "The Z dimension determines the processing height of
        // the tools." - "0"
        za: Option<String>,
        // Distance - "Offsetof thetoolrelativetotheprogrammed contour." -
        // "-0.5"
        ab: Option<String>,
        // Additional C angle - "This parameter definesanadditional gluing
        // unit rotation angle relative to the contour." - "0"
        p60: Option<String>,
        // Separate mode - "If thecheckbox isdeactivated,processing is
        // performed with the stan- dard subprograms." - "000" Off by default"
        md: Option<String>,
        // Tool number - Tool number of the unit - "200"
        tno: Option<String>,
        // Feed - Feed speed in m/min. - "8" On by default"
        f_: Option<String>,
        // Feed oscillation - "Activation of the oscillating milling mode." -
        // "0":Standard milling without oscillation
        // "1": Milling with oscillation"
        oszi: Option<String>,
        // Feed Oscillation - "Feed rate in Z direction for the oscillation
        // motion. [m/min]" - "0" Off by default"
        oszvs: Option<String>,
        // Speed - Speed in percent of database value - "100"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Speed - SM must be set to 1 to activate the use of S_ or S_A - "0"
        sm: Option<String>,
    },
    /// 193 - This macro is used to program saw cuts on contour definitions.
    SawingContour {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Prolong start - "This parameter moves the actual start point of the
        // operation by the value entered in opposition to the processing
        // direction before the selected start point." - "0" Off by default"
        vls: Option<String>,
        // Prolong end - "This parameter moves the actual end point of the
        // operation by the value entered in the processing direction after
        // the selected end point." - "0" Off by default"
        vle: Option<String>,
        // Z dimension - Determines the processing height of the tools. -
        // "10.0"
        zm: Option<String>,
        // Approach Z - "Defines the approach position of the tool if the tool
        // is perpendicular to the XY plane and if ZA=1." - "0"
        z_: Option<String>,
        // Activate approach Z - Activates use of the Approach Z parameter -
        // "0"
        za: Option<String>,
        // Approach mode - "Selection of one out of several predefined
        // approach modes. Within the approach motion, the tool radius com-
        // pensation is gradually added." - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        mda: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        mde: Option<String>,
        // Distance - "Lateral distance of the sawblade relative to the
        // programmed contour. Positive offset moves the prescoring cut in the
        // direction of the programmed side." - "1.0"
        ab: Option<String>,
        // Separate mode - "The checkbox separate mode may be activated by
        // adding the parameter MD to the mpr code.The value must con- sist of
        // three numerical or alphanumerical digits and calls a corresponding
        // NC-subprogram (which must exist) instead of the standard approach
        // and return program." - "000"
        md: Option<String>,
        // Side - "Specifies the side of the saw blade's offset with view in
        // the actual contour direction." - "WRKL"
        // left = "WRKL"
        // right = "WRKR"
        // centric = "NoWRK"
        rk: Option<String>,
        // Tool number - "If the checkbox is activated, the input field is
        // activated so the number of a tool can be entered." - "145"
        tno: Option<String>,
        // Feed - Feed speed in m/min - "0"
        f_: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the process motion
        // into the workpiece." - "0"
        f_z: Option<String>,
        // Speed - Speed in percent of database value - "100"
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing. - "0"
        bl: Option<String>,
        // Trimming direction - This parameter defines the trimming direction
        // mode. Depending on the this, the tool will changed or the direction
        // will changed. - "0"
        // manually: "0"
        // synchronous rotation (change direction): "1"
        // reverse rotation (change direction): "2"
        // synchronous rotation (change tool): "3"
        // reverse rotation (change tool): "4"
        tdm: Option<String>,
    },
    /// 109 - This macro is used to program vertical saw cuts.
    SawingVertical{
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // X start point - "Defines the start point for processing on the X
        // and Y axes." - "0"
        xa: Option<String>,
        // Y start point - "Defines the start point for processing on the X
        // and Y axes." - "0"
        ya: Option<String>,
        // X end point - "Defines the end point for processing on the X and Y
        // axes." - "0"
        xe: Option<String>,
        // Y end point - "Defines the end point for processing on the X and Y
        // axes." - "0"
        ye: Option<String>,
        // Z start - "Reference value in Z for start of processing. If this
        // value is not specified, processing begins on the workpiece
        // surface." - "0"
        za: Option<String>,
        // Distance - "Finishcut: Lateral offset of the tool relative to the
        // connecting line between startpoint and endpoint. Positive values
        // shift the cut in direction of the selected side, negative values
        // shift the cut in opposite direction." - "0"
        ad: Option<String>,
        // Insert mode - "Decision if the groove has the full depth in its
        // start and end point (then the groove must be extended beyond start
        // and end point) or whether the groove is exactly between start and
        // endpoint." - "MOD0"
        // depth = "MOD0"
        // length = "MOD1"
        // length + security distance = "MOD2"
        em: Option<String>,
        // Optimization - "Optimization with regard of speed or of quality
        // when groove width > saw blade width. Speed optimization tries to
        // avoid repositioning moves to the startpoint of the groove without
        // processing by corresponding rotation of the spindle
        // orientation. Quality optimization will avoid rotation motions of
        // the spindle. In this case "empty" repositionining - moves to the
        // startpoint of the groove are caused." - "0"
        // optimized for speed = "0"
        // quality optimized = "1"
        op: Option<String>,
        // Prescoring depth - "Depth of the prescoring cut.Refers to Z-Start
        // and runs in opposite direction of the Z-axis.Only usable when
        // parameter TI (depth) is active." - "0"
        tv: Option<String>,
        // Prescoring Z - "Z-value of the prescoring cut relative to the
        // reference coordinate system. Only usable when parameter Z_ (Z
        // value) is active." - "0.0"
        vz: Option<String>,
        // Scoring Distance - "Prescoring cut: Lateral offset of the tool
        // relative to the connecting line between startpoint and
        // endpoint. Positive values shift the cut in direction of the
        // selected side, negative values shift the cut in opposite
        // direction." - "0"
        vt: Option<String>,
        // Pre-scoring mode - "Defines the processing direction of the saw
        // blade through the material." - "GL"
        // climb milling = "GL"
        // conventional milling = "GGL"
        mv: Option<String>,
        // Depth - Depth of the finish cut. Refers to Z-Start and runs in
        // opposite direction of the Z-axis. Mutually exclusive with Z_. - "0"
        ti: Option<String>,
        // Z value - "Z-value of the finish cut relative to the reference
        // coordinate system. Mutually exclusive with TI (depth)." - "0"
        z_: Option<String>,
        // Saw mode - "Defines the processing direction of the saw blade
        // through the material." - "GL"
        // Climb milling = "GL"
        // Conventional milling = "GGL"
        mn: Option<String>,
        // Separate mode - "If the checkbox is activated, a user- defined NC
        // subprogram is called up instead of the default approach and return
        // program. The Value Entered has 3 characters and is numeric and/or
        // alpha numeric, depending on the naming of the subprogram by the
        // programmer." - "000"
        fm: Option<String>,
        // Side - "The side is used to specify the tool offset viewed from
        // the programmed start and end points." - "WRKL"
        // left = "WRKL"
        // right = "WRKR"
        // centric = "NoWRK"
        rk: Option<String>,
        // Groove width - "If the selection box is activated,the groove width
        // value can be entered." - "3.2"
        nb: Option<String>,
        // Adjustment in % - "If the groove width is programmed to be larger
        // than the saw blade width, the adjustment defines the step wise saw
        // offset in percent of the saw blade width." - "100"
        xy: Option<String>,
        // Tool number - "If the checkbox is activated, the input field is
        // activated so the number of a tool can be entered." - "140"
        t_: Option<String>,
        // Feed - Feed speed in m/min - "0"
        f_: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the process motion
        // into the workpiece." - "0"
        zu: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing.
        bl: Option<String>,
    },
    /// 124 - This macro is used to program swiveled saw cuts, e.g. miter cuts.
    SawingWithAAngle {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        // X start point - "Defines the start point for processing on the X
        // and Y axes." - "0"
        xa: Option<String>,
        // Y start point - "Defines The start point for processing on the X
        // and Y axes." - "0"
        ya: Option<String>,
        // X end point - "Defines The end point for processing on the X and Y
        // axes." - "0"
        xe: Option<String>,
        // Y end point - "Defines The end point for processing on the X and Y
        // axes." - "0"
        ye: Option<String>,
        // Z start - "Reference value in Z for start of processing. If this
        // value is not specified, processing begins on the workpiece
        // surface."  - "0"
        za: Option<String>,
        // XY information on ... - "Determines the Z coordinate to which the
        // previously entered X/Y coordinates of the start and end point are
        // linked." - "0"
        // Z start = "0"
        // Z=0 = "1"
        xy: Option<String>,
        // Lateral correction relating to tool - "Defines The alignment of the
        // sawing process at a swivel angle of 0 degrees." - "1"
        sk: Option<String>,
        // Swiveling angle - "Tool angle setting on unit.The depth is
        // calculated in the direction of the swiveled process." - "45"
        wi2: Option<String>,
        // Prescoring depth - "Depth of the prescoring cut. Refers to Z-Start
        // and runs in opposite direction of the Z-axis. Only usable when
        // parameter TI (depth) is active." - "0"
        tv: Option<String>,
        // Prescoring Z - "Z-value of the prescoring cut rel- ative to the
        // reference coordinate system.Only usable when parameter Z_ (Z value)
        // is active." - "0.0"
        vz: Option<String>,
        // Scoring Distance - "Prescoring cut: Lateral offset of the tool
        // relative to the connecting line between start point and
        // endpoint. Positive values shift the cut in direction of the
        // selected side, negative values shift the cut in opposite
        // direction." - "0"
        vt: Option<String>,
        // Pre-scoring mode - "Processing direction of the saw blade through
        // the material." - "GL"
        // climb milling = "GL"
        // conventional milling = "GGL"
        mv: Option<String>,
        // Depth - "Depth of the finish cut. Refers to Z-Start and runs in
        // opposite direction of the Z-axis. Mutually exclusive with Z_." -
        // "0"
        ti: Option<String>,
        // Z value - "Z-value of the finish cut relative to the reference
        // coordinate system. Mutually exclusive with TI." - "0"
        z_: Option<String>,
        // Distance - "Finishcut: Lateral offset of the tool relative to the
        // connecting line between startpoint and end point. Positive values
        // shift the cut in direction of the selected side, negative values
        // shift the cut in opposite direction." - "0"
        ab: Option<String>,
        // Saw mode - "Defines the processing direction of the saw blade
        // through the material." - "GL"
        // Climb milling = "GL"
        // Conventional milling = "GGL"
        mn: Option<String>,
        // Separate mode - "If defined, a user-defined NC subprogram is called
        // up instead of the default approach and return program. The value
        // entered has 3 characters and is numeric and/or alphanumeric,
        // depending on the naming of the sub program by the programmer." -
        // "000"
        fm: Option<String>,
        // Side - "Specifies the side of the saw blade's offset with view
        // direction from the programmed start point to the programmed end
        // point." - "WRKL"
        // left = "WRKL"
        // right = "WRKR"
        // centric = "NoWRK"
        rk: Option<String>,
        // Groove width - Defines groove width. - "3.2"
        nb: Option<String>,
        // Adjustment in % - "If the groove width is programmed to be larger
        // than the saw blade width, the adjustment defines the step wise saw
        // offset in percent of the saw blade width." - "100"
        xy2: Option<String>,
        // Tool number - "The number of the tool to be used." - "145"
        t_: Option<String>,
        // Feed - Feed speed in m/min - "0"
        f_: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the pro- cess motion
        // into the workpiece." - "0"
        zu: Option<String>,
        // Speed - "Speed in percent of database value" - "100" Off by
        // default"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing - "0"
        bl: Option<String>,
    },
    /// 122 - This macro is used to combine contour-dependent processes into
    /// one production run.
    Sequence {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        base_contour: BaseContour,
        // Name - A name for this sequence. - ""
        nm: Option<String>,
        // Number of macros - This defines how many of the following macros
        // are to be considered part - "0"
        dp: Option<String>,
    },
    /// 108 - This macro defines the snipping of the overhanging edge with the
    /// saw unit after the gluing of the edge.
    SnippingEdges {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_contour: BaseContour,
        // Front edge - "If non-zero, a snipping cut is performed at the front
        // edge (first saw cut)." - "1"
        vk: Option<String>,
        // Additional C-Angle - "This parameter is used to define an
        // additional angle of rotation of the unit in relation to the
        // workpiece." - "0"
        p60: Option<String>,
        // Distance - "Distance of the saw cut from the defined snipping point
        // on the front edge." - "0"
        abv: Option<String>,
        // Side - "This parameter is used to define the processing side of the
        // tool in the area." - "1"
        // Left: "1"
        // Right: "2"
        rk: Option<String>,
        // Rear edge - "If non-zero, a snipping cut is performed at the rear
        // edge ( second saw cut )." - "1"
        hk: Option<String>,
        // Additional C-Angle - "This is used to define an additional angle of
        // rotation of the unit in relation to the workpiece." - "0"
        p61: Option<String>,
        // Distance - "Distance of the saw cut from the defined snipping point
        // on the rear edge." - "0"
        abh: Option<String>,
        // Separate mode - "If specified, a user-defined NC subprogram is
        // called instead of the default approach and return program.The value
        // entered has 3 characters and is numeric and/or alphanumeric,
        // depending on the naming of the subprogram by the programmer." -
        // "001"
        md: Option<String>,
        // Tool number - Tool number of the unit - "140"
        t_: Option<String>,
        // Feed - Feed speed in m/min - "STANDARD"
        f_: Option<String>,
        // Speed - Tool speed in 1/min. - "STANDARD"
        s_: Option<String>,
        // Snip front edge before rear edge - "If the checkbox is
        // activated,thefrontedgeis snipped before the rear edge." - "0"
        vh: Option<String>,
        // Offset C - "This parameter defines a globalrotation angle of the
        // snipping saw unit relative to the contour on the front and rear
        // edge." - "1"
        p62: Option<String>,
        // Snipping for high workpieces - "If the checkbox is activated,the
        // function for snip- ping high workpieces is activated." - "0"
        ks: Option<String>,
        // Z dimension - "Defines the Z position of the saw blade on the
        // underside of the gluing edge." - "0"
        zm: Option<String>,
        // Z-Min bottom edge, sawblade - Defines a safety distance in Z
        // direction, which must be maintained by the saw blade edge. - "0"
        zl: Option<String>,
        // Snipping without tracing - If active, the traced snipping unit is
        // rotated so that snipping is performed without tracing. - "0"
        kt: Option<String>,
    },
    /// 148 - This macro can be used to program the transport in or transport
    /// out of workpieces. The transport is performed with the moving
    /// cantilever supports of the machine. Two vacuum mats are mounted on the
    /// left and right of the moving cantilever supports
    TransportWorkpiece {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // Direction - "This parameter is used to define the transport
        // direction into or out of the machine." -
        // "In = "1"
        // Out = "2"
        ri: Option<String>,
        // X - "Specification of the X position of the center of the left
        // vacuum mat relative to the reference coordinate system." - "0"
        xa: Option<String>,
        // Checkbox Y-left - Activation / deactivation of the parameter Y-left
        // "Y-left inactive = "0"
        // Y-left active = "1"
        li: Option<String>,
        // Y-left - "Specification of the Y position of the center of the left
        // vacuum mat relative to the reference coordinate system." - "0"
        ya1: Option<String>,
        // Code left side - Specification of the tool ID of the left vacuum
        // mat. - "241"
        kl: Option<String>,
        // Checkbox Y-right - Activation / deactivation of the parameter
        // Y-right
        // "Y-right inactive = "0"
        // Y-right active = "1"
        re: Option<String>,
        // Y-right - "Specification of the Y position of the center of the
        // right vacuum mat relative to the reference coordinate system." -
        // "0"
        ya2: Option<String>,
        // Xx offset - "Specification of the X position of the center of the
        // right vacuum mat relative to the center of the left vacuum mat."
        // - "1000"
        xv: Option<String>,
        // Code right side - Specification of the tool ID of the left vacuum
        // mat. - "241"
        kr: Option<String>,
        // Tool number - "Enter the number of a suitable tool directly or
        // select one from the selection dialog." - "16"
        t_: Option<String>,
        // Feed - Feed speed in min - "0"
        f_: Option<String>,
        // Lift - "If this is activated, the workpiece is raised completely by
        // the transport device and moves freely. Otherwise the workpiece will
        // be -seen in motion direction- only raised at its front end. The
        // consoles move along with the workpiece and support it." - "1"
        ki: Option<String>,
        // Automatic adjust - "If this is activated, the workpiece is moved
        // against the stop pin in X and Y direction by the transport
        // device.Only avail- able when transport direction is set to "in"." -
        // "0"
        aa: Option<String>,
        // Remove the rests - If this is activated, the brackets separate
        // after the outfeed of the processed workpiece so that scrap pieces
        // fall onto the chip conveyor belt below.
        rf: Option<String>,
    },
    /// 184 - This macro can be used to program the transport in or transport
    /// out of workpieces.
    TransportWorkpiecePortal {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        // Direction - "This parameter is used to define the transport
        // direction into or out of the machine." - "1"
        // Transport in = "1"
        // Transport out = "2"
        ri: Option<String>,
        // Restack - If the checkbox is activated, restacking takes place. -
        // "0"
        um: Option<String>,
        // X/Y Offset - "For transport in, it specifies the offset of the
        // workpiece. For transport out, it specifies the offset of the
        // suction rails." - "0"
        xa1: Option<String>,
        // X/Y Offset - "For transport in, it specifies the offset of the
        // workpiece. For transport out, it specifies the offset of the
        // suction rails." - "0"
        ya1: Option<String>,
        // Z offset - "For transport in, it specifies the offset of the
        // workpiece. For transport out, it specifies the offset of the
        // suction rails." - "0"
        za1: Option<String>,
        // Stack # - "Determines the number of the stack from which the
        // workpiece is transported onto the machine, or the stack to which
        // the workpiece is transported out of the machine." - "1"
        st: Option<String>,
        // Specify stack offset - "If the checkbox is activated, a stack
        // offset is specified via the 'X/Y offset stack' and 'Z offset stack'
        // parameters.The checkbox is active when parameter XA2 is specified."
        // - "0"
        // -------------------------------------------------------------
        // X/Y stack offset - "Specify The X offset of the related part
        // stack.(in:unprocessed / out: finished)" - "0"
        xa2: Option<String>,
        // X/Y stack offset - "Specify The Y offset of the related part
        // stack.(in:unprocessed / out: finished)" - "0"
        ya2: Option<String>,
        // Z stack offset - "Specify The Z offset of the related part
        // stack.(in:unprocessed / out: finished)" - "0"
        za2: Option<String>,
        // Separate mode - "If the checkbox is deactivated, processing is
        // performed with the standard subprograms." - "000" Off by default"
        md: Option<String>,
        // Tool number - "Enter the number of a suitable tool directly or
        // select one from the selection dialog." - "92" Off by default"
        t_: Option<String>,
        // Feed - Feed speed in m/min. - "5" On by default"
        f_: Option<String>,
        // Transport mode - "This parameter defines the order of alignment to
        // the stops when the workpiece is transported in." - "0"
        am: Option<String>,
        // Clean - If a value > 0 is specified, the workpiece is cleaned with
        // compressed air after processing. - "0"
        re: Option<String>,
    },
    /// 113 - This macro is used to program trimming processes from below the
    /// workpiece on contour elements.
    TrimmingFromBelow {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Prolong start - "This parameter moves the actual start point of the
        // trimmer by the value entered in opposition to the processing
        // direction before the selected start point." - "0" Off by default"
        vls: Option<String>,
        // Prolong end - "This parameter moves the actual end point of the
        // trimmer by the value entered in the processing direction after the
        // selected end point." - "0" Off by default"
        vle: Option<String>,
        // Approach mode - "Selection of one out of several predefined
        // approach modes. Within the approach motion,the tool radius
        // compensation is gradually added." - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        mda: Option<String>,
        // Side - "The side is used to specify the tool offsetin the trimming
        // direction, from the programmed start point to the end point."  -
        // "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN_AB"
        // tangential: "TAN_AB"
        // lateral: "SEI_AB"
        // vertical: "SEN_AB"
        mde: Option<String>,
        // Z dimension - "The Z dimension determines the processing height of
        // the tools. It operates in the Z direction of the respective
        // coordinate system." - "3"
        za: Option<String>,
        // Distance - "Offset of the tool relative to the programmed contour."
        // - "0"
        abs: Option<String>,
        // Arm angle - "Orientation of the underfloor unit's arm around the
        // Z-axis of the referenced coordinate system. X- direction = 0. Unit
        // = degrees." - "90"
        wi: Option<String>,
        // Move together with C axis - "f the checkbox is activated, the C
        // axis is continu- ally tracked so that the tool remains at a
        // constant angle to the contour in its C axis orientation." - "0"
        cr: Option<String>,
        // Tool Number - "Enter the number of a suitable tool directly or
        // select one from the selection dialog." - "125"
        tno: Option<String>,
        // Feed - Feed speed in m/min. - "5"
        f_: Option<String>,
        // Speed - Speed in percent of database value - "0"
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Approach clearance - "Defines a safety distance in the XY plane at
        // which the workpiece is offset to the entered processing start
        // point." - "30"
        an: Option<String>,
        // Withdrawal clearance - "Defines a safety distance in the XY plane
        // at which the tool moves upwards along the Z axis." - "30"
        ab: Option<String>,
        // Run under the workpiece - If the checkbox is activated, the tool
        // approaches from below the workpiece with a safety distance and the
        // tool radius correction is set up below the workpiece contact
        // surface. - "0"
        am: Option<String>,
        // Trimming direction - This parameter defines the trimming direction
        // mode. Depending on the this, the tool will changed or the direction
        // will changed. - "0"
        // manually: "0"
        // synchronous rotation (change direction): "1"
        // reverse rotation (change direction): "2"
        // synchronous rotation (change tool): "3"
        // reverse rotation (change tool): "4"
        tdm: Option<String>,
    },
    /// 133 - This macro is used to program horizontal trimming processes on
    /// contour elements.
    TrimmingHorizontal {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Prolong start - "This parameter moves the actual start point of the
        // trimmer by the value entered in opposition to the processing
        // direction before the selected start point." - "0"
        vls: Option<String>,
        // Prolong end - "This parameter moves the actual end point of the
        // trimmer by the value entered in the processing direction after the
        // selected end point." - "0"
        vle: Option<String>,
        // Approach mode - "Selection of one out of severalpre- defined
        // approach modes.Within the approach motion, the tool radius
        // compensation is gradually added." - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        mda: Option<String>,
        // Side - "The side is used to specify the tool offset in the trimming
        // direction,from the programmed start point to the end point." -
        // "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes.Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN_AB"
        // tangential: "TAN_AB"
        // lateral: "SEI_AB"
        // vertical: "SEN_AB"
        mde: Option<String>,
        // On-the-fly on/off - "If the checkbox is activated, a motion in the
        // Z direction occurs in addition to the lateral or tangential
        // approach and withdrawal mode ." - "0"
        em: Option<String>,
        // Z dimension - "The Z dimension determines the processing height of
        // the tools." - "0"
        zm: Option<String>,
        // Distance - "Offset of the tool relative to the programmed contour."
        // - "-0.5"
        ab: Option<String>,
        // Separate mode - "If the checkbox is deactivated, processing is
        // performed with the standard subprograms." - "000" Off by default"
        md: Option<String>,
        // Tool number - Tool number of the unit - "156"
        tno: Option<String>,
        // Feed - Feed speed in m/min. - "5"
        f_: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Withdrawal clearance - "Defines a safety distance in the XY
        // plane at which the tool moves upwards along the Z axis." - "30" -
        abt: Option<String>,
        // Tool is perpendicular to the XY plane - "woodWOP uses this
        // parameter to align the tool's axis of rotation in the negative Z
        // direction of the reference coordinate system. This is active when
        // C_ is not specified in the file." - -
        // ------------------------------------------------------------- C
        // value - "Specification of the C axis angle. Without specification
        // of A_ and C_ the tool orientation is derived by the
        // perpendicular direction of the assigned level." - "0" -
        c_: Option<String>,
        // A value - The A axis angle setting is specified here. Without
        // specification of A_ and C_ the tool orientation is derived from the
        // perpendicular direction of the assigned level - "0" Off by default
        a_: Option<String>,
        // Trimming direction - This parameter defines the trimming direction
        // mode. Depending on the this, the tool will changed or the direction
        // will changed. - "0"
        // manually: "0"
        // synchronous rotation (change direction): "1"
        // reverse rotation (change direction): "2"
        // synchronous rotation (change tool): "3"
        // reverse rotation (change tool): "4"
        tdm: Option<String>,
    },
    /// 119 - This macro is used to program complex trimming processes such as
    /// letters, writing or company logos.
    TrimmingPolygon {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        base_machine_settings: BaseMachineSettings,
        // X position - "Defines the insertion point using X and Y coordinates
        // of the selected coordinate system" - "0"
        xa: Option<String>,
        // Y position - "Defines the intertion point using X and Y coordinates
        // of the selected coordinate system" - "0"
        ya: Option<String>,
        // Name of Polygon - Reference to a polygon file *.ply - "E.g.:
        // "_gc.ply"
        nm: Option<String>,
        // Embed - "If this is active, the polygon file is embedded directly
        // in this mpr file between STARTLOCAL and ENDLOCAL lines. Hint: There
        // is a separate documentation available for the PLY-format." - "0"
        local: Option<String>,
        // Lenth - "This parameter is used to change the length of the polygon
        // definition in the x direction." - "0"
        br: Option<String>,
        // Width - "This parameter is used to change the width of the polygon
        // definition in the Y direction" - "0"
        he: Option<String>,
        // Z start - "Reference value in Z for start of processing. If this
        // value is not entered, processing begins on the workpiece surface."
        // - "0"
        za: Option<String>,
        // Angle - "This angle is used to rotate processing in the XY plane
        // around the Z axis.This corresponds to the angle of processing in
        // relation to the X axis" - "0"
        wi: Option<String>,
        // Depth - "Depth of the processing from the start point in the
        // processing direction" - "0"
        ti: Option<String>,
        // Side - "The side is used to specify the tool offset viewed from the
        // programmed start and end points." - "WRKL"
        // left = "WRKL"
        // right = "WRKR"
        // centric = "NoWRK"
        rk: Option<String>,
        // Separate mode - "If the checkbox is activated, a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program. The value entered has 3 characters and is numeric and/or
        // alphanumeric, depending on the naming of the subprogram by the
        // programmer." - "000"
        md: Option<String>,
        // Tool number - "If the checkbox is activated, the input field is
        // activated so the number of a tool can be entered." - "101"
        wz: Option<String>,
        // Feed - Feed speed in m/min - "0"
        f_: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default"
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - "SM must be set to "1" to activate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Mechanical tracing - This parameter is used to activate the tracing
        // system - "0"
        mt: Option<String>,
        // Tracing Stroke - "Movement path in the scanning direction in order
        // to bring the sensing device into the sensing range." - "0"
        hu: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing - "0"
        bl: Option<String>,
    },
    /// 105 - This macro is used to define vertical trimming processes on
    /// contour elements
    TrimmingVertical {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Prolong start - "This parameter moves the actual start point of
        // the trimmer by the value entered in opposition to the processing
        // direction before the selected start point." - "0"
        vls: Option<String>,
        // Prolong end - "This parameter moves the actual end point of the
        // trimmer by the value entered in the processing direction after the
        // selected end point." - "0"
        vle: Option<String>,
        // Approach mode - "Selection of one out of several predefined
        // approach modes. Within the approach motion, the tool radius
        // compensation is gradually added." - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        // no checks:"NOCHECK"
        mda: Option<String>,
        // Side - "The side is used to specify the tooloffset in the trimming
        // direction, from the programmed start point to the end point." -
        // "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN_AB"
        // tangential: "TAN_AB"
        // lateral: "SEI_AB"
        // vertical: "SEN_AB"
        // no checks:"NOCHECK_AB"
        mde: Option<String>,
        // On-the-fly on/off - "If the checkbox is deactivated, no process
        // motion occurs in addition to the selected approach and withdrawal
        // mode. If the checkbox is activated,a motion in the Z direction
        // occurs in addition to the lateral or tangential approach and
        // withdrawal mode ." - "0"
        em: Option<String>,
        // Z dimension - "The Z dimension determines the processing height of
        // the tools.It operates in the Z direction of the respective
        // coordinate system." - "-3"
        za: Option<String>,
        // Distance - "Offset of the tool relative to the programmed contour."
        // - "0"
        ab: Option<String>,
        // Separate mode - "Here you can create a suitable mode by programming
        // NC subprograms." - "000"
        md: Option<String>,
        // Tool Number - "Enter the number of a suitable tool directly or
        // select one from the selection dialog." - "101"
        tno: Option<String>,
        // Feed - Feed speed in m/min. - "5"
        f_: Option<String>,
        // Feed oscillation - "Activation of the oscillating milling mode." -
        // "0":Standard milling without oscillation
        // "1": Milling with oscillation"
        oszi: Option<String>,
        // Feed oscillation - "Feed rate in Z direction for the oscillation
        // motion [m/min]." - "0"
        oszvs: Option<String>,
        // Speed - "Speed in percent of database value" - "100" Off by
        // default"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Speed - "SM must be setto "1"to acti- vate the use of S_ or S_A" -
        // "0"
        sm: Option<String>,
        // Approach clearance - Defines a safety distance in the XY plane at
        // which the workpiece is offset to the entered processing start
        // point.
        af: Option<String>,
        // "Tool is perpendicular to the XY plane" - "This parameter enables
        // vertical trimming of a contour definition that is not on the XY
        // plane (000)" - "1"
        wzs: Option<String>,
        // Milling in steps - "If the checkbox is activated, the contour is
        // processed according to the entry for the Number of adjustments
        // parameter in several increments." - "0"
        stufen: Option<String>,
        // Number of adjustments - "If the milling in steps parameter is
        // activated,thecontour is milled in several infeeds in line with the
        // specification of the Number of adjustments parameter ." - "0"
        anzzst: Option<String>,
        // Z start - "If the milling in steps parameter is activated,a
        // reference value for Z is defined for the first adjustment of the
        // tool using Z start ." - "0"
        zstart: Option<String>,
        // Chip deflector - "If the checkbox is activated, the ChipGuides chip
        // guide system is activated." - "0"
        sc: Option<String>,
        // Approach angle - "The angle for the tangential approach is
        // corrected by the specified approach angle." - "0"
        aw: Option<String>,
        // Withdraw angle - "The angle for the tangential withdrawal is
        // corrected by the specified withdrawal angle." - "0"
        bw: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing - "0"
        bl: Option<String>,
        // Trimming direction - This parameter defines the trimming direction
        // mode. Depending on the this, the tool will changed or the direction
        // will changed. - "0"
        // manually: "0"
        // synchronous rotation (change direction): "1"
        // reverse rotation (change direction): "2"
        // synchronous rotation (change tool): "3"
        // reverse rotation (change tool): "4"
        tdm: Option<String>,
    },
    /// 128 - This macro is used to execute trimming processes on the surface
    /// of the workpiece on contour elements.
    TrimmingVerticalTraced {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Insert mode - "Decision if the groove has the full depth in its
        // start and end point (then the groove must be extended beyond start
        // and end point)or whether the groove is exactly between start and
        // end- point." - "MOD0"
        // depth = "MOD0"
        // length = "MOD1"
        em: Option<String>,
        // Prolong start - "This parameter moves the actual start point of the
        // trimmer by the value entered in opposition to the
        // processing direction before the selected start point." - "0"
        vls: Option<String>,
        // Prolong end - "This parameter moves the actual end point of the
        // trimmer bythe value entered in the processing direction after the
        // selected end point." - "0"
        vle: Option<String>,
        // Approach mode - "Selection of one out of several predefined
        // approach modes. Within the approach motion, the tool radius
        // compensation is gradually added." - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        mda: Option<String>,
        // Side - "The side is used to specify the tool offset in the trimming
        // direction, from the programmed start point to the end point." -
        // "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Withdrawal mode - "Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed." - "TAN_AB"
        // tangential: "TAN_AB"
        // lateral: "SEI_AB"
        // vertical: "SEN_AB"
        mde: Option<String>,
        // On-the-fly on/off - "If the checkbox is activated, a motion in the
        // Z direction occurs in addition to the lateral or tangential
        // approach and withdrawal mode ." - "0"
        fl: Option<String>,
        // Distance - "Offset of the tool relative to the programmed contour."
        // - "0"
        ab: Option<String>,
        // Additional C-Angle - "This parameter is used to define an
        // additional angle of rotation of the unit in relation to the
        // workpiece." - "0"
        wi: Option<String>,
        // C-angle absolute/relative - "Specification whether orientation
        // changes in the contour must be traced by the C-angle of the milling
        // unit(relative) or whether no change of the milling unit's C-
        // orientation is desired (absolute)." - "0"
        // Absolute = "0"
        // Relative = "1"
        cm: Option<String>,
        // Separate mode - "If the checkbox is activated,a user-defined NC
        // subprogram is called up instead of the default approach and return
        // program. The value entered has 3 characters and is numeric and/or
        // alpha numeric,depending on the naming of the sub program by the
        // programmer." - "000"
        md: Option<String>,
        // Tool number - Tool number of the unit - "230"
        tno: Option<String>,
        // Feed - Feed speed in m/min. - "5"
        f_: Option<String>,
        // Checkbox feed Z adjustment - "Activation/Deactivation of the
        // parameter feed Z adjustment."
        // "Inactive = "0"
        // Active = "1"
        zua: Option<String>,
        // Feed Z adjustment - "Feed speed in m/min for the process motion
        // into the workpiece." - "0.5" Off by default"
        zu: Option<String>,
        // Speed interpretation mode - "Specification of how the speed input
        // must be interpreted."
        // "0":Absolute value (use only in combination with S_A)
        // "1":Relative value (use only in combination with S_)"
        dz: Option<String>,
        // Speed - "Speed in percent of database value" - "100"
        s_: Option<String>,
        // Speed - "Speed in rpm, absolute speed declaration" - "0"
        s_a: Option<String>,
        // Approach clearance - "Defines a safety distance in the XY plane at
        // which the workpiece is offset to the entered processing start
        // point." - "0"
        af: Option<String>,
        // C new - "This parameter is used to de- termine whether the
        // subsequent Change technology parameters macro with C new data
        // results in absolute or relative C values." - "0"
        // Relative = "0"
        // Absolute = "1"
        _cm: Option<String>,
        // Electronic tracing - "Electronic tracing is active when the ZWZ
        // parameter is specified (mutually exclusive with TH)." -
        // -------------------------------------------------------------
        // Z dimension - "The Z dimension determines the processing height of
        // the tools." - "10.0"
        zwz: Option<String>,
        // Mechanical tracing - "Mechanical tracing is active when the TH
        // parameter is specified (mutually exclusive with Z WZ)." -
        // -------------------------------------------------------------
        // Tracing stroke - "With mechanically traced units ,the sensor
        // position can be changed by the specified value." - "1"
        th: Option<String>,
        // Z Tracer - "This parameter is used to determine the Z-coordinate
        // for tracing." - "3"
        za: Option<String>,
        // Without Z Control - "Intended for test run/simulation purposes in
        // combination with electronic tracing." - "1" = Without Z control "0"
        // = With Z control"
        tm: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing - "0"
        bl: Option<String>,
        // Trimming direction - This parameter defines the trimming direction
        // mode. Depending on the this, the tool will changed or the direction
        // will changed. - "0"
        // manually: "0"
        // synchronous rotation (change direction): "1"
        // reverse rotation (change direction): "2"
        // synchronous rotation (change tool): "3"
        // reverse rotation (change tool): "4"
        tdm: Option<String>,
    },
    /// 149 - This macro is used to program units with a chip deflector or two
    /// tools on contour elements.
    TrimmingVerticalWithCAxis {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Prolong start - This parameter moves the actual start point of the
        // trimmer by the value entered in opposition to the processing
        // direction before the selected start point. - "0" On by default
        vls: Option<String>,
        // Prolong end - This parameter moves the actual end point of the
        // trimmer by the value entered in the processing direction after the
        // selected end point. - "0" On by default
        vle: Option<String>,
        // Approach mode - Selection of one out of several predefined approach
        // modes. Within the approach motion, the tool radius compensation is
        // gradually added. - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        mda: Option<String>,
        // Side - The side is used to specify the tool offset in the trimming
        // direction, from the programmed start point to the end point. -
        // "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Withdrawal mode - Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed. - "TAN_AB"
        // tangential: "TAN_AB"
        // lateral: "SEI_AB"
        // vertical: "SEN_AB"
        mde: Option<String>,
        // On-the-fly on/off - If the checkbox is activated, a motion in the Z
        // direction occurs in addition to the lateral or tangential approach
        // and withdrawal mode . - "0"
        em: Option<String>,
        // Z dimension - The Z dimension determines the processing height of
        // the tools. - "-3"
        za: Option<String>,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "0"
        ab: Option<String>,
        // Additional C-Angle - This parameter is used to define an additional
        // angle of rotation of the unit in relation to the workpiece. - "0"
        c_: Option<String>,
        // Separate mode - If the checkbox is deactivated, processing is
        // performed with the standard subprograms. - "000" Off by default
        md: Option<String>,
        // Tool number - Tool number of the unit - "101"
        tno: Option<String>,
        // Feed - Feed speed in m/min. - "5" On by default
        f_: Option<String>,
        // Oscillation motion - Activation of the oscillating milling mode.
        // "0": Standard milling without oscillation
        // "1": Milling with oscillation
        oszi: Option<String>,
        // Feed Z adjustment - Feed rate in Z direction for the oscillation
        // motion [m/min]. - "0" Off by default
        oszvs: Option<String>,
        // Speed - Speed in percent of database value - "STANDARD" Off by
        // default
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - SM must be set to "1" to activate the use of S_ or S_A -
        // "0"
        sm: Option<String>,
        // Approach clearance - Defines a safety distance in the XY plane at
        // which the workpiece is offset to the entered processing start
        // point. - "0"
        af: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing. - "0"
        bl: Option<String>,
        // Trimming direction - This parameter defines the trimming direction
        // mode. Depending on the this, the tool will changed or the direction
        // will changed. - "0"
        // manually: "0"
        // synchronous rotation (change direction): "1"
        // reverse rotation (change direction): "2"
        // synchronous rotation (change tool): "3"
        // reverse rotation (change tool): "4"
        tdm: Option<String>,

    },
    /// 140 - This macro is used to process trimming contours that are
    /// executed under an A-angle.
    TrimmingWithAAngle {
        id: MacroId,
        base: BaseMacro,
        wood_time: BaseWoodTimeCategory,
        hood_setting: BaseHoodSetting,
        synchronous_mode: BaseSynchronousModeBehavior,
        measurement: BaseMeasurementDependence,
        tech_auto: BaseTechAuto,
        base_machine_settings: BaseMachineSettings,
        base_contour: BaseContour,
        // Prolong start - This parameter moves the actual start point of the
        // trimmer by the value entered in opposition to the processing
        // direction before the selected start point. - "0" Off by default
        vls: Option<String>,
        // Prolong end - This parameter moves the actual end point of the
        // trimmer by the value entered in the processing direction after the
        // selected end point. - "0" Off by default
        vle: Option<String>,
        // Approach mode - Selection of one out of several predefined approach
        // modes. Within the approach motion, the tool radius compensation is
        // gradually added. - "TAN"
        // tangential: "TAN"
        // lateral: "SEI"
        // vertical: "SEN"
        mda: Option<String>,
        // Side - The side is used to specify the tool offset in the trimming
        // direction, from the programmed start point to the end point. -
        // "WRKL"
        // left: "WRKL"
        // right: "WRKR"
        // centric: "NoWRK"
        rk: Option<String>,
        // Withdrawal mode - Selection of one out of several predefined
        // withdrawal modes. Within the withdrawal motion, the tool radius
        // compensation is gradually removed. - "TAN_AB"
        // tangential: "TAN_AB"
        // lateral: "SEI_AB"
        // vertical: "SEN_AB"
        mde: Option<String>,
        // On-the-fly on/off - If the checkbox is activated, a motion in the Z
        // direction occurs in addition to the lateral or tangential approach
        // and withdrawal mode . - "0"
        em: Option<String>,
        // Z dimension - The Z dimension determines the processing height of
        // the tools. - "0"
        zm: Option<String>,
        // Specify distance - This must be set to 1 to enable the use of
        // parameters AB (distance) and AF (approach clearance). - "0"
        av: Option<String>,
        // Distance - Offset of the tool relative to the programmed contour. -
        // "-0.5"
        ab: Option<String>,
        // Approach clearance (axial) - The approach clearance is the
        // clearance between the Z dimension specified and the tool tip. The C
        // and A axes are positioned at this point. The trimming tool moves
        // from here in the direction of the tool to the start point. - "0"
        af: Option<String>,
        // Separate mode - This may be activated by adding the parameter MD to
        // the mpr code. The value must consist of three numerical or
        // alphanumerical digits and calls a corresponding NC-subprogram
        // (which must exist) instead of the standard approach and return
        // program. - "000"
        md: Option<String>,
        // Tool number - Tool number of the unit - "156"
        tno: Option<String>,
        // Feed - Feed speed in m/min. - "5" On by default
        f_: Option<String>,
        // Speed - Speed in percent of database value - "100" Off by default
        s_: Option<String>,
        // Speed - Speed in rpm, absolute speed declaration - "0"
        s_a: Option<String>,
        // Speed - SM must be set to "1" to activate the use of S_ or S_A - "0"
        sm: Option<String>,
        // Tool is perpendicular to the XY plane woodWOP uses this parameter
        // to align the tool's axis of rotation in the negative Z direction of
        // the reference coordinate system. This is active when C_ and A_ are
        // not specified in the file.
        // -------------------------------------------------------------
        // C value - The C value defines the angle of rotation of the tool
        // along the C axis and its direction along the XY plane. - "0"
        c_: Option<String>,
        // A value - The A value defines the movement of the C axis in the X/Y
        // plane. - "0" Off by default
        a_: Option<String>,
        // Tool orientation - Can be used to orient the tool with A_ and C_
        // relative to the contour and level.
        // "0" = (Default) A_ and C_ value are interpreted as absolute machine
        // values when given.
        // "1" = A_ and C_ value are relative to contour and level.
        to: Option<String>,
        // Move together with C axis - f the checkbox is activated, the C axis
        // is continually tracked so that the tool remains at a constant angle
        // to the contour in its C axis orientation. - "0"
        cr: Option<String>,
        // Z value - Defines the approach position of the tool if the tool is
        // perpendicular to the XY plane. - "0" Off by default
        z_: Option<String>,
        // Depth - Depth of the processing from the defined start point in the
        // processing direction. - "0"
        ti: Option<String>,
        // Milling in steps - If the checkbox is activated, the contour is
        // processed according to the entry for the Number of adjustments
        // parameter in several increments. - "0"
        stufen: Option<String>,
        // Number of adjustments - If the Milling in steps parameter is
        // activated, the contour is milled in several infeeds in line with
        // the specification of the Number of adjustments parameter. - "0"
        anzzst: Option<String>,
        // Z start - If the milling in steps parameter is activated, a
        // reference value for Z is defined for the first adjustment of the
        // tool using Z start. - "0"
        zstart: Option<String>,
        // Operation from below When WIU, ANU, AMU or ABU are specified,
        // operation from below is active
        // -------------------------------------------------------------
        // Arm angle - For operation from below. Orientation of the drilling
        // unit's arm around the Z-axis of the referenced coordinate
        // system. X-direction = 0. Unit = degrees. When A_ and C_ are
        // specified, WIU is computed automatically. - "0"
        wiu: Option<String>,
        // Approach clearance - For operation from below. Defines a safety
        // distance in the XY plane from the starting point at which the tool
        // moves downwards in Z-direction. - "0"
        anu: Option<String>,
        // Withdrawal clearance - For operation from below. Defines a safety
        // distance in the XY plane from the endpoint at which the tool moves
        // upwards in Z-direction. - "10"
        abu: Option<String>,
        // Run under the workpiece - For operation from below. When active,
        // the unit / tool is moved downwards in Z-direction to a level below
        // the bottom surface of the workpiece keeping the approach clearance
        // from the start point. The approach to the start-point and the
        // activation of the tool radius correction is made below the
        // workpiece. When inactive, the unit / tool is just moved downwards
        // to the Z-level of the milling operation. The approach to the
        // startpoint and the activation of the tool radius correction is made
        // on this level. - "0"
        amu: Option<String>,
        // Blow off - This function enables the blow-off sprayer on the tool
        // to be activated during processing. - "0"
        bl: Option<String>,
        // Trimming direction - This parameter defines the trimming direction
        // mode. Depending on the this, the tool will changed or the direction
        // will changed. - "0"
        // manually: "0"
        // synchronous rotation (change direction): "1"
        // reverse rotation (change direction): "2"
        // synchronous rotation (change tool): "3"
        // reverse rotation (change tool): "4"
        tdm: Option<String>,
        // Change spindle orientation - Selection between two spindle
        // orientations that are possible for the realization of the same tool
        // orientation. - "0"
        cso: Option<String>,
    },
    /// 100 - Using this macro, the workpiece-specific properties are defined.
    Workpiece {
        // X processed part length - The finished part length defines the
        // length of the workpiece in the X direction after processing. - "L"
        la: Option<String>,
        // Y processed part width - The finished part width defines the width
        // of the workpiece in the Y direction after processing. - "B"
        br: Option<String>,
        // Z processed part thickness - The finished part thickness defines
        // the thickness of the workpiece in the Z direction after
        // processing. - "D"
        di: Option<String>,
        // X unprocessed part dimension - Absolute length of the unprocessed
        // workpiece. - "1846"
        rl: Option<String>,
        // Y unprocessed part dimension - Absolute width of the unprocessed
        // workpiece. - "606"
        rby: Option<String>,
        // X dimension - If this parameter is activated, the difference
        // between the unprocessed part dimensions and the finished part
        // dimensions must be entered. - "6"
        ax: Option<String>,
        // Y dimension - If this parameter is activated, the difference
        // between the unprocessed part dimensions and the finished part
        // dimensions must be entered. - "6"
        ay: Option<String>,
        // Prod. part offset - Offset between the unprocessed part and the
        // processed part in X-direction. - "3"
        fnx: Option<String>,
        // Prod. part offset - Offset between the unprocessed part and the
        // processed part in Y-direction. - "3"
        fny: Option<String>,
        // Unprocessed part offset X - This parameter is used to specify an
        // offset for the unprocessed part relative to the machining place's
        // zero point on the machine table. - "0.0"
        rnx: Option<String>,
        // Unprocessed part offset Y - This parameter is used to specify an
        // offset for the unprocessed part relative to the machining place's
        // zero point on the machine table. - "0.0"
        rny: Option<String>,
        // Unprocessed part offset Z - This parameter is used to specify an
        // offset for the unprocessed part in Z direction relative to the
        // machining place's zero point on the machine table. - "0.0"
        rnz: Option<String>,
        // Inch - If this parameter is activated, the unit of measurement in
        // woodWOP is changed from mm to inch. - "0"
        inch: Option<String>,
        // Number of parts - This parameter is evaluated by the software for
        // the purpose of machine data acquisition. - "1"
        anz: Option<String>,
        // Loading time - This parameter is evaluated to calculate the
        // production time. - "0"
        bes: Option<String>,
        // Removal time - This parameter is evaluated to calculate the
        // production time. - "0"
        ent: Option<String>,
        // Material name - This parameter is used to specify the material
        // name. - ""
        material: Option<String>,
        // Customer name - This parameter is used to specify customer name. -
        // ""
        customer: Option<String>,
        // Order number - This parameter is used to specify the order
        // number. - ""
        order: Option<String>,
        // Article number - This parameter is used to specify the article
        // number. - ""
        article: Option<String>,
        // Part number (ID) - This parameter is used to specify the part
        // number (ID) - ""
        partid: Option<String>,
        // Part type - This parameter is used to specify the part type. - ""
        parttype: Option<String>,
        // Count for part - This parameter determines the number of
        // subprograms for the entire part. - "1"
        mprcount: Option<String>,
        // Current part number - This parameter defines the current number of
        // the subprogram for the entire part. - "1"
        mprnumber: Option<String>,
        // Info 1 - Free Info 1 - ""
        info1: Option<String>,
        // Info 2 - Free Info 2 - ""
        info2: Option<String>,
        // Info 3 - Free Info 3 - ""
        info3: Option<String>,
        // Info 4 - Free Info 4 - ""
        info4: Option<String>,
        // Info 5 - Free Info 5 - ""
        info5: Option<String>,
        // Graindirection - This parameter is used to specify the
        // graindirection direction - "" Off by default
        ma: Option<String>,
        // Icon - These parameters contain the embedded bitmap to be used as
        // icon for the workpiece macro.'
        r00: Option<String>,
        r01: Option<String>,
        r02: Option<String>,
        r03: Option<String>,
        r04: Option<String>,
        r05: Option<String>,
        r06: Option<String>,
        r07: Option<String>,
        r08: Option<String>,
        r09: Option<String>,
        r10: Option<String>,
        r11: Option<String>,
        r12: Option<String>,
        r13: Option<String>,
        r14: Option<String>,
        r15: Option<String>,
        r16: Option<String>,
        r17: Option<String>,
        r18: Option<String>,
        r19: Option<String>,
        r20: Option<String>,
        r21: Option<String>,
        r22: Option<String>,
        r23: Option<String>,
        r24: Option<String>,
        r25: Option<String>,
        r26: Option<String>,
        r27: Option<String>,
    },
}
