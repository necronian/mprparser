use std::fs::File;
use std::io::prelude::*;
use std::{
    error::Error,
    path::PathBuf,
};

#[derive(Debug, Default, Clone)]
pub struct PreParseMPR {
    pub header: Vec<String>,
    pub variable: Vec<String>,
    pub coordinates: Vec<String>,
    pub contours: Vec<String>,
    pub processes: Vec<String>,
    pub embeds: Vec<PreParseMPR>,
}

impl PreParseMPR {
    pub fn new(s: PathBuf) -> Result<Self, Box<dyn Error + Send + Sync>> {
        let contents = read_file_to_utf8(s);
        let mut lines = contents.lines().peekable();
        let mut embeds: Vec<PreParseMPR> = Vec::new();
        let parent = initial_mpr_parse(read_mpr(&mut lines)?);
        
        while lines.peek() != None {
            embeds.push(initial_mpr_parse(read_mpr(&mut lines)?));
        }
        
        Ok(PreParseMPR {
            header: parent.header,
            variable: parent.variable,
            coordinates: parent.coordinates,
            contours: parent.contours,
            processes: parent.processes,
            embeds,
        })
    }
    
}

fn read_file_to_utf8(s: PathBuf) -> String {
    // Read file into Vec<u8>
    let mut file = File::open(s).expect("Unable to open the file");

    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents)
        .expect("Unable to read the file");

    // Convert iso-8859-1 to UTF8 because germans
    contents.iter().map(|&c| c as char).collect()
}

fn read_mpr<'a, T: Iterator<Item = &'a str>>(
    iter: &mut std::iter::Peekable<T>,
) -> Result<Vec<String>, Box<dyn Error + Send + Sync>> {
    let mut output: Vec<String> = Vec::new();

    while !(iter.peek() == None || iter.peek().unwrap().starts_with('!')) {
        output.push(iter.next().unwrap().to_string());
    }

    if iter.peek() == None {
        return Err(Box::from("MPRReadNoTerminated"));
    } else {
        output.push(iter.next().unwrap().to_string());
    }

    Ok(output)
}

fn read_header(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();

    while !(iter.peek().unwrap().starts_with('!')
        || iter.peek().unwrap().starts_with("[0")
        || iter.peek().unwrap().starts_with("[K")
        || iter.peek().unwrap().starts_with("]0")
        || iter.peek().unwrap().starts_with("]1")
        || iter.peek().unwrap().starts_with("]2")
        || iter.peek().unwrap().starts_with("]3")
        || iter.peek().unwrap().starts_with("]4")
        || iter.peek().unwrap().starts_with("]5")
        || iter.peek().unwrap().starts_with("]6")
        || iter.peek().unwrap().starts_with("]7")
        || iter.peek().unwrap().starts_with("]8")
        || iter.peek().unwrap().starts_with("]9")
        || iter.peek().unwrap().starts_with("<0")
        || iter.peek().unwrap().starts_with("<1"))
    {
        output.push(iter.next().unwrap().to_string());
    }

    output
}

fn read_variables(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();

    while !(iter.peek().unwrap().starts_with('!')
        || iter.peek().unwrap().starts_with("[K")
        || iter.peek().unwrap().starts_with("]0")
        || iter.peek().unwrap().starts_with("]1")
        || iter.peek().unwrap().starts_with("]2")
        || iter.peek().unwrap().starts_with("]3")
        || iter.peek().unwrap().starts_with("]4")
        || iter.peek().unwrap().starts_with("]5")
        || iter.peek().unwrap().starts_with("]6")
        || iter.peek().unwrap().starts_with("]7")
        || iter.peek().unwrap().starts_with("]8")
        || iter.peek().unwrap().starts_with("]9")
        || iter.peek().unwrap().starts_with("<0")
        || iter.peek().unwrap().starts_with("<1"))
    {
        output.push(iter.next().unwrap().to_string());
    }

    output
}

fn read_coordinates(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();

    while !(iter.peek().unwrap().starts_with('!')
        || iter.peek().unwrap().starts_with("]0")
        || iter.peek().unwrap().starts_with("]1")
        || iter.peek().unwrap().starts_with("]2")
        || iter.peek().unwrap().starts_with("]3")
        || iter.peek().unwrap().starts_with("]4")
        || iter.peek().unwrap().starts_with("]5")
        || iter.peek().unwrap().starts_with("]6")
        || iter.peek().unwrap().starts_with("]7")
        || iter.peek().unwrap().starts_with("]8")
        || iter.peek().unwrap().starts_with("]9")
        || iter.peek().unwrap().starts_with("<1"))
    {
        output.push(iter.next().unwrap().to_string());
    }

    output
}

fn read_contours(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();

    while !(iter.peek().unwrap().starts_with('!')
        || iter.peek().unwrap().starts_with("[K")
        || iter.peek().unwrap().starts_with("[0")
        || iter.peek().unwrap().starts_with("<0")
        || iter.peek().unwrap().starts_with("<1"))
    {
        output.push(iter.next().unwrap().to_string());
    }

    output
}

fn read_processes(iter: &mut std::iter::Peekable<std::slice::Iter<String>>) -> Vec<String> {
    let mut output: Vec<String> = Vec::new();

    while !(iter.peek().unwrap().starts_with('!')
        || iter.peek().unwrap().starts_with("[0")
        || iter.peek().unwrap().starts_with("[K")
        || iter.peek().unwrap().starts_with("]0")
        || iter.peek().unwrap().starts_with("]1")
        || iter.peek().unwrap().starts_with("]2")
        || iter.peek().unwrap().starts_with("]3")
        || iter.peek().unwrap().starts_with("]4")
        || iter.peek().unwrap().starts_with("]5")
        || iter.peek().unwrap().starts_with("]6")
        || iter.peek().unwrap().starts_with("]7")
        || iter.peek().unwrap().starts_with("]8")
        || iter.peek().unwrap().starts_with("]9")
        || iter.peek() == None)
    {
        output.push(iter.next().unwrap().to_string());
    }

    output
}

fn initial_mpr_parse(s: Vec<String>) -> PreParseMPR {
    let mut lines = s.iter().peekable();

    let header = read_header(&mut lines);
    let mut variable: Vec<String> = Vec::new();
    let mut coordinates: Vec<String> = Vec::new();
    let mut contours: Vec<String> = Vec::new();
    let mut processes: Vec<String> = Vec::new();
    let embeds: Vec<PreParseMPR> = Vec::new();

    while lines.peek() != None {
        if lines.peek().unwrap().starts_with("[0") {
            variable = read_variables(&mut lines);
        }

        if lines.peek().unwrap().starts_with("[K") {
            coordinates = read_coordinates(&mut lines);
        }

        if lines.peek().unwrap().starts_with(']') {
            contours = read_contours(&mut lines);
        }

        if lines.peek().unwrap().starts_with('<') {
            processes = read_processes(&mut lines);
        }

        if lines.peek().unwrap().starts_with('!') || lines.peek().unwrap().starts_with('\\') {
            lines.next();
        }
    }

    PreParseMPR {
        header,
        variable,
        coordinates,
        contours,
        processes,
        embeds,
    }
}
