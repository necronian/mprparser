use crate::util::parse_kv;

/// To establish the version of the MPR-file in an easy way a header is
/// created. The header starts with descriptor [H and is located in the first
/// section of the MPR-data.

#[derive(Debug, Default, Clone)]
pub struct Header {
    // Version of MPR-file (string parameter). Possible values: 4.0 :
    // WoodWOP-Version 4.0
    pub version: Option<String>,
    // if set to 6.x, _BSX and _BSY variables will be set in the workpiece
    // macro of all components, which is already the default for _BSZ
    pub ww: Option<String>,
    // automatically mirror flush trimmings
    pub bfs: Option<String>,
    // NC-program shall be generated as in Y mirrored program
    pub gy: Option<String>,
    // NC-program shall be generated as in X and Y mirrored program
    pub gxy: Option<String>,
    // NC-program shall be generated as sub program
    pub up: Option<String>,
    // Free-motion mode
    pub fm: Option<String>,
    // clearance height (WEEKE and WEEKE-BHX, Default: STANDARD")
    pub zs: Option<String>,
    // Optimizing modus; The drillings will be optimized for processing
    // time. It will be checked if processes can be operated together. And the
    // sequence will be optimized: = 0 => No Optimizing = 1 => Optimizing
    // drilling taking into concern the next processings. (default) = 2 =>
    // Best possible optimizing
    pub op: Option<String>,
    // Machine type (HOMAG, CF-HOMAG, FK-HOMAG odre WEEKE)
    pub mat: Option<String>,
    // Dynamic Mode relative (default 100)
    pub dn: Option<String>,
    // Dynamic Level: = 0 => Not active. = 1 => Standard setting = 2 =>
    // Optimized for speed = 3 => Optimized for quality
    pub dst: Option<String>,
    // delete elements that cannot be processed
    pub wrk2: Option<String>,
    // Mode for evaluate working length of the tool: = 0 => Not evaluated. = 1
    // => Evaluate only for macros with a depth definitions. = 2 => Evaluate
    // for macros with a depth definitions and contour macros.
    pub ewl: Option<String>,
    // 0 => Measures in mm = 1 => Measures in INCH
    pub inch: Option<String>,
    // Representation in WoodWOP (NOMIRROR / XMIRROR / YMIRROR / XYMIRROR)
    pub view: Option<String>,
    // Number of parts to be produced (workpiece macro)
    pub anz: Option<String>,
    // time to apply parts to the machine (woodTime)
    pub bes: Option<String>,
    // time to remove parts from the machine (woodTime)
    pub ent: Option<String>,
    // Boardsize in X. These _∗ - parameters contain the calculated values of
    // the work piece description. These values are used by external
    // evaluation programs. Irrelevant for the NC-generator.
    pub _bsx: Option<String>,
    // Boardsize in Y
    pub _bsy: Option<String>,
    // Boardsize in Z
    pub _bsz: Option<String>,
    // Offset of finished workpiece in X
    pub _fnx: Option<String>,
    // Offset of finished workpiece in Y
    pub _fny: Option<String>,
    // Offset of raw workpiece in X
    pub _rnx: Option<String>,
    // Offset of raw workpiece in Y
    pub _rny: Option<String>,
    // Offset of raw workpiece in Z
    pub _rnz: Option<String>,
    // Size of raw workpiece in X
    pub _rx: Option<String>,
    // Size of raw workpiece Y
    pub _ry: Option<String>,
    // Bitmap of programs. Generally, meaningfully only to use with WoodWop
    // components. Here there can be inserted a bitmap 28 X 28. These bitmap
    // will be showed instead of the workpiece bitmap or the component macro
    // bitmap. The parameter R00 to R27 are optional
    pub r00: Option<String>,
    pub r01: Option<String>,
    pub r02: Option<String>,
    pub r03: Option<String>,
    pub r04: Option<String>,
    pub r05: Option<String>,
    pub r06: Option<String>,
    pub r07: Option<String>,
    pub r08: Option<String>,
    pub r09: Option<String>,
    pub r10: Option<String>,
    pub r11: Option<String>,
    pub r12: Option<String>,
    pub r13: Option<String>,
    pub r14: Option<String>,
    pub r15: Option<String>,
    pub r16: Option<String>,
    pub r17: Option<String>,
    pub r18: Option<String>,
    pub r19: Option<String>,
    pub r20: Option<String>,
    pub r21: Option<String>,
    pub r22: Option<String>,
    pub r23: Option<String>,
    pub r24: Option<String>,
    pub r25: Option<String>,
    pub r26: Option<String>,
    pub r27: Option<String>,
    // Treat workpiece carefully
    pub schn: Option<String>,
    // Highspeed Cutting: full spindle rotation speed of 24.000 RPM or higher
    // is allowed
    pub hsp: Option<String>,
    // Avoid clearance height for horizontal processing
    pub o2: Option<String>,
    // Clockwise optimizing of vertical drillings
    pub o4: Option<String>,
    // Avoid clearance height for processing from below
    pub o3: Option<String>,
    // Avoid clearance height edge post process
    pub o5: Option<String>,
    // suction cup positions will be generated in order of programming
    pub sr: Option<String>,
    // Max. edge lengths which can be grouped
    pub ml: Option<String>,
    // clearance height (Homag NC-postprocessor, Default STANDARD")
    pub uf: Option<String>,
    // NC-program shall be generated as in X mirrored program
    pub gp: Option<String>,
    // NC-program shall be generated as normal program (not mirrored)
    pub np: Option<String>,
    // Dont clear tool after end of program
    pub ne: Option<String>,
    // Dont feed out tool after end of program (only for throughfeed table)
    pub na: Option<String>,
    // force unit selection
    pub us: Option<String>,
    // Combine edges
    pub cb: Option<String>,
    // Turn angle. NC-program shall be turned. possible values: 0, 90, -90
    pub dw: Option<String>,
    // Mirror. Only affects drawing in woodWOP. No influence on
    // NC-generation. = 0 => draw unmirrored = 1 => draw mirrored
    pub mi: Option<String>,
    // Grain Direction, inactive if this parameter is missing (workpiece macro)
    pub ma: Option<String>,
    pub hp: Option<String>,
    pub r#in: Option<String>,
    pub gx: Option<String>,
    pub nm: Option<String>,
    pub km: Option<String>,
    pub fw: Option<String>,    
    pub hs: Option<String>,
    pub material: Option<String>,
    pub customer: Option<String>,
    pub order: Option<String>,
    pub article: Option<String>,
    pub partid: Option<String>,
    pub parttype: Option<String>,
    pub mprcount: Option<String>,
    pub mprnumber: Option<String>,
    pub info1: Option<String>,
    pub info2: Option<String>,
    pub info3: Option<String>,
    pub info4: Option<String>,
    pub info5: Option<String>,
}

impl Header {
    pub fn new(h: Vec<String>) -> Self {
        parse_header(h)
    }

    pub fn write(&self) -> Vec<String> {
        let mut header:Vec<String> = vec!["[H\r\n".to_string()];
        if let Some(version) = &self.version {
            header.push(format!("VERSION=\"{}\"\r\n",version));
        }
        if let Some(ww) = &self.ww {
            header.push(format!("WW=\"{}\"\r\n",&ww));
        }
        if let Some(hp) = &self.hp {
            header.push(format!("HP=\"{}\"\r\n",&hp));
        }
        if let Some(r#in) = &self.r#in {
            header.push(format!("IN=\"{}\"\r\n",&r#in));
        }
        if let Some(gx) = &self.gx {
            header.push(format!("GX=\"{}\"\r\n",&gx));
        }
        if let Some(bfs) = &self.bfs {
            header.push(format!("BFS=\"{}\"\r\n",&bfs));
        }
        if let Some(gy) = &self.gy {
            header.push(format!("GY=\"{}\"\r\n",&gy));
        }
        if let Some(gxy) = &self.gxy {
            header.push(format!("GXY=\"{}\"\r\n",&gxy));
        }
        if let Some(up) = &self.up {
            header.push(format!("UP=\"{}\"\r\n",&up));
        }
        if let Some(fm) = &self.fm {
            header.push(format!("FM=\"{}\"\r\n",&fm));
        }
        if let Some(fw) = &self.fw {
            header.push(format!("FW=\"{}\"\r\n",&fw));
        }
        if let Some(zs) = &self.zs {
            header.push(format!("ZS=\"{}\"\r\n",&zs));
        }
        if let Some(hs) = &self.hs {
            header.push(format!("HS=\"{}\"\r\n",&hs));
        }
        if let Some(op) = &self.op {
            header.push(format!("OP=\"{}\"\r\n",&op));
        }
        if let Some(mat) = &self.mat {
            header.push(format!("MAT=\"{}\"\r\n",&mat));
        }
        if let Some(dn) = &self.dn {
            header.push(format!("DN=\"{}\"\r\n",&dn));
        }
        if let Some(dst) = &self.dst {
            header.push(format!("DST=\"{}\"\r\n",&dst));
        }
        if let Some(wrk2) = &self.wrk2 {
            header.push(format!("WRK2=\"{}\"\r\n",&wrk2));
        }
        if let Some(ewl) = &self.ewl {
            header.push(format!("EWL=\"{}\"\r\n",&ewl));
        }
        if let Some(inch) = &self.inch {
            header.push(format!("INCH=\"{}\"\r\n",&inch));
        }
        if let Some(view) = &self.view {
            header.push(format!("VIEW=\"{}\"\r\n",&view));
        }
        if let Some(anz) = &self.anz {
            header.push(format!("ANZ=\"{}\"\r\n",&anz));
        }
        if let Some(bes) = &self.bes {
            header.push(format!("BES=\"{}\"\r\n",&bes));
        }
        if let Some(ent) = &self.ent {
            header.push(format!("ENT=\"{}\"\r\n",&ent));
        }
        if let Some(_bsx) = &self._bsx {
            header.push(format!("_BSX={}\r\n",&_bsx));
        }
        if let Some(_bsy) = &self._bsy {
            header.push(format!("_BSY={}\r\n",&_bsy));
        }
        if let Some(_bsz) = &self._bsz {
            header.push(format!("_BSZ={}\r\n",&_bsz));
        }
        if let Some(_fnx) = &self._fnx {
            header.push(format!("_FNX={}\r\n",&_fnx));
        }
        if let Some(_fny) = &self._fny {
            header.push(format!("_FNY={}\r\n",&_fny));
        }
        if let Some(_rnx) = &self._rnx {
            header.push(format!("_RNX={}\r\n",&_rnx));
        }
        if let Some(_rny) = &self._rny {
            header.push(format!("_RNY={}\r\n",&_rny));
        }
        if let Some(_rnz) = &self._rnz {
            header.push(format!("_RNZ={}\r\n",&_rnz));
        }
        if let Some(_rx) = &self._rx {
            header.push(format!("_RX={}\r\n",&_rx));
        }
        if let Some(_ry) = &self._ry {
            header.push(format!("_RY={}\r\n",&_ry));
        }
        if let Some(r00) = &self.r00 {
            header.push(format!("R00=\"{}\"\r\n",&r00));
        }
        if let Some(r01) = &self.r01 {
            header.push(format!("R01=\"{}\"\r\n",&r01));
        }
        if let Some(r02) = &self.r02 {
            header.push(format!("R02=\"{}\"\r\n",&r02));
        }
        if let Some(r03) = &self.r03 {
            header.push(format!("R03=\"{}\"\r\n",&r03));
        }
        if let Some(r04) = &self.r04 {
            header.push(format!("R04=\"{}\"\r\n",&r04));
        }
        if let Some(r05) = &self.r05 {
            header.push(format!("R05=\"{}\"\r\n",&r05));
        }
        if let Some(r06) = &self.r06 {
            header.push(format!("R06=\"{}\"\r\n",&r06));
        }
        if let Some(r07) = &self.r07 {
            header.push(format!("R07=\"{}\"\r\n",&r07));
        }
        if let Some(r08) = &self.r08 {
            header.push(format!("R08=\"{}\"\r\n",&r08));
        }
        if let Some(r09) = &self.r09 {
            header.push(format!("R09=\"{}\"\r\n",&r09));
        }
        if let Some(r10) = &self.r10 {
            header.push(format!("R10=\"{}\"\r\n",&r10));
        }
        if let Some(r11) = &self.r11 {
            header.push(format!("R11=\"{}\"\r\n",&r11));
        }
        if let Some(r12) = &self.r12 {
            header.push(format!("R12=\"{}\"\r\n",&r12));
        }
        if let Some(r13) = &self.r13 {
            header.push(format!("R13=\"{}\"\r\n",&r13));
        }
        if let Some(r14) = &self.r14 {
            header.push(format!("R14=\"{}\"\r\n",&r14));
        }
        if let Some(r15) = &self.r15 {
            header.push(format!("R15=\"{}\"\r\n",&r15));
        }
        if let Some(r16) = &self.r16 {
            header.push(format!("R16=\"{}\"\r\n",&r16));
        }
        if let Some(r17) = &self.r17 {
            header.push(format!("R17=\"{}\"\r\n",&r17));
        }
        if let Some(r18) = &self.r18 {
            header.push(format!("R18=\"{}\"\r\n",&r18));
        }
        if let Some(r19) = &self.r19 {
            header.push(format!("R19=\"{}\"\r\n",&r19));
        }
        if let Some(r20) = &self.r20 {
            header.push(format!("R20=\"{}\"\r\n",&r20));
        }
        if let Some(r21) = &self.r21 {
            header.push(format!("R21=\"{}\"\r\n",&r21));
        }
        if let Some(r22) = &self.r22 {
            header.push(format!("R22=\"{}\"\r\n",&r22));
        }
        if let Some(r23) = &self.r23 {
            header.push(format!("R23=\"{}\"\r\n",&r23));
        }
        if let Some(r24) = &self.r24 {
            header.push(format!("R24=\"{}\"\r\n",&r24));
        }
        if let Some(r25) = &self.r25 {
            header.push(format!("R25=\"{}\"\r\n",&r25));
        }
        if let Some(r26) = &self.r26 {
            header.push(format!("R26=\"{}\"\r\n",&r26));
        }
        if let Some(r27) = &self.r27 {
            header.push(format!("R27=\"{}\"\r\n",&r27));
        }
        if let Some(schn) = &self.schn {
            header.push(format!("SCHN=\"{}\"\r\n",&schn));
        }
        if let Some(hsp) = &self.hsp {
            header.push(format!("HSP=\"{}\"\r\n",&hsp));
        }
        if let Some(o2) = &self.o2 {
            header.push(format!("O2=\"{}\"\r\n",&o2));
        }
        if let Some(o3) = &self.o3 {
            header.push(format!("O3=\"{}\"\r\n",&o3));
        }
        if let Some(o4) = &self.o4 {
            header.push(format!("O4=\"{}\"\r\n",&o4));
        }
        if let Some(o5) = &self.o5 {
            header.push(format!("O5=\"{}\"\r\n",&o5));
        }
        if let Some(sr) = &self.sr {
            header.push(format!("SR=\"{}\"\r\n",&sr));
        }
        if let Some(ml) = &self.ml {
            header.push(format!("ML=\"{}\"\r\n",&ml));
        }
        if let Some(uf) = &self.uf {
            header.push(format!("UF=\"{}\"\r\n",&uf));
        }
        if let Some(gp) = &self.gp {
            header.push(format!("GP=\"{}\"\r\n",&gp));
        }
        if let Some(np) = &self.np {
            header.push(format!("NP=\"{}\"\r\n",&np));
        }
        if let Some(ne) = &self.ne {
            header.push(format!("NE=\"{}\"\r\n",&ne));
        }
        if let Some(na) = &self.na {
            header.push(format!("NA=\"{}\"\r\n",&na));
        }
        if let Some(us) = &self.us {
            header.push(format!("US=\"{}\"\r\n",&us));
        }
        if let Some(cb) = &self.cb {
            header.push(format!("CB=\"{}\"\r\n",&cb));
        }
        if let Some(dw) = &self.dw {
            header.push(format!("DW=\"{}\"\r\n",&dw));
        }
        if let Some(nm) = &self.nm {
            header.push(format!("NM=\"{}\"\r\n",&nm));
        }
        if let Some(km) = &self.km {
            header.push(format!("KM=\"{}\"\r\n",&km));
        }
        if let Some(mi) = &self.mi {
            header.push(format!("MI=\"{}\"\r\n",&mi));
        }
        if let Some(ma) = &self.ma {
            header.push(format!("MA=\"{}\"\r\n",&ma));
        }
        if let Some(material) = &self.material {
            header.push(format!("MATERIAL=\"{}\"\r\n",&material));
        }
        if let Some(customer) = &self.customer {
            header.push(format!("CUSTOMER=\"{}\"\r\n",&customer));
        }
        if let Some(order) = &self.order {
            header.push(format!("ORDER=\"{}\"\r\n",&order));
        }
        if let Some(article) = &self.article {
            header.push(format!("ARTICLE=\"{}\"\r\n",&article));
        }
        if let Some(partid) = &self.partid {
            header.push(format!("PARTID=\"{}\"\r\n",&partid));
        }
        if let Some(parttype) = &self.parttype {
            header.push(format!("PARTTYPE=\"{}\"\r\n",&parttype));
        }
        if let Some(mprcount) = &self.mprcount {
            header.push(format!("MPRCOUNT=\"{}\"\r\n",&mprcount));
        }
        if let Some(mprnumber) = &self.mprnumber {
            header.push(format!("MPRNUMBER=\"{}\"\r\n",&mprnumber));
        }
        if let Some(info1) = &self.info1 {
            header.push(format!("INFO1=\"{}\"\r\n",&info1));
        }
        if let Some(info2) = &self.info2 {
            header.push(format!("INFO2=\"{}\"\r\n",&info2));
        }
        if let Some(info3) = &self.info3 {
            header.push(format!("INFO3=\"{}\"\r\n",&info3));
        }
        if let Some(info4) = &self.info4 {
            header.push(format!("INFO4=\"{}\"\r\n",&info4));
        }
        if let Some(info5) = &self.info5 {
            header.push(format!("INFO5=\"{}\"\r\n",&info5));
        }
        header.push("\r\n".to_string());
        header
    }
}

fn parse_header(h: Vec<String>) -> Header {
    let mut data = h.iter();
    let mut header = Header::default();

    // The first line is [H indicating the start of the header section
    data.next();

    for line in data {
        if let Some((k, v)) = parse_kv(line) {
            let value = Some(v.to_string());
            match k {
                "VERSION" => header.version = value,
                "WW" => header.ww = value,
                "HP" => header.hp = value,
                "IN" => header.r#in = value,
                "GX" => header.gx = value,
                "BFS" => header.bfs = value,
                "GY" => header.gy = value,
                "GXY" => header.gxy = value,
                "UP" => header.up = value,
                "FM" => header.fm = value,
                "FW" => header.fw = value,
                "ZS" => header.zs = value,
                "HS" => header.hs = value,
                "OP" => header.op = value,
                "MAT" => header.mat = value,
                "DN" => header.dn = value,
                "DST" => header.dst = value,
                "WRK2" => header.wrk2 = value,
                "EWL" => header.ewl = value,
                "INCH" => header.inch = value,
                "VIEW" => header.view = value,
                "ANZ" => header.anz = value,
                "BES" => header.bes = value,
                "ENT" => header.ent = value,
                "_BSX" => header._bsx = value,
                "_BSY" => header._bsy = value,
                "_BSZ" => header._bsz = value,
                "_FNX" => header._fnx = value,
                "_FNY" => header._fny = value,
                "_RNX" => header._rnx = value,
                "_RNY" => header._rny = value,
                "_RNZ" => header._rnz = value,
                "_RX" => header._rx = value,
                "_RY" => header._ry = value,
                "R00" => header.r00 = value,
                "R01" => header.r01 = value,
                "R02" => header.r02 = value,
                "R03" => header.r03 = value,
                "R04" => header.r04 = value,
                "R05" => header.r05 = value,
                "R06" => header.r06 = value,
                "R07" => header.r07 = value,
                "R08" => header.r08 = value,
                "R09" => header.r09 = value,
                "R10" => header.r10 = value,
                "R11" => header.r11 = value,
                "R12" => header.r12 = value,
                "R13" => header.r13 = value,
                "R14" => header.r14 = value,
                "R15" => header.r15 = value,
                "R16" => header.r16 = value,
                "R17" => header.r17 = value,
                "R18" => header.r18 = value,
                "R19" => header.r19 = value,
                "R20" => header.r20 = value,
                "R21" => header.r21 = value,
                "R22" => header.r22 = value,
                "R23" => header.r23 = value,
                "R24" => header.r24 = value,
                "R25" => header.r25 = value,
                "R26" => header.r26 = value,
                "R27" => header.r27 = value,
                "SCHN" => header.schn = value,
                "HSP" => header.hsp = value,
                "O2" => header.o2 = value,
                "O4" => header.o4 = value,
                "O3" => header.o3 = value,
                "O5" => header.o3 = value,
                "SR" => header.sr = value,
                "ML" => header.ml = value,
                "UF" => header.uf = value,
                "GP" => header.gp = value,
                "NP" => header.np = value,
                "NE" => header.ne = value,
                "NA" => header.na = value,
                "US" => header.us = value,
                "CB" => header.cb = value,
                "DW" => header.dw = value,
                "NM" => header.nm = value,
                "KM" => header.km = value,
                "MI" => header.mi = value,
                "MA" => header.ma = value,
                "MATERIAL" => header.material = value,
                "CUSTOMER" => header.customer = value,
                "ORDER" => header.order = value,
                "ARTICLE" => header.article = value,
                "PARTID" => header.article = value,
                "PARTTYPE" => header.parttype = value,
                "MPRCOUNT" => header.parttype = value,
                "MPRNUMBER" => header.mprnumber = value,
                "INFO1" => header.info1 = value,
                "INFO2" => header.info2 = value,
                "INFO3" => header.info3 = value,
                "INFO4" => header.info4 = value,
                "INFO5" => header.info5 = value,
                unmatched => panic!("Unknown Header Value! {}", unmatched),
            }
        }
    }

    header
}
