use glob::glob;
use std::collections::HashMap;
use std::error::Error;
use std::path::PathBuf;
use clap::{Arg, App};

mod contour;
mod coordinate;
mod error;
mod formula;
mod header;
mod initial_parse;
mod mpr;
mod process;
mod tokenizer;
mod variable;
mod util;
mod macros;

use crate::formula::*;
use crate::mpr::MPR;

/// Try to parse multiple MPR files, useful for finding parser errors
fn check_all(path: &str) -> Result<(), Box<dyn Error + Send + Sync>> {
    println!("Checking All WoodWop Files I can find!");
    println!("In the location {}", path);

    for entry in glob(path).expect("Failed to read glob pattern") {
        println!("{:?}",&entry);
        match entry {
            Ok(path) => {
                println!("{:?}", path.display());
                MPR::new(path)?;
            }
            Err(e) => println!("{:?}", e),
        }
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let matches = App::new("MPRParse")
        .version("0.1.0")
        .author("Jeffrey Stoffers <jstoffers@uberpurple.com>")
        .about("Parser and commandline editor for MPR files")
        .arg(Arg::with_name("globals")
             .short("g")
             .long("globals")
             .help("Specify the location of the global variables.")
             .value_name("GLOBALS"))
        .arg(Arg::with_name("test")
             .short("t")
             .long("test")
             .help("Try parsing all MPR files in the supplied input directory recursivly."))
        .arg(Arg::with_name("file")
             .help("The name of the file/Directory to process.")
             .required(true)
             .index(1))
        .get_matches();

    let file = matches.value_of("file").unwrap();

    if matches.is_present("test") {
        check_all(file)?;
    } else {
        let file = PathBuf::from(file);
        // Parse MPR and create an environment with evaluated variables. Either
        // start with an empty one or evaluate the contents of the WoodWop
        // globals file if the path is passed to the program
        let data = MPR::new(file)?.new_environment ( match matches.value_of("globals") {
            Some(f) => eval_globals(MPR::new(PathBuf::from(f))?),
            None => HashMap::new()
        });

        // Evaluate all the variables in the MPR and return the final mpr with
        // an environment full of variables
        let mpr = evaluate_variables(data);

        //let mpr = eval_process(mpr);
        
        // let test = &mpr.contours[0].elements[0].typ.clone();
        // if let ElementType::Point{y, ..} = test {
        //     let ny = y.as_ref().unwrap();
        //     let val = evaluate_formula(mpr,ny);
        //     println!("{:?}",val);
        // }

        println!("{:?}",mpr.processes);
        //println!("{:?}",mpr.header.write());
    }
    Ok(())
}
