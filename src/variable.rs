use std::error::Error;
use crate::util::parse_kv;

/// A variable table will be initiated by the string '[001' in a line. The
/// following rows will be interpreted as variables until the next section
/// (coordinate system, contour elements or processing macros) is
/// declared. The name of the variable starts at column 1. Its name can not
/// exceed eight characters. The first character can't be a number but has to
/// be a letter. The value follows, divided by an equals sign, in the same
/// row. Formulas are allowed, but only variables defined above can be
/// used. After each variable a comment has to be specified in the following
/// row which describes the variable. These comments are initiated by the
/// identifier KM. The comment is a string of at most 80 characters. It can
/// contain special keywords: BOOL, PRIVATE, DISABLED, GLOBAL and EXPORT. For
/// their meaning refer to the woodWOP documentation on variables.

#[derive(Debug, Default, Clone)]
pub struct Variable {
    pub name: String,
    pub value: String,
    pub comment: Option<String>,
}

impl Variable {
    /// Taking a Vec of strings return a Vec of Variable parsed from the input
    /// strings
    pub fn new(h: Vec<String>)
               -> Result<Vec<Variable>,Box<dyn Error + Send + Sync>> {
        let mut variables: Vec<Variable> = Vec::new();
        // Get an iterator of the vec with the ability to look ahead
        let mut lines = h.iter().peekable();

        // First item is the variable table header [001
        lines.next();

        // While there are still lines left process variables
        while lines.peek() != None {
            // Parse kv pairs
            if let Some((k, v)) = parse_kv(lines.next().unwrap()) {
                // If the line after a variable is not a comment line this is
                // an error in the MPR file
                if let Some(l) = lines.peek() {
                    if l.starts_with("KM") {
                        if let Some((_, c)) = parse_kv(lines.next().unwrap()) {
                            variables.push(Variable {
                                name: k.to_string(),
                                value: v.to_string(),
                                comment: if c.is_empty() {
                                    None
                                } else {
                                    Some(c.to_string())
                                },
                            });
                        }
                    } else {
                        return Err(Box::from("Line does not start with KM.\
                                              Missing variable comment line."));
                    }
                } else {
                    return Err(Box::from("Unexpexted end of lines while\
                                          processing variables."));
                }
            }
        }

        Ok(variables)
    }
}
